<?php

namespace App;

use App\User;
use App\Notifications;
use App\Usersignal;
use Log;

class OneSignal
{
    public function sendStatusChangeNotificationWeb($cadeira_id){

		$ids = array();
		
		$user = User::where("cadeira_id", $cadeira_id)->pluck("id")->toArray();

		foreach ($user as $valor) {
			$user_onesignal = Usersignal::where("cadeira_id", $valor)->pluck("onesginal_id")->toArray();
			foreach ($user_onesignal as $value) {
				array_push($ids, $value);
			}
		}

		Log::info($ids);

    	$heading = array(
        "en" => "Gentleman's BarberShop"
        );

    	$content = array(
        "en" => "Foi feita uma desmarcação!"
        );

        $fields = array(
	        'app_id' => "3f305ede-5599-46d6-a753-f036c4149b45",
    		'include_player_ids' => $ids,
	        //'included_segments' => array('All'),
	        'large_icon' =>"ic_launcher_round.png",
	        'contents' => $content,
	        'headings' => $heading,
	        'priority' => 10
	    );

	    $fields = json_encode($fields);

	    $ch = curl_init();
	    curl_setopt($ch, CURLOPT_URL, "https://onesignal.com/api/v1/notifications");
	    curl_setopt($ch, CURLOPT_HTTPHEADER, array('Content-Type: application/json; charset=utf-8',
	                                               'Authorization: Basic ODYwM2FmZTktYTk2Yi00YjcxLWJlMTktYTEyNzkxZTNmZGI4'));
	    curl_setopt($ch, CURLOPT_RETURNTRANSFER, TRUE);
	    curl_setopt($ch, CURLOPT_HEADER, FALSE);
	    curl_setopt($ch, CURLOPT_POST, TRUE);
	    curl_setopt($ch, CURLOPT_POSTFIELDS, $fields);
	    curl_setopt($ch, CURLOPT_SSL_VERIFYPEER, FALSE);    

	    $response = curl_exec($ch);
	    curl_close($ch);
    }
}
