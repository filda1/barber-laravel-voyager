<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Diastrabalho;
use App\DiastrabalhoEstetica;
use App\Marcacoes;
use App\MarcacoesEstetica;
use App\Config;
use App\ConfigsEstetica;
use App\BarberJob;
use App\EsteticaJob;
use App\Slot;
use App\SlotsEstetica;
use App\AppUser;
use App\SpecialDate;
use App\SpecialDatesEstetica;
use App\Desmarcacoes;
use App\DesmarcacoesEstetica;
use App\SemanasEspeciai;
use App\SemanasEspeciaisEstetica;
use App\OneSignal;
use Carbon\Carbon;
use Storage;
use DB;
use Log;

class MarcacoesController extends Controller
{   
    public function new(Request $request){

        $jobs = "[" . trim($request->jobs_id, ',') . "]"; 

        $jobsIds = json_decode($jobs, true);

        $totalMinutes = 0;

        foreach ($jobsIds as $value) {
            $totalMinutes = $totalMinutes + BarberJob::find($value['id'])->duracao;
        }

        $diaSemana = Carbon::createFromFormat('d/m/Y', $request->date);
        $horaInicio = Carbon::createFromFormat('H:i', $request->hora)->day('22')->month('2')->year('2000');
        $horaFim = Carbon::createFromFormat('H:i', $request->hora)->day('22')->month('2')->year('2000')->addMinutes($totalMinutes);

        if($diaSemana != Carbon::today()){
            if($diaSemana > Carbon::today()->addDays(31))
                return "false";
        }


        $new = new Marcacoes();
        $new->cadeira_id = $request->cadeira;
        $new->user_id = $request->user_id;
        $new->trabalho = $jobs;
        $new->dia = $diaSemana;
        $new->hora_inicio = $horaInicio;

        $new->hora_fim = $horaFim;
    
        if($new->save())
            return "true";
        else
            return "false";
    }

    public function newEstetica(Request $request){

        $jobs = "[" . trim($request->jobs_id, ',') . "]"; 

        $jobsIds = json_decode($jobs, true);

        $totalMinutes = 0;

        foreach ($jobsIds as $value) {
            $totalMinutes = $totalMinutes + EsteticaJob::find($value['id'])->duracao;
        }

        $diaSemana = Carbon::createFromFormat('d/m/Y', $request->date);
        $horaInicio = Carbon::createFromFormat('H:i', $request->hora)->day('22')->month('2')->year('2000');
        $horaFim = Carbon::createFromFormat('H:i', $request->hora)->day('22')->month('2')->year('2000')->addMinutes($totalMinutes);

        if($diaSemana != Carbon::today()){
            if($diaSemana > Carbon::today()->addDays(31))
                return "false";
        }


        $new = new MarcacoesEstetica();
        $new->cadeira_id = $request->cadeira;
        $new->user_id = $request->user_id;
        $new->trabalho = $jobs;
        $new->dia = $diaSemana;
        $new->hora_inicio = $horaInicio;

        $new->hora_fim = $horaFim;
    
        if($new->save())
            return "true";
        else
            return "false";
    }

    public function lastApointment($id){
        $hoje = Carbon::now()->format('Y-m-d');
        
        $marcacao = DB::table('marcacoes')->where('user_id', $id)->where('dia_fechado', null)->where('dia','>=', $hoje)->orderBy('dia', 'asc')->whereNull('deleted_at')->first();

        if(!$marcacao){
            return "none";
        }

        $time = Carbon::now()->format('Y-m-d H:i:s');

        $hora_fim = Carbon::parse($marcacao->hora_fim)->format('H:i');

        if(Carbon::parse($marcacao->dia) <= Carbon::today()){
            if($hora_fim > $time){
                return "none";
            }
        }
        
        $marcacao->cadeira_id = Slot::find($marcacao->cadeira_id)->slot;
        return json_encode($marcacao);
    }

    public function lastApointmentEstetica($id){
        $hoje = Carbon::now()->format('Y-m-d');
        
        $marcacao = DB::table('marcacoes_estetica')->where('user_id', $id)->where('dia_fechado', null)->where('dia','>=', $hoje)->orderBy('dia', 'asc')->whereNull('deleted_at')->first();

        if(!$marcacao){
            return "none";
        }

        $time = Carbon::now()->format('Y-m-d H:i:s');

        $hora_fim = Carbon::parse($marcacao->hora_fim)->format('H:i');

        if(Carbon::parse($marcacao->dia) <= Carbon::today()){
            if($hora_fim > $time){
                return "none";
            }
        }
        
        $marcacao->cadeira_id = SlotsEstetica::find($marcacao->cadeira_id)->slot;
        return json_encode($marcacao);
    }

    public function deleteApointment($id, $iduser){
        $marcacao = Marcacoes::find($id);
        if(!$marcacao){
            return "none";
        }

        if($marcacao->user_id != $iduser){
            return "none";
        }

        $des = new Desmarcacoes();
        $des->cadeira_id = $marcacao->cadeira_id;
        $des->user_id = $marcacao->user_id;
        $des->dia = $marcacao->dia;
        $des->trabalho = $marcacao->trabalho;
        $des->hora_inicio = $marcacao->hora_inicio;
        $des->hora_fim = $marcacao->hora_fim;
        $des->save();

        $onesignal = new OneSignal();
        $onesignal->sendStatusChangeNotificationWeb($marcacao->cadeira_id);

        $marcacao->delete();

        return "ok";
    }

    public function deleteApointmentEstetica($id, $iduser){
        $marcacao = MarcacoesEstetica::find($id);
        if(!$marcacao){
            return "none";
        }

        if($marcacao->user_id != $iduser){
            return "none";
        }

        $des = new DesmarcacoesEstetica();
        $des->cadeira_id = $marcacao->cadeira_id;
        $des->user_id = $marcacao->user_id;
        $des->dia = $marcacao->dia;
        $des->trabalho = $marcacao->trabalho;
        $des->hora_inicio = $marcacao->hora_inicio;
        $des->hora_fim = $marcacao->hora_fim;
        $des->save();
        /*
        $onesignal = new OneSignal();
        $onesignal->sendStatusChangeNotificationWeb($marcacao->cadeira_id);
        */
        $marcacao->delete();

        return "ok";
    }

    public function infoApointment($id){
        $marcacao = Marcacoes::find($id);
        if(!$marcacao){
            return "none";
        }
        $marcacao->cadeira_id = Slot::find($marcacao->cadeira_id)->slot;

        $trabalhos = "";

        $t = json_decode($marcacao->trabalho);

        foreach ($t as $value2) {
            $b = BarberJob::find($value2->id)->nome;

            if($trabalhos == "") {
                $trabalhos = $b; 
            }else{
                $trabalhos = $trabalhos . ", " . $b; 
            }
        }

        $marcacao->trabalho = $trabalhos;

        return json_encode($marcacao);
    }

    public function infoApointmentEstetica($id){
        $marcacao = MarcacoesEstetica::find($id);
        if(!$marcacao){
            return "none";
        }
        $marcacao->cadeira_id = SlotsEstetica::find($marcacao->cadeira_id)->slot;

        $trabalhos = "";

        $t = json_decode($marcacao->trabalho);

        foreach ($t as $value2) {
            $b = EsteticaJob::find($value2->id)->nome;

            if($trabalhos == "") {
                $trabalhos = $b; 
            }else{
                $trabalhos = $trabalhos . ", " . $b; 
            }
        }

        $marcacao->trabalho = $trabalhos;

        return json_encode($marcacao);
    }

    public function old($id){
        $now = Carbon::now();

        $old = Marcacoes::where('dia', '<', $now)->where('hora_inicio', '<', $now)->where('user_id', $id)->get();

        foreach ($old as $value) {
            $value->cadeira_id = Slot::find($value->cadeira_id)->slot;
            $value->hora_inicio = Carbon::parse($value->hora_inicio)->format('H:i');
        }

        return $old;
    }

    public function oldEstetica($id){
        $now = Carbon::now();

        $old = MarcacoesEstetica::where('dia', '<', $now)->where('user_id', $id)->get();

        foreach ($old as $value) {
            $value->cadeira_id = SlotsEstetica::find($value->cadeira_id)->slot;
            $value->hora_inicio = Carbon::parse($value->hora_inicio)->format('H:i');
        }

        return $old;
    }

    public function get($id){
        $now = Carbon::now();

        $n = Marcacoes::where('dia', '>=', $now)->where('user_id', $id)->get();

        foreach ($n as $value) {
            $value->cadeira_id = Slot::find($value->cadeira_id)->slot;
            $value->hora_inicio = Carbon::parse($value->hora_inicio)->format('H:i');
        }

        return $n;
    }

    public function getEstetica($id){
        $now = Carbon::now();

        $n = MarcacoesEstetica::where('dia', '>=', $now)->where('user_id', $id)->get();

        foreach ($n as $value) {
            $value->cadeira_id = SlotsEstetica::find($value->cadeira_id)->slot;
            $value->hora_inicio = Carbon::parse($value->hora_inicio)->format('H:i');
        }

        return $n;
    }

    public function getDate(Request $request){
        $now = Carbon::createFromFormat('d/m/Y', $request->date)->format('Y-m-d');

        $n = Marcacoes::where('dia', '=', $now)->where('user_id', $request->user_id)->whereNull('deleted_at')->get();

        foreach ($n as $value) {
            $value->cadeira_id = Slot::find($value->cadeira_id)->slot;
            $value->hora_inicio = Carbon::parse($value->hora_inicio)->format('H:i');
        }

        return $n;
    }

    public function getDateEstetica(Request $request){
        $now = Carbon::createFromFormat('d/m/Y', $request->date)->format('Y-m-d');

        $n = MarcacoesEstetica::where('dia', '=', $now)->where('user_id', $request->user_id)->whereNull('deleted_at')->get();

        foreach ($n as $value) {
            $value->cadeira_id = SlotsEstetica::find($value->cadeira_id)->slot;
            $value->hora_inicio = Carbon::parse($value->hora_inicio)->format('H:i');
        }

        return $n;
    }

    public function getCadeiras(Request $request){
    	$diaSemana = Carbon::createFromFormat('d/m/Y', $request->date)->format('l');
    	$diasTrabalho = Diastrabalho::where($diaSemana, '=', 1)->get();
        $cadeiras = array();

        foreach ($diasTrabalho as $key) {
            $special = DB::table('special_dates')->where('date', Carbon::createFromFormat('d/m/Y', $request->date)->format('Y-m-d'))->where('cadeira_id', $key->cadeira)->orderBy('created_at', 'desc')->first();
            if($special){
                if($special->isAllDay != 1){
                    array_push($cadeiras, $key->cadeira);
                }else{
                }
            }else{
                array_push($cadeiras, $key->cadeira);
            }
        }

        $semana = SemanasEspeciai::where('dia', Carbon::createFromFormat('d/m/Y', $request->date)->format('Y-m-d'))->get();

        foreach ($semana as $value) {
            array_push($cadeiras, $value->barbers);
        }

        if(isset($request->jobs_id)){
            foreach ($cadeiras as $key => $value) {
                $jobs = "[" . trim($request->jobs_id, ',') . "]";
                $jobsIds = json_decode($jobs, true);
                foreach ($jobsIds as $job) {
                    if($job['id'] == 10 || $job['id'] == 4){
                        if($value == 3 || $value == 5 )
                            unset($cadeiras[$key]);
                    }
                    if($job['id'] == 1 || $job['id'] == 2){
                        if($value == 6)
                            unset($cadeiras[$key]);
                    }
                    break;
                }
                
                $myRequest = new \Illuminate\Http\Request();
                $myRequest->setMethod('POST');
                $myRequest->request->add(['jobs_id' => $request->jobs_id]);
                $myRequest->request->add(['date' => $request->date]);
                $myRequest->request->add(['cadeira' => $value]);

                $result = $this->getHours($myRequest);
                $check = array_filter($result);

                if (empty($check)) {
                    unset($cadeiras[$key]);
                }
            }      
        }

        $cadeirasR = DB::table('slots')->whereIn('id', $cadeiras)->where('is_Active', '1')->get();
        $cadeirasF = DB::table('slots')->whereNotIn('id', $cadeiras)->where('is_Active', '1')->get();

        $total = array();

        array_push($total, $cadeirasR);
        array_push($total, $cadeirasF);

    	return $total;
    }
    
    public function getCadeirasEstetica(Request $request){
    	$diaSemana = Carbon::createFromFormat('d/m/Y', $request->date)->format('l');
        
    	$diasTrabalho = DiastrabalhoEstetica::where($diaSemana, '=', 1)->get();
        
        $cadeiras = array();

        foreach ($diasTrabalho as $key) {

            $special = DB::table('special_dates_estetica')->where('date', Carbon::createFromFormat('d/m/Y', $request->date)->format('Y-m-d'))->where('cadeira_id', $key->cadeira)->orderBy('created_at', 'desc')->first();

            if($special){
                    if($special->isAllDay != 1){
                        array_push($cadeiras, $key->cadeira);
                    }else{
                    }
            }else{
                array_push($cadeiras, $key->cadeira);
            }
            
        }

        $semana = SemanasEspeciaisEstetica::where('dia', Carbon::createFromFormat('d/m/Y', $request->date)->format('Y-m-d'))->get();

        foreach ($semana as $value) {
            array_push($cadeiras, $value->esteticista);
        }

        /*if(isset($request->jobs_id)){
            foreach ($cadeiras as $key => $value) {
                $jobs = "[" . trim($request->jobs_id, ',') . "]"; 

                $jobsIds = json_decode($jobs, true);

                foreach ($jobsIds as $job) {

                        if($job['id'] == 10 || $job['id'] == 4){
                            if($value == 3 || $value == 5 )
                                unset($cadeiras[$key]);
                        }

                        if($job['id'] == 1 || $job['id'] == 2){
                            if($value == 6)
                                unset($cadeiras[$key]);
                        }
                    
                    break;
                }
                
                $myRequest = new \Illuminate\Http\Request();
                $myRequest->setMethod('POST');
                $myRequest->request->add(['jobs_id' => $request->jobs_id]);
                $myRequest->request->add(['date' => $request->date]);
                $myRequest->request->add(['cadeira' => $value]);

                $result = $this->getHours($myRequest);

                $check = array_filter($result);

                if (empty($check)) {
                    unset($cadeiras[$key]);
                }
            }      
        }   */

        $cadeirasR = DB::table('slots_estetica')->whereIn('id', $cadeiras)->where('is_Active', '1')->get();
        $cadeirasF = DB::table('slots_estetica')->whereNotIn('id', $cadeiras)->where('is_Active', '1')->get();

        $total = array();

        array_push($total, $cadeirasR);
        array_push($total, $cadeirasF);

    	return $total;
    }

    public function barberGetDay(Request $request){
        $now = Carbon::now();

        $n = Marcacoes::where('dia', '=', $now->format('Y-m-d'))->where('cadeira_id', $request->cadeira_id)->get();

        foreach ($n as $value) {
            $trabalhos = "";

            $t = json_decode($value->trabalho);

            foreach ($t as $value2) {
                $b = BarberJob::find($value2->id)->nome;

                if($trabalhos == "") {
                    $trabalhos = $b; 
                }else{
                    $trabalhos = $trabalhos . ", " . $b; 
                }
            }

            $u = AppUser::find($value->user_id);
            $value["user_name"] = $u->first_name . " " . $u->last_name;
            $value["trabalhosName"] = $trabalhos;
            $value->hora_inicio = Carbon::parse($value->hora_inicio)->format('H:i');
        }

        return $n;
    }

    public function getMarcacao($id){
        $m = Marcacoes::find($id);

        $u = AppUser::find($m->user_id);
        $m["user_name"] = $u->first_name . " " . $u->last_name;

        $trabalhos = "";

        $t = json_decode($m->trabalho);

        foreach ($t as $value2) {
            $b = BarberJob::find($value2->id)->nome;

            if($trabalhos == "") {
                $trabalhos = $b; 
            }else{
                $trabalhos = $trabalhos . ", " . $b; 
            }
        }
        $m["trabalhosName"] = $trabalhos;

        return $m;
    }

    public function finalizarMarcacao($id, $valor){
        $m = Marcacoes::find($id);

        if($m->valor_final == null){
            $m->dia_fechado = Carbon::now();
        }
        $m->valor_final = $valor;
        
        $m->save();

        return "success";
    }

    public function uploadImage(Request $request){
        $output = "";

        parse_str($request->info, $output);

        $image = $request["image"]; 

        $image = preg_replace('/data:image\/(.*?);base64,/','',$image);
        $image = str_replace(' ', '+', $image);

        $imageName = 'image_' . time() . '.' . 'jpeg'; //generating unique file name;
        Storage::disk(config('voyager.storage.disk'))->put("marcacoes/".$imageName, base64_decode($image));

        $m = Marcacoes::find($output["marcacao_id"]);
        $m->image = "marcacoes/".$imageName;
        $r = $m->save();

        if($r){
            return "marcacoes/".$imageName;
        }else{
            return "na";
        }

    }

    public function uploadImageEstetica(Request $request){
        $output = "";

        parse_str($request->info, $output);

        $image = $request["image"]; 

        $image = preg_replace('/data:image\/(.*?);base64,/','',$image);
        $image = str_replace(' ', '+', $image);

        $imageName = 'image_' . time() . '.' . 'jpeg'; //generating unique file name;
        Storage::disk(config('voyager.storage.disk'))->put("marcacoesEstetica/".$imageName, base64_decode($image));

        $m = MarcacoesEstetica::find($output["marcacao_id"]);
        $m->image = "marcacoesEstetica/".$imageName;
        $r = $m->save();

        if($r){
            return "marcacoesEstetica/".$imageName;
        }else{
            return "na";
        }

    }

    public function verificarDisponiblidade(Request $request){
        return $this->getHours($request);
    }

    public function verificarDisponiblidadeEstetica(Request $request){
        return $this->getHoursEstetica($request);
    }

    public function getHours($request){
        $jobs = "[" . trim($request->jobs_id, ',') . "]"; 

        $jobsIds = json_decode($jobs, true);

        $totalMinutes = 0;

        foreach ($jobsIds as $value) {
            $totalMinutes = $totalMinutes + BarberJob::find($value['id'])->duracao;
        }
        $checkDay = Carbon::createFromFormat('d/m/Y', $request->date)->format("l");
        //AQUI
        if($checkDay == "Saturday" || $checkDay == "Sunday"){
            $horaAbertura = Carbon::createFromFormat('H:i', Config::where("key", "Hora_de_Abertura_FS")->first()->value)->day('22')->month('2')->year('2000');
            $horaAlmoço = Carbon::createFromFormat("H:i", Config::where("key", "Hora_de_Almoço_FS")->first()->value)->day('22')->month('2')->year('2000');
            $horaReabertura = Carbon::createFromFormat("H:i", Config::where("key", "Hora_de_Reabertura_FS")->first()->value)->day('22')->month('2')->year('2000');
            $horaFecho = Carbon::createFromFormat("H:i", Config::where("key", "Hora_de_Fecho_FS")->first()->value)->day('22')->month('2')->year('2000');
        }else{
            $horaAbertura = Carbon::createFromFormat('H:i', Config::where("key", "Hora_de_Abertura")->first()->value)->day('22')->month('2')->year('2000');
            $horaAlmoço = Carbon::createFromFormat("H:i", Config::where("key", "Hora_de_Almoço")->first()->value)->day('22')->month('2')->year('2000');
            $horaReabertura = Carbon::createFromFormat("H:i", Config::where("key", "Hora_de_Reabertura")->first()->value)->day('22')->month('2')->year('2000');
            $horaFecho = Carbon::createFromFormat("H:i", Config::where("key", "Hora_de_Fecho")->first()->value)->day('22')->month('2')->year('2000');
        }

        $specialDate = SpecialDate::where("cadeira_id", $request->cadeira)
                                  ->where("date", Carbon::createFromFormat('d/m/Y', $request->date)->format("Y-m-d"))
                                  ->where("isAllDay", 0)
                                  ->first();

        if($specialDate){
            $horaEspecialStart = Carbon::parse($specialDate->start_hour)->format('H:i');
            $horaEspecialEnd = Carbon::parse($specialDate->end_hour)->format('H:i');

            $horaEspecialOpenStart = Carbon::parse($specialDate->start_hour)->day('22')->month('2')->year('2000');
            $horaEspecialOpenEnd = Carbon::parse($specialDate->end_hour)->day('22')->month('2')->year('2000');
        }

        $horasdisponiveis = array();
        $horaAtual = $horaAbertura;

        if($horaAbertura == "00:00" && $horaFecho == "00:00")
            return array_values($horasdisponiveis);
        for ($i=0; $i < 200; $i++) {
        	if($i == 0)
            	$hora = $horaAtual;
    		else
            	$hora = $horaAtual->addMinutes($totalMinutes);
            if($specialDate){
                if($specialDate->tipo == "aberto"){
                    if($hora >= $horaEspecialOpenStart && $hora < $horaEspecialOpenEnd){
                        array_push($horasdisponiveis, $hora->format('H:i'));
                        continue;
                    }
                }
            }

            if($hora >= $horaAlmoço && $hora < $horaReabertura){
                array_push($horasdisponiveis, $horaReabertura->format('H:i'));
                $horaAtual = $horaReabertura;
                continue;
            }

            if($hora >= $horaFecho)
                continue;

            if($hora == "2000-02-22 12:20:00"){
                if($totalMinutes != 10)
                    continue;
            }
            array_push($horasdisponiveis, $hora->format('H:i'));
        }

        $marcacoesDia = Marcacoes::where('dia', Carbon::createFromFormat('d/m/Y', $request->date)->format("Y-m-d"))->where('cadeira_id', $request->cadeira)->get();
        $marcacoesEsteticaDia = MarcacoesEstetica::where('dia', Carbon::createFromFormat('d/m/Y', $request->date)->format("Y-m-d"))->where('user_id', $request->user_id)->get();

        foreach ($marcacoesDia as $marcacao) {
            $marcacaoInicial = Carbon::parse($marcacao->hora_inicio)->day('22')->month('2')->year('2000');
            $marcacaoFim = Carbon::parse($marcacao->hora_fim)->day('22')->month('2')->year('2000');
            $marcacaoInicialLimite = $marcacaoInicial;
            $marcacaoInicialLimite->subMinutes($totalMinutes);
            foreach ($horasdisponiveis as $key => $value) {
                $horaTmp = Carbon::createFromFormat('H:i', $value)->day('22')->month('2')->year('2000');
                $horaTmp2 = Carbon::createFromFormat('H:i', $value)->subMinutes(1)->day('22')->month('2')->year('2000');
                if($value.":00" == $marcacao->hora_inicio){
                    unset($horasdisponiveis[$key]);
                }
                if($specialDate){
                    if($specialDate->tipo == "fechado"){
                        if( $value >= $horaEspecialStart && $value < $horaEspecialEnd){
                            unset($horasdisponiveis[$key]);
                        }
                    }
                }
                if($horaTmp2 >= $marcacaoInicial && $horaTmp < $marcacaoFim){
                    unset($horasdisponiveis[$key]);
                }else if($horaTmp2 >= $marcacaoInicialLimite && $horaTmp < $marcacaoFim){
                    unset($horasdisponiveis[$key]);
                }
            }
        }
        foreach ($marcacoesEsteticaDia as $marcacao) {
            $marcacaoInicial = Carbon::parse($marcacao->hora_inicio)->day('22')->month('2')->year('2000');
            $marcacaoFim = Carbon::parse($marcacao->hora_fim)->day('22')->month('2')->year('2000');
            $marcacaoInicialLimite = $marcacaoInicial;
            $marcacaoInicialLimite->subMinutes($totalMinutes);
            foreach ($horasdisponiveis as $key => $value) {
                $horaTmp = Carbon::createFromFormat('H:i', $value)->day('22')->month('2')->year('2000');
                $horaTmp2 = Carbon::createFromFormat('H:i', $value)->subMinutes(1)->day('22')->month('2')->year('2000');
                if($value.":00" == $marcacao->hora_inicio){
                    unset($horasdisponiveis[$key]);
                }
                if($specialDate){
                    if($specialDate->tipo == "fechado"){
                        if( $value >= $horaEspecialStart && $value < $horaEspecialEnd){
                            unset($horasdisponiveis[$key]);
                        }
                    }
                }
                if($horaTmp2 >= $marcacaoInicial && $horaTmp < $marcacaoFim){
                    unset($horasdisponiveis[$key]);
                }else if($horaTmp2 >= $marcacaoInicialLimite && $horaTmp < $marcacaoFim){
                    unset($horasdisponiveis[$key]);
                }
            }
        }
        if($specialDate){
            foreach ($horasdisponiveis as $key => $value) {
                if($specialDate->tipo == "fechado"){
                    if($value >= $horaEspecialStart && $value < $horaEspecialEnd)
                        unset($horasdisponiveis[$key]);
                }
            }
        }
        
        $now = Carbon::today()->startOfDay();
        $time = Carbon::now()->format('H:i');
        $dateSelected = Carbon::createFromFormat('d/m/Y', $request->date)->startOfDay();
        
        if($now == $dateSelected){
            foreach ($horasdisponiveis as $key => $value) {
                if($value <= $time)
                    unset($horasdisponiveis[$key]);
            }
        }
        $semana = SemanasEspeciai::where('dia', Carbon::createFromFormat('d/m/Y', $request->date)->format('Y-m-d'))
                                 ->where('barbers', $request->cadeira)
                                 ->first();
        if($semana){
            if($semana->manha == 1 && $semana->tarde == 1){
            }
            else{
                foreach ($horasdisponiveis as $key => $value) {
                    if($semana->manha == 1){
                        if( $value > Carbon::parse($horaAlmoço)->format("H:i"))
                            unset($horasdisponiveis[$key]);
                    }
                    if($semana->tarde == 1){
                        if( $value < Carbon::parse($horaReabertura)->format("H:i"))
                            unset($horasdisponiveis[$key]);
                    }
                }
            }
        }
        return array_values($horasdisponiveis);
    }
    
    public function getHoursEstetica($request){
        $jobs = "[" . trim($request->jobs_id, ',') . "]"; 

        $jobsIds = json_decode($jobs, true);

        $totalMinutes = 0;

        foreach ($jobsIds as $value) {
            $totalMinutes = $totalMinutes + EsteticaJob::find($value['id'])->duracao;
        }
        $checkDay = Carbon::createFromFormat('d/m/Y', $request->date)->format("l");
        //AQUI
        if($checkDay == "Saturday" || $checkDay == "Sunday"){
            $horaAbertura = Carbon::createFromFormat('H:i', ConfigsEstetica::where("key", "Hora_de_Abertura_FS")->first()->value)->day('22')->month('2')->year('2000');
            $horaAlmoço = Carbon::createFromFormat("H:i", ConfigsEstetica::where("key", "Hora_de_Almoço_FS")->first()->value)->day('22')->month('2')->year('2000');
            $horaReabertura = Carbon::createFromFormat("H:i", ConfigsEstetica::where("key", "Hora_de_Reabertura_FS")->first()->value)->day('22')->month('2')->year('2000');
            $horaFecho = Carbon::createFromFormat("H:i", ConfigsEstetica::where("key", "Hora_de_Fecho_FS")->first()->value)->day('22')->month('2')->year('2000');
        }else{
            $horaAbertura = Carbon::createFromFormat('H:i', ConfigsEstetica::where("key", "Hora_de_Abertura")->first()->value)->day('22')->month('2')->year('2000');
            $horaAlmoço = Carbon::createFromFormat("H:i", ConfigsEstetica::where("key", "Hora_de_Almoço")->first()->value)->day('22')->month('2')->year('2000');
            $horaReabertura = Carbon::createFromFormat("H:i", ConfigsEstetica::where("key", "Hora_de_Reabertura")->first()->value)->day('22')->month('2')->year('2000');
            $horaFecho = Carbon::createFromFormat("H:i", ConfigsEstetica::where("key", "Hora_de_Fecho")->first()->value)->day('22')->month('2')->year('2000');
        }

        $specialDate = SpecialDatesEstetica::where("cadeira_id", $request->cadeira)->where("date", Carbon::createFromFormat('d/m/Y', $request->date)->format("Y-m-d"))->where("isAllDay", 0)->first();

        if($specialDate){
            $horaEspecialStart = Carbon::parse($specialDate->start_hour)->format('H:i');
            $horaEspecialEnd = Carbon::parse($specialDate->end_hour)->format('H:i');

            $horaEspecialOpenStart = Carbon::parse($specialDate->start_hour)->day('22')->month('2')->year('2000');
            $horaEspecialOpenEnd = Carbon::parse($specialDate->end_hour)->day('22')->month('2')->year('2000');
        }

        $horasdisponiveis = array();
        $horaAtual = $horaAbertura;

        if($horaAbertura == "00:00" && $horaFecho == "00:00")
            return array_values($horasdisponiveis);

        for ($i=0; $i < 200; $i++) {

        	if($i == 0)
            	$hora = $horaAtual;
    		else
            	$hora = $horaAtual->addMinutes($totalMinutes);

            if($specialDate){
                if($specialDate->tipo == "aberto"){
              

                    if($hora >= $horaEspecialOpenStart && $hora < $horaEspecialOpenEnd){
                    
                        array_push($horasdisponiveis, $hora->format('H:i'));
                        continue;
                    }
                }
            }

            if($hora >= $horaAlmoço && $hora < $horaReabertura){
                array_push($horasdisponiveis, $horaReabertura->format('H:i'));
                $horaAtual = $horaReabertura;
                continue;
            }

            if($hora >= $horaFecho)
                continue;

            if($hora == "2000-02-22 12:20:00"){
                if($totalMinutes != 10)
                    continue;
            }

            array_push($horasdisponiveis, $hora->format('H:i'));

        }

        $marcacoesEsteticaDia = MarcacoesEstetica::where('dia', Carbon::createFromFormat('d/m/Y', $request->date)->format("Y-m-d"))->where('cadeira_id', $request->cadeira)->get();
        $marcacoesDia = Marcacoes::where('dia', Carbon::createFromFormat('d/m/Y', $request->date)->format("Y-m-d"))->where('user_id', $request->user_id)->get();

        foreach ($marcacoesEsteticaDia as $marcacao) {

            $marcacaoInicial = Carbon::parse($marcacao->hora_inicio)->day('22')->month('2')->year('2000');
            $marcacaoFim = Carbon::parse($marcacao->hora_fim)->day('22')->month('2')->year('2000');

            $marcacaoInicialLimite = $marcacaoInicial;
            $marcacaoInicialLimite->subMinutes($totalMinutes);

            foreach ($horasdisponiveis as $key => $value) {
               $horaTmp = Carbon::createFromFormat('H:i', $value)->day('22')->month('2')->year('2000');
               $horaTmp2 = Carbon::createFromFormat('H:i', $value)->subMinutes(1)->day('22')->month('2')->year('2000');

               
                if($value.":00" == $marcacao->hora_inicio){
                    unset($horasdisponiveis[$key]);
                }

                if($specialDate){
                    if($specialDate->tipo == "fechado"){
                        if( $value >= $horaEspecialStart && $value < $horaEspecialEnd){
                            unset($horasdisponiveis[$key]);
                        }
                    }
                }

                if($horaTmp2 >= $marcacaoInicial && $horaTmp < $marcacaoFim){
                    unset($horasdisponiveis[$key]);
                }else if($horaTmp2 >= $marcacaoInicialLimite && $horaTmp < $marcacaoFim){
                    unset($horasdisponiveis[$key]);
                }
            }
        }

        foreach ($marcacoesDia as $marcacao) {

            $marcacaoInicial = Carbon::parse($marcacao->hora_inicio)->day('22')->month('2')->year('2000');
            $marcacaoFim = Carbon::parse($marcacao->hora_fim)->day('22')->month('2')->year('2000');

            $marcacaoInicialLimite = $marcacaoInicial;
            $marcacaoInicialLimite->subMinutes($totalMinutes);

            foreach ($horasdisponiveis as $key => $value) {
               $horaTmp = Carbon::createFromFormat('H:i', $value)->day('22')->month('2')->year('2000');
               $horaTmp2 = Carbon::createFromFormat('H:i', $value)->subMinutes(1)->day('22')->month('2')->year('2000');
               
                if($value.":00" == $marcacao->hora_inicio){
                    unset($horasdisponiveis[$key]);
                }

                if($specialDate){
                    if($specialDate->tipo == "fechado"){
                        if( $value >= $horaEspecialStart && $value < $horaEspecialEnd){
                            unset($horasdisponiveis[$key]);
                        }
                    }
                }

                if($horaTmp2 >= $marcacaoInicial && $horaTmp < $marcacaoFim){
                    unset($horasdisponiveis[$key]);
                }else if($horaTmp2 >= $marcacaoInicialLimite && $horaTmp < $marcacaoFim){
                    unset($horasdisponiveis[$key]);
                }
            }
        }

        if($specialDate){
            foreach ($horasdisponiveis as $key => $value) {
                if($specialDate->tipo == "fechado"){
                    if($value >= $horaEspecialStart && $value < $horaEspecialEnd)
                        unset($horasdisponiveis[$key]);
                }
            }
        }
        
        $now = Carbon::today()->startOfDay();
        $time = Carbon::now()->format('H:i');
        $dateSelected = Carbon::createFromFormat('d/m/Y', $request->date)->startOfDay();
        
        if($now == $dateSelected){
            foreach ($horasdisponiveis as $key => $value) {
                if($value <= $time)
                    unset($horasdisponiveis[$key]);
            }
        }
    
        $semana = SemanasEspeciaisEstetica::where('dia', Carbon::createFromFormat('d/m/Y', $request->date)->format('Y-m-d'))->where('esteticista', $request->cadeira)->first();

        if($semana){
            if($semana->manha == 1 && $semana->tarde == 1){
            }else{
                foreach ($horasdisponiveis as $key => $value) {
                    if($semana->manha == 1){
                        if( $value > Carbon::parse($horaAlmoço)->format("H:i"))
                            unset($horasdisponiveis[$key]);
                    }

                    if($semana->tarde == 1){
                        if( $value < Carbon::parse($horaReabertura)->format("H:i"))
                            unset($horasdisponiveis[$key]);
                    }
                }
            }
        }

        return array_values($horasdisponiveis);
    }
}
