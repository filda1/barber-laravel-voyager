<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\BarberJob;
use App\EsteticaJob;

class BarberJobsController extends Controller
{
    public function getJobs(){

    	$a = BarberJob::where('id', '!=', 0)->where('id', '!=', 2)->get();
	 	return $a;
    }
    public function getJobsEstetica(){

    	$a = EsteticaJob::where('id', '!=', "")->where('ativo', '!=', "0")->orderBy("created_at","asc")->get();
	 	return $a;
    }
}
