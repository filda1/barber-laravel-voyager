<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Galeria;
use App\MarcacoesRecorrente;
use App\MarcacoesRecorrentesEstetica;
use App\Marcacoes;
use App\MarcacoesEstetica;
use App\Slot;
use App\SlotsEstetica;
use App\AppUser;
use App\FechoCaixa;
use App\FechoCaixaEstetica;
use App\SpecialDate;
use App\SpecialDatesEstetica;
use App\Usersignal;
use App\BarberJob;
use App\EsteticaJob;
use App\OneSignal;
use Carbon\Carbon;
use Session;
use App\Sms;
use DB;
use PDF;
use Auth;

class HelperController extends Controller
{
    public function getImages(){

    	$imgs = array();

        $imgsColl = DB::table('galeria')->orderBy('type', 'desc')->get();

        return $imgsColl; 
    }

    /*public function teste(){
        Auth::loginUsingId(6);
    }*/

    public function checkDate($cadeira, $dia){
        $check = SpecialDate::where('date', $dia)->where('cadeira_id', $cadeira)->first();

        if($check)
            return $check;

    }

    public function checkDateEstetica($cadeira, $dia){
        $check = SpecialDatesEstetica::where('date', $dia)->where('cadeira_id', $cadeira)->first();

        if($check)
            return $check;

    }
   //***************************************************************//
    public function sendMarcacoesSms(){

        $now = Carbon::now()->addDays(1)->format('Y-m-d');
        $time = Carbon::now()->format('H:i:s');

        $m = Marcacoes::where('dia', '=', $now)
                      ->where('has_send_sms', '=', 0)
                      ->where('hora_inicio', '<=', $time)
                      ->get();

        foreach ($m as $value) {

            $user = AppUser::find($value->user_id);

            if(!$user)
                continue;

            if($user->numero == null || $user->numero == 0)
                if($value->numero_temp == null)
                    continue;

            $sms = new Sms();

            if($value->numero_temp == null)
                $sms->numero = $user->numero;
            else
                $sms->numero = $value->numero_temp;

             $sms->conteudo = 'Gentleman\'s Barbershop informa que a sua marcacao e amanha as ' . Carbon::parse($value->hora_inicio)->format('H:i') . '. Caso pretenda desmarcar/reagendar ligue 913762183.';
             $sms->save();

             $value->has_send_sms = 1;
             $value->save();

        }
        
    }

  //*************************************************************************//
    public function sendMarcacoesEsteticaSms(){

        $now = Carbon::now()->addDays(1)->format('Y-m-d');
        $time = Carbon::now()->format('H:i:s');

        $m = MarcacoesEstetica::where('dia', '=', $now)
                      ->where('has_send_sms', '=', 0)
                      ->where('hora_inicio', '<=', $time)
                      ->get();

        foreach ($m as $value) {

            $user = AppUser::find($value->user_id);

            if(!$user)
                continue;

            if($user->numero == null || $user->numero == 0)
                if($value->numero_temp == null)
                    continue;

            $sms = new Sms();

            if($value->numero_temp == null)
                $sms->numero = $user->numero;
            else
                $sms->numero = $value->numero_temp;

             $sms->conteudo = 'Joana Moreira Atelier informa que a sua marcação é amanha às ' . Carbon::parse($value->hora_inicio)->format('H:i') . '. Caso pretenda desmarcar/reagendar por favor ligue 913367986. Cumprimentos';
             $sms->save();

             $value->has_send_sms = 1;
             $value->save();

        }
        
    }

    public function saveRecorrente(Request $request){
        if (!$request->ajax()) {
        	$day = Carbon::parse($request->ultimo_dia);

        	$m = new MarcacoesRecorrente();
        	$m->cadeira_id = $request->cadeira_id;
        	$m->user_id = $request->user_id;
        	$m->ultimo_dia = $request->ultimo_dia;
        	$m->hora_inicio = $request->hora_inicio;
        	$m->hora_fim = $request->hora_fim;
        	$m->proximo_dia = $day;
        	$m->semanas = $request->semanas;

            if($request->semanas == 1)
                $timeToLoop = 24;
                
            else if($request->semanas == 2)
                $timeToLoop = 12;
            else
                $timeToLoop = 6;

            $duplicated = array();

            for ($i=0; $i < $timeToLoop; $i++) { 
                $date = $day->addWeeks($request->semanas);

                $marcacao = Marcacoes::where('dia', $date)
                                     ->whereBetween('hora_inicio', [$request->hora_inicio, $request->hora_fim])
                                     ->where('cadeira_id', $request->cadeira_id)
                                     ->first();

                if(!$marcacao){
                    $marcacao = Marcacoes::where('dia', $date)
                                         ->whereBetween('hora_fim', [$request->hora_inicio, $request->hora_fim])
                                         ->where('cadeira_id', $request->cadeira_id)
                                         ->first();
                }

                if($marcacao){
                    $user = AppUser::find($marcacao->user_id);
                    $marcacao->user_id = $user->first_name . " " . $user->last_name;

                    array_push($duplicated, $marcacao);
                    $canSave = false;
                    continue;
                }

                $new = new Marcacoes();
                $new->cadeira_id = $request->cadeira_id;
                $new->user_id = $request->user_id;
                $new->trabalho = '[{"id": "0"}]';
                $new->dia = $date;
                $new->hora_inicio = $request->hora_inicio;
                $new->hora_fim = $request->hora_fim;
                
                $new->save();

            }
            $m->save();
        }

        Session::flash('duplicated', $duplicated);

    	return redirect('admin/marcacoes-recorrentes');
    }

    public function saveRecorrenteEstetica(Request $request){
        if (!$request->ajax()) {
        	$day = Carbon::parse($request->ultimo_dia);

        	$m = new MarcacoesRecorrentesEstetica();
        	$m->cadeira_id = $request->cadeira_id;
        	$m->user_id = $request->user_id;
        	$m->ultimo_dia = $request->ultimo_dia;
        	$m->hora_inicio = $request->hora_inicio;
        	$m->hora_fim = $request->hora_fim;
        	$m->proximo_dia = $day;
        	$m->semanas = $request->semanas;

            if($request->semanas == 1)
                $timeToLoop = 24;
            else if($request->semanas == 2)
                $timeToLoop = 12;
            else
                $timeToLoop = 6;

            $duplicated = array();

            for ($i=0; $i < $timeToLoop; $i++) { 
                $date = $day->addWeeks($request->semanas);

                $marcacao = MarcacoesEstetica::where('dia', $date)
                                     ->whereBetween('hora_inicio', [$request->hora_inicio, $request->hora_fim])
                                     ->where('cadeira_id', $request->cadeira_id)
                                     ->first();

                if(!$marcacao){
                    $marcacao = MarcacoesEstetica::where('dia', $date)
                                         ->whereBetween('hora_fim', [$request->hora_inicio, $request->hora_fim])
                                         ->where('cadeira_id', $request->cadeira_id)
                                         ->first();
                }

                if($marcacao){
                    $user = AppUser::find($marcacao->user_id);
                    $marcacao->user_id = $user->first_name . " " . $user->last_name;

                    array_push($duplicated, $marcacao);
                    $canSave = false;
                    continue;
                }

                $new = new MarcacoesEstetica();
                $new->cadeira_id = $request->cadeira_id;
                $new->user_id = $request->user_id;
                $new->trabalho = '[{"id": "0"}]';
                $new->dia = $date;
                $new->hora_inicio = $request->hora_inicio;
                $new->hora_fim = $request->hora_fim;
                
                $new->save();

            }
            $m->save();
        }

        Session::flash('duplicated', $duplicated);

    	return redirect('admin/marcacoes-recorrentes-estetica');
    }

    public function deleteRecorrente($id){

        $m = MarcacoesRecorrente::find($id);

        $old = Marcacoes::where('cadeira_id', $m->cadeira_id)
                            ->where('user_id', $m->user_id)
                            ->where('hora_inicio', $m->hora_inicio)
                            ->where('hora_fim', $m->hora_fim)
                            ->where('trabalho', '[{"id": "0"}]')
                            ->delete();

        $m->delete();

        return redirect('admin/marcacoes-recorrentes')->with(['message' => "Marcação recorrente eliminada com sucesso.", 'alert-type' => 'success']);
    }

    public function deleteRecorrenteEstetica($id){

        $m = MarcacoesRecorrentesEstetica::find($id);

        $old = MarcacoesEstetica::where('cadeira_id', $m->cadeira_id)
                            ->where('user_id', $m->user_id)
                            ->where('hora_inicio', $m->hora_inicio)
                            ->where('hora_fim', $m->hora_fim)
                            ->where('trabalho', '[{"id": "0"}]')
                            ->delete();

        $m->delete();

        return redirect('admin/marcacoes-recorrentes-estetica')->with(['message' => "Marcação recorrente eliminada com sucesso.", 'alert-type' => 'success']);
    }

    public function updateRecorrente($id, Request $request){
        $day = Carbon::parse($request->ultimo_dia);

        $m = MarcacoesRecorrente::find($id);
        $m->cadeira_id = $request->cadeira_id;
        $m->user_id = $request->user_id;
        $m->ultimo_dia = $request->ultimo_dia;
        $m->hora_inicio = $request->hora_inicio;
        $m->hora_fim = $request->hora_fim;
        $m->proximo_dia = $day;
        $m->semanas = $request->semanas;


        $new = new Marcacoes();
        $new->cadeira_id = $request->cadeira_id;
        $new->user_id = $request->user_id;
        $new->trabalho = '[{"id": "0"}]';
        $new->dia = $day->addWeeks($request->semanas);
        $new->hora_inicio = $request->hora_inicio;
        $new->hora_fim = $request->hora_fim;
        
        $m->save();
        $new->save();

        return redirect('admin/marcacoes-recorrentes');
    }

    public function updateRecorrenteEstetica($id, Request $request){
        $day = Carbon::parse($request->ultimo_dia);

        $m = MarcacoesRecorrentesEstetica::find($id);
        $m->cadeira_id = $request->cadeira_id;
        $m->user_id = $request->user_id;
        $m->ultimo_dia = $request->ultimo_dia;
        $m->hora_inicio = $request->hora_inicio;
        $m->hora_fim = $request->hora_fim;
        $m->proximo_dia = $day;
        $m->semanas = $request->semanas;


        $new = new MarcacoesEstetica();
        $new->cadeira_id = $request->cadeira_id;
        $new->user_id = $request->user_id;
        $new->trabalho = '[{"id": "0"}]';
        $new->dia = $day->addWeeks($request->semanas);
        $new->hora_inicio = $request->hora_inicio;
        $new->hora_fim = $request->hora_fim;
        
        $m->save();
        $new->save();

        return redirect('admin/marcacoes-recorrentes-estetica');
    }

    public function specialDateCheck(Request $request){

        if($request->diaTodo == "on"){
            $marcacoes = Marcacoes::where('dia', $request->dia)
                              ->where('cadeira_id', $request->cadeira)
                              ->get();
        }else{
            $marcacao = Marcacoes::where('dia', $request->dia)
                                     ->whereBetween('hora_inicio', [$request->hora_inicio, $request->hora_fim])
                                     ->where('cadeira_id', $request->cadeira);


            $marcacoes = Marcacoes::where('dia', $request->dia)
                                     ->whereBetween('hora_fim', [$request->hora_inicio, $request->hora_fim])
                                     ->where('cadeira_id', $request->cadeira)
                                     ->union($marcacao)
                                     ->get();
        }

        foreach ($marcacoes as $value) {
            $user = AppUser::find($value->user_id);
            $value->user_id = $user->first_name . " " . $user->last_name;
        }
        
        return $marcacoes;
    }

    public function specialDateDelete(Request $request){
        $ids = explode(",", $request->ids);

        foreach ($ids as $value) {
            $marcacao = Marcacoes::find($value);

            if($marcacao)
                $marcacao->delete();
        }

        return "ok";
    }

    public function updateMarcacao($id, Request $request){

        $cleanJson = array();

        foreach ($request->trabalho as $key => $value) {
            $cleanJson[$key]["id"] = $value;
        }

        $new = Marcacoes::find($id);
        $new->cadeira_id = $request->cadeira_id;
        $new->user_id = $request->user_id;
        $new->trabalho = json_encode($cleanJson);
        $new->dia = $request->dia;
        $new->hora_inicio = $request->hora_inicio;
        $new->hora_fim = $request->hora_fim;
        $new->valor_final = $request->valor_final;
        $new->dia_fechado = $request->dia_fechado;
        $new->nome_temp = $request->nome_temp;
        $new->numero_temp = $request->numero_temp;
        $new->save();

        Session::flash("date", $request->dia);

        return redirect('admin/marcacoes');
    }

    public function updateMarcacaoEstetica($id, Request $request){

        $cleanJson = array();

        foreach ($request->trabalho as $key => $value) {
            $cleanJson[$key]["id"] = $value;
        }

        $new = MarcacoesEstetica::find($id);
        $new->cadeira_id = $request->cadeira_id;
        $new->user_id = $request->user_id;
        $new->trabalho = json_encode($cleanJson);
        $new->dia = $request->dia;
        $new->hora_inicio = $request->hora_inicio;
        $new->hora_fim = $request->hora_fim;
        $new->valor_final = $request->valor_final;
        $new->dia_fechado = $request->dia_fechado;
        $new->nome_temp = $request->nome_temp;
        $new->numero_temp = $request->numero_temp;
        $new->save();

        Session::flash("date", $request->dia);

        return redirect('admin/marcacoes-estetica');
    }

    public function newMarcacao(Request $request){
        if (!$request->ajax()) {
            $cleanJson = array();

            foreach ($request->trabalho as $key => $value) {
                $cleanJson[$key]["id"] = $value;
            }

            if($request->cadeira_id == null)
                return redirect()->back()->withInput()->with(['message' => "Não selecionou um barbeiro.", 'alert-type' => 'warning']);
            
            if($request->user_id == null)
                return redirect()->back()->withInput()->with(['message' => "Não selecionou um utilzador.", 'alert-type' => 'warning']);

            $new = new Marcacoes();
            $new->cadeira_id = $request->cadeira_id;
            $new->user_id = $request->user_id;
            $new->trabalho = json_encode($cleanJson);
            $new->dia = $request->dia;
            $new->hora_inicio = $request->hora_inicio;
            $new->hora_fim = $request->hora_fim;
            $new->nome_temp = $request->nome_temp;
            $new->numero_temp = $request->numero_temp;
            $new->save();
        }
        return redirect('admin/marcacoes');
    }

    public function newMarcacaoEstetica(Request $request){
        if (!$request->ajax()) {
            $cleanJson = array();

            foreach ($request->trabalho as $key => $value) {
                $cleanJson[$key]["id"] = $value;
            }

            $new = new MarcacoesEstetica();
            $new->cadeira_id = $request->cadeira_id;
            $new->user_id = $request->user_id;
            $new->trabalho = json_encode($cleanJson);
            $new->dia = $request->dia;
            $new->hora_inicio = $request->hora_inicio;
            $new->hora_fim = $request->hora_fim;
            $new->nome_temp = $request->nome_temp;
            $new->numero_temp = $request->numero_temp;
            $new->save();
        }
        return redirect('admin/marcacoes-estetica');
    }

    public function fecharCaixa(){
        $s = Slot::all();
        $s2 = SlotsEstetica::all();


        return view('fecho.index', ['s' => $s,'s2' => $s2]);
    }

    public function getFecho($date, $cadeira){
        $f = DB::table('marcacoes')
                ->where('dia', '=', $date)
                ->where('cadeira_id', '=', $cadeira)
                ->where('dia_fechado', '!=', NULL)
                ->get();

        $v = DB::table('fecho_caixa')
                ->where('dia', '=', $date)
                ->where('cadeira_id', '=', $cadeira)
                ->orderBy('created_at', 'desc')
                ->first();


        $id = 0;
        foreach ($f as $value) {
            $id=$value->cadeira_id;
            $value->cadeira_id = Slot::find($value->cadeira_id)->slot;
            $value->user_id = AppUser::find($value->user_id)->first_name . " " . AppUser::find($value->user_id)->last_name;
        }

        $returnHTML = view('fecho.results')->with('a', $f)->with('id', $id)->with('v', $v)->render();
        return response()->json($returnHTML);
    }

    public function getFechoEstetica($date, $cadeira){
        $f = DB::table('marcacoes_estetica')
                ->where('dia', '=', $date)
                ->where('cadeira_id', '=', $cadeira)
                ->where('dia_fechado', '!=', NULL)
                ->get();

        $v = DB::table('fecho_caixa_estetica')
                ->where('dia', '=', $date)
                ->where('cadeira_id', '=', $cadeira)
                ->orderBy('created_at', 'desc')
                ->first();


        $id = 0;
        foreach ($f as $value) {
            $id=$value->cadeira_id;
            $value->cadeira_id = SlotsEstetica::find($value->cadeira_id)->slot;
            $value->user_id = AppUser::find($value->user_id)->first_name . " " . AppUser::find($value->user_id)->last_name;
        }

        $returnHTML = view('fecho.results')->with('a', $f)->with('id', $id)->with('v', $v)->render();
        return response()->json($returnHTML);
    }

    public function fechoCaixaPrint($date, $cadeira){
        $f = DB::table('marcacoes')
                ->where('dia_fechado', '=', $date)
                ->where('cadeira_id', '=', $cadeira)
                ->get();


        foreach ($f as $value) {
            $value->cadeira_id = Slot::find($value->cadeira_id)->slot;
            $value->user_id = AppUser::find($value->user_id)->first_name . " " . AppUser::find($value->user_id)->last_name;
        }


        $pdf = PDF::loadHTML(view('fecho.results')->with('a', $f)->with('load', $load="1"));
        $pdf->setPaper(array(0,0,600,1123));
        return $pdf->stream('fecho.results');
    }
    
    public function fechoCaixaPrintEstetica($date, $cadeira){
        $f = DB::table('marcacoes_estetica')
                ->where('dia_fechado', '=', $date)
                ->where('cadeira_id', '=', $cadeira)
                ->get();


        foreach ($f as $value) {
            $value->cadeira_id = SlotsEstetica::find($value->cadeira_id)->slot;
            $value->user_id = AppUser::find($value->user_id)->first_name . " " . AppUser::find($value->user_id)->last_name;
        }


        $pdf = PDF::loadHTML(view('fecho.results')->with('a', $f)->with('load', $load="1"));
        $pdf->setPaper(array(0,0,600,1123));
        return $pdf->stream('fecho.results');
    }

    public function saveCaixa(Request $request){
        $new = new FechoCaixa();
        $new->dia = $request->dia;
        $new->cadeira_id = $request->cadeira_id;
        $new->valor = $request->valor;
        $new->save();

        return "ok";
    }

    public function saveCaixaEstetica(Request $request){
        $new = new FechoCaixaEstetica();
        $new->dia = $request->dia;
        $new->cadeira_id = $request->cadeira_id;
        $new->valor = $request->valor;
        $new->save();

        return "ok";
    }

    public function getMaracaoes(Request $request){
        $m = Marcacoes::where('dia', '>=', $request->startDate)->where('dia', '<=', $request->endDate)->where('cadeira_id', $request->cadeiraID)->get();

        foreach ($m as $value) {
            $u = AppUser::find($value->user_id);

            if(!$u)
                continue;

            $value->user_id = $u->first_name . " " . $u->last_name;
            $trabalhos = json_decode($value->trabalho);
             $value->trabalho = "";
            foreach ($trabalhos as $trabalho) {
                if($trabalho->id == 0){
                    $value->trabalho = "\n" . "Marcação Recorrente";
                    continue;
                }

                $tb = BarberJob::find($trabalho->id);

                if($tb == null){
                    $value->trabalho = "\n" . "Erro a obter Marcação";
                    continue;
                }

                $value->trabalho = $value->trabalho ."\n" . $tb->nome;
            }
            

        }

        return $m;
    }

    public function getMaracaoesEstetica(Request $request){
        $m = MarcacoesEstetica::where('dia', '>=', $request->startDate)->where('dia', '<=', $request->endDate)->where('cadeira_id', $request->cadeiraID)->get();

        foreach ($m as $value) {
            $u = AppUser::find($value->user_id);

            if(!$u)
                continue;

            $value->user_id = $u->first_name . " " . $u->last_name;
            $trabalhos = json_decode($value->trabalho);
             $value->trabalho = "";
            foreach ($trabalhos as $trabalho) {
                if($trabalho->id == 0){
                    $value->trabalho = "\n" . "Marcação Recorrente";
                    continue;
                }

                $tb = EsteticaJob::find($trabalho->id);

                if($tb == null){
                    $value->trabalho = "\n" . "Erro a obter Marcação";
                    continue;
                }

                $value->trabalho = $value->trabalho ."\n" . $tb->nome;
            }
            

        }

        return $m;
    }

    public function updateMarcacoesRecorrentes(){
        $now = Carbon::now()->format("Y-m-d");
        $a = MarcacoesRecorrente::where('proximo_dia', '<=', $now)->get();

        foreach ($a as $value) {
            $lastDay = Carbon::parse($value->proximo_dia);
            $value->ultimo_dia = $value->proximo_dia;
            
            $nextDay = $lastDay->addWeeks($value->semanas);
            $value->proximo_dia = $nextDay;

            $checkMarcacao = Marcacoes::where('cadeira_id', $value->cadeira_id)->where('user_id', $value->user_id)->where('dia', $nextDay)->where('hora_inicio', $value->hora_inicio)->where('hora_fim', $value->hora_fim)->get();

            if($checkMarcacao){
                continue;
            }

            $new = new Marcacoes();
            $new->cadeira_id = $value->cadeira_id;
            $new->user_id = $value->user_id;
            $new->trabalho = '[{"id": "0"}]';
            $new->dia = $nextDay;
            $new->hora_inicio = $value->hora_inicio;
            $new->hora_fim = $value->hora_fim;

            $new->save();
            $value->save();
        }
    }

    public function updateMarcacoesRecorrentesEstetica(){
        $now = Carbon::now()->format("Y-m-d");
        $a = MarcacoesRecorrentesEstetica::where('proximo_dia', '<=', $now)->get();

        foreach ($a as $value) {
            $lastDay = Carbon::parse($value->proximo_dia);
            $value->ultimo_dia = $value->proximo_dia;
            
            $nextDay = $lastDay->addWeeks($value->semanas);
            $value->proximo_dia = $nextDay;

            $checkMarcacao = MarcacoesEstetica::where('cadeira_id', $value->cadeira_id)->where('user_id', $value->user_id)->where('dia', $nextDay)->where('hora_inicio', $value->hora_inicio)->where('hora_fim', $value->hora_fim)->get();

            if($checkMarcacao){
                continue;
            }

            $new = new MarcacoesEstetica();
            $new->cadeira_id = $value->cadeira_id;
            $new->user_id = $value->user_id;
            $new->trabalho = '[{"id": "0"}]';
            $new->dia = $nextDay;
            $new->hora_inicio = $value->hora_inicio;
            $new->hora_fim = $value->hora_fim;

            $new->save();
            $value->save();
        }
    }

    public function updateDatasEspeciais(){
        $now = Carbon::now()->format("Y-m-d");
        $a = SpecialDate::where('date', '<=', $now)->get();

        foreach ($a as $value) {
            $value->date = Carbon::parse($value->date)->addWeeks($value->semanas_repetir);
            $value->save();
        }
    }

    public function updateDatasEspeciaisEstetica(){
        $now = Carbon::now()->format("Y-m-d");
        $a = SpecialDatesEstetica::where('date', '<=', $now)->get();

        foreach ($a as $value) {
            $value->date = Carbon::parse($value->date)->addWeeks($value->semanas_repetir);
            $value->save();
        }
    }

    public function getSMS($tag){

        if($tag != "sWJqWKKFqDx4hVq14cnahw8zCB7Zun7ioWw1YpHCHl10GH81S01Gk")
            return;

        $sms = Sms::where('estado', '1')->first();

        if($sms){
            $sms->estado=2;
            $sms->save();
            return $sms->toJson();
        }
        else
            return "false";
    }

    public function setSMS($id){
        $sms=Sms::find($id);
        $sms->estado=3;
        $sms->save();
        return "true";
    }

    public function centroMensagens(){
        $users = AppUser::all();

        return view('mensagens.index', ['users'=>$users]);
    }

    public function sendSMS(Request $req){

        $ids = explode(',', $req->toSend);

        foreach ($ids as $value) {
            $user = AppUser::find($value);

            if(!$user)
                continue;

            if($user->numero == null)
                continue;

            $sms = new Sms();
            $sms->numero = $user->numero;
            $sms->conteudo = $req->textToSend;
            $sms->save();
        }

        return redirect('centro-mensagens')->with(['message' => "As mensagens serão enviadas brevemente.", 'alert-type' => 'success']);
    }

    public function sendNotification(Request $req){
        $ids = explode(',', $req->toSend);

        foreach ($ids as $value) {
            $user = AppUser::find($value);

            if(!$user)
                continue;

            if($user->onesignal_id == null)
                continue;

            $heading = array(
            "en" => $req->title
            );

            $content = array(
            "en" => $req->textToSend 
            );

            $fields = array(
                'app_id' => "3f305ede-5599-46d6-a753-f036c4149b45",
                'include_player_ids' => array($user->onesignal_id),
                //'included_segments' => array('All'),
                'data' => array("foo" => "bar"),
                'large_icon' =>"ic_launcher_round.png",
                'contents' => $content,
                'headings' => $heading
            );

            $fields = json_encode($fields);

            $ch = curl_init();
            curl_setopt($ch, CURLOPT_URL, "https://onesignal.com/api/v1/notifications");
            curl_setopt($ch, CURLOPT_HTTPHEADER, array('Content-Type: application/json; charset=utf-8',
                                                       'Authorization: Basic NTlmYmVkN2EtYWFiNS00ZjVlLTgwOTgtZTNjZjQ2OWU4NzZk'));
            curl_setopt($ch, CURLOPT_RETURNTRANSFER, TRUE);
            curl_setopt($ch, CURLOPT_HEADER, FALSE);
            curl_setopt($ch, CURLOPT_POST, TRUE);
            curl_setopt($ch, CURLOPT_POSTFIELDS, $fields);
            curl_setopt($ch, CURLOPT_SSL_VERIFYPEER, FALSE);    

            $response = curl_exec($ch);
            curl_close($ch);
        }

        return redirect('centro-mensagens')->with(['message' => "As mensagens serão enviadas brevemente.", 'alert-type' => 'success']);
    }

    public function relatorio(){
        
        $barb = Slot::all();

        return view('relatorios.browse', compact('barb'));
    }
    
    public function getrelatorio(Request $request){
        
        //$tipo = "Corte Cabelo";
        $users = Marcacoes::whereBetween('dia', [$request->data_ini, $request->data_fim])->where('cadeira_id', $request->barb)->where('dia_fechado', '!=', NULL)->get();
        /*$tipo2 = "Corte Cabelo e Barba";
        $users2 = Marcacoes::whereBetween('dia', [$request->data_ini, $request->data_fim])->where('cadeira_id', $request->barb)->where('dia_fechado', '!=', NULL)>where('trabalho','[{"id": "9"}]')->get();
        $tipo3 = "Barba";
        $users3 = Marcacoes::whereBetween('dia', [$request->data_ini, $request->data_fim])->where('cadeira_id', $request->barb)->where('dia_fechado', '!=', NULL)>where('trabalho','[{"id": "10"}]')->get();
        */
        $resultados = array();
        foreach ($users as $value) {
            $u = AppUser::find($value->user_id);
            $value->user_id = $u->first_name . " " . $u->last_name;

            $trabalhos = json_decode($value->trabalho);
            $value->trabalho = "";
            foreach ($trabalhos as $trabalho) {
                if($trabalho->id == 0){
                    $value->trabalho = "\n" . "Marcação Recorrente";
                    continue;
                }

                $tb = BarberJob::find($trabalho->id);

                if($tb == null){
                    $value->trabalho = "\n" . "Erro a obter Marcação";
                    continue;
                }

                $value->trabalho = $value->trabalho ."\n" . $tb->nome;
            }

            if (isset($resultados[$value->trabalho])) {
                $index = count($resultados[$value->trabalho]);
            }else{
                $index = 0;
            }

            $resultados[$value->trabalho][$index] = $value;

        }
        $returnHTML = view('relatorios.relatorio', compact('resultados'))->render();

        return $returnHTML;

        /*$returnHTML2 = view('relatorios.linha', compact('users2','tipo2'))->render();
        $returnHTML3 = view('relatorios.linha', compact('users3','tipo3'))->render();
        
        $total = $returnHTML.''.$returnHTML2.''.$returnHTML3;*/

    }

    public function saveOneSignal(Request $request){
        $check = Usersignal::where('onesginal_id',$request->oneSignal)->where('cadeira_id',$request->user)->get();
        $count = count($check);
        if($count==0 && $request->oneSignal!=NULL){
            $new = new Usersignal();
            $new->onesginal_id = $request->oneSignal;
            $new->cadeira_id = $request->user;
            if($new->save()){
                return "guardou";
            }else{
                return "nao";
            }
        }
    }
    
}
