<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Marcacoes;
use App\MarcacoesEstetica;
use App\Slot;
use App\SlotsEstetica;
use App\AppUser;
use Log;

class OneSignalController extends Controller
{
    public function sendMarcacoesNotifications(){
		$now = \Carbon\Carbon::now()->format('Y-m-d');
		$hour = \Carbon\Carbon::now()->addHours(2)->format('H:i:s');

		$m = Marcacoes::where('dia', '=', $now)
    	 				->where('hora_inicio', '<=', $hour)
    	 				->where('has_send_noti', '=', 0)->get();

		foreach ($m as $value) {

    	 	$heading = array(
	         	"en" => 'Não se esqueça da sua Marcação'
			);

	     	$content = array(
	         	"en" => 'Tem uma Marcação para o dia ' . $value->dia . ' com o barbeiro '. Slot::find($value->cadeira_id)->slot 
			);

			$fields = array(
				'app_id' => "3f305ede-5599-46d6-a753-f036c4149b45",
				'include_player_ids' => array(AppUser::find($value->user_id)->onesignal_id),
				'included_segments' => array('All'),
				'data' => array("foo" => "bar"),
				'large_icon' =>"ic_launcher_round.png",
				'contents' => $content,
				'headings' => $heading
			);

			$fields = json_encode($fields);

			$ch = curl_init();
			curl_setopt($ch, CURLOPT_URL, "https:onesignal.com/api/v1/notifications");
			curl_setopt($ch, CURLOPT_HTTPHEADER, array('Content-Type: application/json; charset=utf-8',
													'Authorization: Basic NTlmYmVkN2EtYWFiNS00ZjVlLTgwOTgtZTNjZjQ2OWU4NzZk'));
			curl_setopt($ch, CURLOPT_RETURNTRANSFER, TRUE);
			curl_setopt($ch, CURLOPT_HEADER, FALSE);
			curl_setopt($ch, CURLOPT_POST, TRUE);
			curl_setopt($ch, CURLOPT_POSTFIELDS, $fields);
			curl_setopt($ch, CURLOPT_SSL_VERIFYPEER, FALSE);    

			$response = curl_exec($ch);
			curl_close($ch);

			$value->has_send_noti = 1;
    	 	$value->save();
    	}
    	
	}

	public function sendMarcacoesNotificationsEstetica(){

		$now = \Carbon\Carbon::now()->format('Y-m-d');
		$hour = \Carbon\Carbon::now()->addHours(2)->format('H:i:s');

		$m = MarcacoesEstetica::where('dia', '=', $now)
						->where('hora_inicio', '<=', $hour)
						->where('has_send_noti', '=', 0)->get();

		foreach ($m as $value) {

			$heading = array(
			"en" => 'Não se esqueça da sua Marcação'
			);

			$content = array(
			"en" => 'Tem uma Marcação para o dia ' . $value->dia . ' com a esteticista '. SlotsEstetica::find($value->cadeira_id)->slot 
			);

			$fields = array(
				'app_id' => "3f305ede-5599-46d6-a753-f036c4149b45",
				'include_player_ids' => array(AppUser::find($value->user_id)->onesignal_id),
				'included_segments' => array('All'),
				'data' => array("foo" => "bar"),
				'large_icon' =>"ic_launcher_round.png",
				'contents' => $content,
				'headings' => $heading
			);

			$fields = json_encode($fields);

			$ch = curl_init();
			curl_setopt($ch, CURLOPT_URL, "https:onesignal.com/api/v1/notifications");
			curl_setopt($ch, CURLOPT_HTTPHEADER, array('Content-Type: application/json; charset=utf-8',
													   'Authorization: Basic NTlmYmVkN2EtYWFiNS00ZjVlLTgwOTgtZTNjZjQ2OWU4NzZk'));
			curl_setopt($ch, CURLOPT_RETURNTRANSFER, TRUE);
			curl_setopt($ch, CURLOPT_HEADER, FALSE);
			curl_setopt($ch, CURLOPT_POST, TRUE);
			curl_setopt($ch, CURLOPT_POSTFIELDS, $fields);
			curl_setopt($ch, CURLOPT_SSL_VERIFYPEER, FALSE);    

			$response = curl_exec($ch);
			curl_close($ch);

			$value->has_send_noti = 1;
			$value->save();
	   }
	   
   	}

    public function sendEndMarcacoesNotifications(){

    	 $now = \Carbon\Carbon::now()->format('Y-m-d');
    	 $hour = \Carbon\Carbon::now()->format('H:i:s');

    	 $m = Marcacoes::where('dia', '=', $now)
    	 				->where('hora_fim', '<=', $hour)
						 ->where('has_send_foto_noti', '=', 0)->get();

    	 foreach ($m as $value) {

    	 	$heading = array(
	         "en" => 'Obrigado pela sua preferência!'
	         );

	     	$content = array(
	         "en" => 'Aproveite e fotografe o seu novo estilo!' 
	         );

	         $fields = array(
		         'app_id' => "3f305ede-5599-46d6-a753-f036c4149b45",
	     		'include_player_ids' => array(AppUser::find($value->user_id)->onesignal_id),
		         'included_segments' => array('All'),
		         'data' => array("foo" => "bar"),
		         'large_icon' =>"ic_launcher_round.png",
		         'contents' => $content,
		         'headings' => $heading
		     );

		     $fields = json_encode($fields);

		     $ch = curl_init();
		     curl_setopt($ch, CURLOPT_URL, "https:onesignal.com/api/v1/notifications");
		     curl_setopt($ch, CURLOPT_HTTPHEADER, array('Content-Type: application/json; charset=utf-8',
		                                                'Authorization: Basic NTlmYmVkN2EtYWFiNS00ZjVlLTgwOTgtZTNjZjQ2OWU4NzZk'));
		     curl_setopt($ch, CURLOPT_RETURNTRANSFER, TRUE);
		     curl_setopt($ch, CURLOPT_HEADER, FALSE);
		     curl_setopt($ch, CURLOPT_POST, TRUE);
		     curl_setopt($ch, CURLOPT_POSTFIELDS, $fields);
		     curl_setopt($ch, CURLOPT_SSL_VERIFYPEER, FALSE);    

		     $response = curl_exec($ch);
		     curl_close($ch);

		     $value->has_send_foto_noti = 1;
    	 	$value->save();
		}
    	
	}
	
	public function sendEndMarcacoesNotificationsEstetica(){

		$now = \Carbon\Carbon::now()->format('Y-m-d');
		$hour = \Carbon\Carbon::now()->format('H:i:s');

		$m = MarcacoesEstetica::where('dia', '=', $now)
						->where('hora_fim', '<=', $hour)
						->where('has_send_foto_noti', '=', 0)->get();

		foreach ($m as $value) {

			$heading = array(
			"en" => 'Obrigado pela sua preferência!'
			);

			$content = array(
			"en" => '' 
			);

			$fields = array(
				'app_id' => "3f305ede-5599-46d6-a753-f036c4149b45",
				'include_player_ids' => array(AppUser::find($value->user_id)->onesignal_id),
				'included_segments' => array('All'),
				'data' => array("foo" => "bar"),
				'large_icon' =>"ic_launcher_round.png",
				'contents' => $content,
				'headings' => $heading
			);

			$fields = json_encode($fields);

			$ch = curl_init();
			curl_setopt($ch, CURLOPT_URL, "https:onesignal.com/api/v1/notifications");
			curl_setopt($ch, CURLOPT_HTTPHEADER, array('Content-Type: application/json; charset=utf-8',
													   'Authorization: Basic NTlmYmVkN2EtYWFiNS00ZjVlLTgwOTgtZTNjZjQ2OWU4NzZk'));
			curl_setopt($ch, CURLOPT_RETURNTRANSFER, TRUE);
			curl_setopt($ch, CURLOPT_HEADER, FALSE);
			curl_setopt($ch, CURLOPT_POST, TRUE);
			curl_setopt($ch, CURLOPT_POSTFIELDS, $fields);
			curl_setopt($ch, CURLOPT_SSL_VERIFYPEER, FALSE);    

			$response = curl_exec($ch);
			curl_close($ch);

			$value->has_send_foto_noti = 1;
			$value->save();
	   }
	   
   	}
}
