<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Illuminate\Support\Facades\Crypt;
use Intervention\Image\Facades\Image;
use App\AppUser;
use App\Slot;
use Carbon\Carbon;
use Storage;
use Hash;
use Log;
use Mail;

class AppUserController extends Controller
{
    public function registerUser(Request $request){
        $output = "";
        $user = new AppUser();

        parse_str($request->info, $output);


         if($output["imageURL"] == "useDefault") {
            $image = $request["image"];

            $image = preg_replace('/data:image\/(.*?);base64,/','',$image);
            $image = str_replace(' ', '+', $image);
      
            $imageName = 'image_' . time() . '.' . 'jpeg'; //generating unique file name;
            Storage::disk(config('voyager.storage.disk'))->put("users/".$imageName, base64_decode($image));
            
            $user->image = "users/".$imageName;
         }else{
            $imageName = 'image_' . time() . '.' . 'jpeg';

            Image::make($output["imageURL"])->save(public_path('storage/users/' . $imageName));
            $user->image = "users/".$imageName;
         }

        if(AppUser::where('email', $output["email"])->count() > 0)
            return "email";
    	
    	$user->first_name = $output["first_name"];
    	$user->last_name = $output["last_name"];
    	$user->email = $output["email"];
        $user->numero = $output["number"];
    	$user->password = Hash::make($output["password"]);

        if($output["dateSelected"] != "na")
            $user->birthday = Carbon::createFromFormat('d/m/Y', $output["birthday"]);
        
    	$r = $user->save();

    	if($r){
    		return "success";
    	}else{
    		return "error";
    	}
    }

    public function changePassword(Request $request){

        $u = AppUser::where('email', $request->email)->first();

        if(Hash::check($request->oldPassword, $u->password)){
            $u->password = Hash::make($request->newPassword);
            $u->save();

            return "success";
        }else{
            return "passwordError";
        }
    }

    public function newPassword(Request $request){
        $u = AppUser::where('email', $request->email)->first();
        
        $id = $u->id = Crypt::encryptString($u->id);

        Mail::send('email.confirm_email', ["var"=>$u, "id"=> $id], function ($message) use ($u){
            $message->to($u->email)->subject("Alteração de palavra passe"); 
        });

        if(count(Mail::failures()) > 0){
            return 'erro';
        }
        else{
            return 'sucesso';
        }
    }

    public function resetPassword($id){

        try {
            $decrypted = Crypt::decryptString($id);

            $u = AppUser::find($decrypted);

            return view('pass.index', ['s' => $u]);

        } catch (DecryptException $e) {
            //
        }

    }
    public function resetPassword2(Request $request){

        $u = AppUser::find($request->id);

        if($request->pass1 === $request->pass2){
            $u->password = Hash::make($request->pass1);
            if($u->save()){
                $i=1;
                return view('pass.success', ['s' => $u, 'i' => $i]);
            }
            else{
                $i=0;
                return view('pass.success', ['s' => $u, 'i' => $i]);
            }
        }
        else{
            $i=0;
            return view('pass.success', ['s' => $u, 'i' => $i]);
        }
    }

    public function loginUser(Request $request){
        $u = AppUser::where('email', $request->email)->first();


        if(!$u){

            $u = Slot::where('slot', $request->email)->first();
            if($u){
                if($u->is_blocked == 1)
                    return "blocked";

                if(Hash::check($request->password, $u->password)){
                    $u->login_token = str_random(100);
                    $u->save();
                    $u["isBarber"] = 1;
                    return $u;
                }
            }
            return "incorrect";
        }else{
                if($u->is_blocked == 1)
                    return "blocked";
                
               if(Hash::check($request->password, $u->password)){
                $u->login_token = str_random(100);
                $u->save();
                $u["isBarber"] = 0;
                return $u;
            }else{
                return "incorrect";
            }

        }
    }

    public function loginUserWithToken(Request $request){

        if($request->token == null || $request->token == ""){
            return "incorrect";
        }

        $u = AppUser::where('login_token', $request->token)->first();

        
        if(!$u){
            $u = Slot::where('login_token', $request->token)->first();
            if($u){
                if($u->is_blocked == 1)
                    return "blocked";

                $u->login_token = str_random(100);
                $u->save();
                $u["isBarber"] = 1;
                return $u;
            }
            return "incorrect";
        }else{
            if($u->is_blocked == 1)
                return "blocked";

            $u->login_token = str_random(100);
            $u->save();
            $u["isBarber"] = 0;
            return $u;
        }
    }

    public function upload(Request $request)
    {
        $fullFilename = null;
        $resizeWidth = 1800;
        $resizeHeight = null;
        $slug = $request->input('type_slug');
        $file = $request->file('image');

        $path = $slug.'/'.date('F').date('Y').'/';

        $filename = basename($file->getClientOriginalName(), '.'.$file->getClientOriginalExtension());
        $filename_counter = 1;

        // Make sure the filename does not exist, if it does make sure to add a number to the end 1, 2, 3, etc...
        while (Storage::disk(config('voyager.storage.disk'))->exists($path.$filename.'.'.$file->getClientOriginalExtension())) {
            $filename = basename($file->getClientOriginalName(), '.'.$file->getClientOriginalExtension()).(string) ($filename_counter++);
        }

        $fullPath = $path.$filename.'.'.$file->getClientOriginalExtension();

        $ext = $file->guessClientExtension();

        if (in_array($ext, ['jpeg', 'jpg', 'png', 'gif'])) {
            $image = Image::make($file)
                ->resize($resizeWidth, $resizeHeight, function (Constraint $constraint) {
                    $constraint->aspectRatio();
                    $constraint->upsize();
                })
                ->encode($file->getClientOriginalExtension(), 75);

            // move uploaded file from temp to uploads directory
            if (Storage::disk(config('voyager.storage.disk'))->put($fullPath, (string) $image, 'public')) {
                $status = __('voyager::media.success_uploading');
                $fullFilename = $fullPath;
            } else {
                $status = __('voyager::media.error_uploading');
            }
        } else {
            $status = __('voyager::media.uploading_wrong_type');
        }

        // echo out script that TinyMCE can handle and update the image in the editor
        return "<script> parent.helpers.setImageValue('".Voyager::image($fullFilename)."'); </script>";
    }

    public function updateOnesignalId(Request $request){
        $u = AppUser::find($request->user_id);
        $u->onesignal_id = $request->onesignal_id;
        $u->save();

        return "ok";
    }

}
