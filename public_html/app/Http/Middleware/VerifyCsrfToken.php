<?php

namespace App\Http\Middleware;

use Illuminate\Foundation\Http\Middleware\VerifyCsrfToken as Middleware;

class VerifyCsrfToken extends Middleware
{
    /**
     * Indicates whether the XSRF-TOKEN cookie should be set on the response.
     *
     * @var bool
     */
    protected $addHttpCookie = true;

    /**
     * The URIs that should be excluded from CSRF verification.
     *
     * @var array
     */
    protected $except = [
        '/api/user/new',
        '/api/user/login',
        '/api/marcacoes/cadeiras',
        '/api/marcacoes/cadeiras/estetica',
        '/api/user/login/with-token',
        '/api/marcacoes/verficar-disponiblidade',
        '/api/marcacoes/verficar-disponiblidade/estetica',
        '/api/marcacoes/new',
        '/api/marcacoes/new/estetica',
        '/api/marcacoes/get/day',
        '/api/marcacoes/get/estetica/day',
        '/api/barber/get/day',
        '/api/estetica/get/day',
        '/api/marcacao/upload-image',
        '/api/marcacao/estetica/upload-image',
        '/api/user/change-password',
        '/api/user/new-password'
    ];
}
