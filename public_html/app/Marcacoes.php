<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;

class Marcacoes extends Model
{
 	protected $table = 'marcacoes';
 	use SoftDeletes;
    protected $dates = ['deleted_at'];
}
