@if(count($a) > 0)
	@if(isset($load))
		<span style="font-size: 15px; float: right;">{{$a[0]->dia_fechado}}</span>
	@endif
	<h3 style="margin-top: 2em;">{{$a[0]->cadeira_id}}</h3>
	<table class="table" style="width: 100%;">
  		<thead>
    		<tr>
				<th>Cliente</th>
				<th>Valor</th>
    		</tr>
  		</thead>
  		<tbody>
		@php
		$total = 0;
		$totalFalse = 0;
		@endphp

		@foreach($a as $c)
			@php
			$total = $total + $c->valor_final;
			$totalFalse = $totalFalse + $c->valor_final;
			@endphp
			<tr>
				<td>{{$c->user_id}} @if($c->user_id == "Cliente Final")({{$c->nome_temp}}) @endif</td>
				<td>{{$c->valor_final}}</td>
			</tr>
			@if($loop->last)
				<tr style="background: black; color: white;">
					<td>Total</td>
					<td id="totalFake">{{$totalFalse}} <span id="total" style="opacity: 0;">{{$total}}</span></td>
				</tr>
				@if(!isset($load))
				<tr style="background: #525252; color: white;">
					<td>Valor potencial</td>
					<td><input id="lucro" disabled type="number" style="color: black;" placeholder="Valor a Ganhar"> <input disabled type="number" id="otherPercent" placeholder="Percentagem a Ganhar" style="color: black;"></td>
				</tr>
				<tr style="background: #525252; color: white;">
					<td>Valor pago</td>
					@if(isset($v))
					<td><input value="" readonly id="pay" type="number" onkeyup="calc()" style="color: black;"> <input type="number" value="{{ $v->valor }}" onkeyup="calc2()" id="myPercent" style="color: black;"></td>
					@else
					<td><input id="pay" type="number" readonly onkeyup="calc()" style="color: black;"> <input type="number" onkeyup="calc2()" id="myPercent" style="color: black;"></td>
					@endif
				</tr>
				@endif
			@endif
  		@endforeach
  		</tbody>
	</table>

	@if(isset($a))
		@if(!isset($load))
			@if(Auth::user()->role_id == 3 || Auth::user()->role_id == 1)
			<div class="pull-right">
				<a class="btn btn-primary" target="_new" type="Button" href="{{ url('/fecho-imprimir', $a[0]->dia_fechado) . '/' . $id }}">Imprimir</a>
				<button class="btn btn-primary" target="_new" onclick="save(this)">Guardar</button>
			</div>
			@elseif(Auth::user()->role_id == 4)
			<div class="pull-right">
				<a class="btn btn-primary" target="_new" type="Button" href="{{ url('/fecho-imprimir-estetica', $a[0]->dia_fechado) . '/' . $id }}">Imprimir</a>
				<button class="btn btn-primary" target="_new" onclick="saveEstetica(this)">Guardar</button>
			</div>
			@else
			<div class="pull-right">
				<a class="btn btn-primary" target="_new" type="Button" href="{{ url('/fecho-imprimir', $a[0]->dia_fechado) . '/' . $id }}">Imprimir</a>
				<button class="btn btn-primary" target="_new" onclick="save(this)">Guardar</button>
			</div>
			@endif
		@endif
	@endif

	@if(isset($load))
		<link rel="stylesheet" href="https://stackpath.bootstrapcdn.com/bootstrap/4.2.1/css/bootstrap.min.css" integrity="sha384-GJzZqFGwb1QTTN6wy59ffF1BuGJpLSa9DkKMp0DgiMDm4iYMj70gZWKYbI706tWS" crossorigin="anonymous">
	@endif

	<link rel="stylesheet" type="text/css" href="https://maxcdn.bootstrapcdn.com/font-awesome/4.5.0/css/font-awesome.css">

	<script>

	function calc(){
		var a = parseFloat($("#total").html());
		var b = parseFloat($("#pay").val());
		var c = a - b;

		$("#myPercent").val(parseFloat((100 * b) / a).toFixed(2));
		$("#otherPercent").val(parseFloat((100 * c) / a).toFixed(2));

		$("#lucro").val(c);
	}

	function calc2(){
		var a = parseFloat($("#total").html());
		var totalFake = parseFloat($("#totalFake").html());
		var b = parseFloat($("#myPercent").val());
		var c = (a * b) / 100;

		$("#otherPercent").val(parseFloat(100 - b).toFixed(2));

		for (var i = 0; i < $(".dif").length; i++) {
			c = (c + $(".dif")[i].value*1);
		}

		$("#pay").val(parseFloat(c).toFixed(2));
		$("#lucro").val(parseFloat(totalFake - c).toFixed(2));
	}

	@if(isset($v))
		calc2();
	@endif

	@if(!isset($load))
		function save(e){
			$(e).html('<span class="fa fa-spinner fa-spin"></span> Guardando').addClass('btn-warning').removeClass('btn-primary');
			$.ajax({url: "{{ url('/fecho-caixa/save') }}" + "?dia=" + "{{$a[0]->dia_fechado}}" + "&cadeira_id=" + "{{$id}}" + "&valor=" + $("#myPercent").val(), success: function(result){
				$(e).html('<span class="fa fa-check"></span> Guardado').addClass('btn-success').removeClass('btn-warning');
				setTimeout(function(){
					$(e).html('Guardar').addClass('btn-primary').removeClass('btn-success');
				}, 2000);
				}
			});
		} 
		function saveEstetica(e){
			$(e).html('<span class="fa fa-spinner fa-spin"></span> Guardando').addClass('btn-warning').removeClass('btn-primary');
			$.ajax({url: "{{ url('/fecho-caixa-estetica/save') }}" + "?dia=" + "{{$a[0]->dia_fechado}}" + "&cadeira_id=" + "{{$id}}" + "&valor=" + $("#myPercent").val(), success: function(result){
				$(e).html('<span class="fa fa-check"></span> Guardado').addClass('btn-success').removeClass('btn-warning');
				setTimeout(function(){
					$(e).html('Guardar').addClass('btn-primary').removeClass('btn-success');
				}, 2000);
				}
			});
		} 
	@endif
</script>
@endif