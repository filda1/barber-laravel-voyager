@extends('voyager::master')

@section('page_title', 'Fecho de Caixa')

@section('page_header')
    <div class="container-fluid">
        <h1 class="page-title">
            <i class="voyager-dollar"></i> Fecho de Caixa
        </h1>
    </div>
@stop

@section('content')
    @include('voyager::alerts')
    <div class="page-content browse container-fluid">        
        @if(Auth::user()->role_id == 3 || Auth::user()->role_id == 1)
        <div class="row">
            <div class="col-md-12">
                <div class="panel panel-bordered">
                    <div class="panel-body">
                        <div class="col-md-4">
                            <label>Selecionar data de fecho </label>
                            <input onchange="getFecho()" type="date" id="dateToFecho">
                        </div>
                        <div class="col-md-3">
                            <label>Selecionar cadeira </label>
                            <select id="cadeira" onchange="getFecho()">
                                @foreach($s as $b)
                                <option value="{{ $b->id }}">{{ $b->slot }}</option>
                                @endforeach
                            </select>
                        </div><br><br>
                        <div class="col-12" id="fecho-container">
                            
                        </div>
                        
                    </div>
                </div>
            </div>
        </div>
        @elseif(Auth::user()->role_id == 4)
        <div class="row">
            <div class="col-md-12">
                <div class="panel panel-bordered">
                    <div class="panel-body">
                        <div class="col-md-4">
                            <label>Selecionar data de fecho </label>
                            <input onchange="getFechoEstetica()" type="date" id="dateToFecho">
                        </div>
                        <div class="col-md-3">
                            <label>Selecionar cadeira </label>
                            <select id="cadeira" onchange="getFechoEstetica()">
                                @foreach($s2 as $b)
                                <option value="{{ $b->id }}">{{ $b->slot }}</option>
                                @endforeach
                            </select>
                        </div><br><br>
                        <div class="col-12" id="fecho-container">
                            
                        </div>
                        
                    </div>
                </div>
            </div>
        </div>
        @else
            <div class="row">
                <div class="col-md-12">
                    <div class="panel panel-bordered">
                        <div class="panel-body">
                            <div class="col-md-4">
                                <label>Selecionar data de fecho </label>
                                <input onchange="getFecho()" type="date" id="dateToFecho">
                            </div>
                            <div class="col-md-3">
                                <label>Selecionar cadeira </label>
                                <select id="cadeira" onchange="getFecho()">
                                    <option value="{{ Auth::user()->cadeira_id }}" selected>{{ Auth::user()->name }}</option>
                                </select>
                            </div><br><br>
                            <div class="col-12" id="fecho-container">
                                
                            </div>
                            
                        </div>
                    </div>
                </div>
            </div>
        @endif
    </div>

   
@stop


@section('javascript')
<script>
    function getFecho(val){
       $.ajax({url: "{{ url('get-fecho') }}" + "/" + $("#dateToFecho").val() + "/" + $("#cadeira").val(), success: function(result){
            $("#fecho-container").html(result);
        }}); 
    }
    function getFechoEstetica(val){
       $.ajax({url: "{{ url('get-fecho-estetica') }}" + "/" + $("#dateToFecho").val() + "/" + $("#cadeira").val(), success: function(result){
            $("#fecho-container").html(result);
        }}); 
    }
</script> 
@stop
