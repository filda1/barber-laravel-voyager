<table class="table table-hover table-striped">
    <thead>
        <th>ID</th>
        <th>Cadeira</th>
        <th>Cliente</th>
        <th>Dia</th>
        <th>Valor</th>  
    </thead>
    <tbody>
@php
    $i = 0;
    $total=0;
    $len = count($users);
    foreach($users as $values){
        $total+=$values->valor_final;
        if ($i == $len - 1) {
            @endphp
            <tr>
                <td>{{$values->id}}</td>
                <td>{{$values->cadeira_id}}</td>
                <td>{{$values->user_id}}</td>
                <td>{{$values->dia}}</td>
                <td>{{$values->valor_final}}</td>
            </tr>
            <tr style="color:black;">
                <td colspan="4"><b>Total</b></td>
                <td><b>{{$total}}</b></td>
            </tr>
            @php
        }else{
    @endphp
    <tr>
        <td>{{$values->id}}</td>
        <td>{{$values->cadeira_id}}</td>
        <td>{{$values->user_id}}</td>
        <td>{{$values->dia}}</td>
        <td>{{$values->valor_final}}</td>
    </tr>
    @php
        }
    $i++;
    }
@endphp

    </tbody>
</table>