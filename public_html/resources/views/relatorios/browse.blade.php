@extends('voyager::master')

@section('page_title', 'Relatórios')

@section('page_header')
<div class="container-fluid">
    <h1 class="page-title"><i class="voyager-receipt"></i>Relatórios</h1>
</div>
@stop


@section('content')
    <div class="page-content browse container-fluid">
        <div class="alerts">
            @if (session('status'))
                <div class="alert alert-success">
                    {{ session('status') }}
                </div>
            @endif
            @if (session('error'))
                <div class="alert alert-danger">
                    {{ session('error') }}
                </div>
            @endif
        </div>
        <div class="row">
            <div class="col-md-12">
                <div class="panel panel-default">
                    <div class="panel-body">
                        <form>
                            <div class="form-group">
                                <label for="exampleInputEmail1">Data Inicio</label>
                                <input type="date" class="form-control" name="data_ini" id="data_ini" onchange="check()">
                            </div>
                            <div class="form-group">
                                <label for="exampleInputEmail1">Data Fim</label>
                                <input type="date" class="form-control" name="data_fim" id="data_fim" onchange="check()">
                            </div>
                            <div class="form-group">
                                <label for="exampleInputEmail1">Barbeiro</label>
                                <select name="barbeiro" class="form-control" id="barb" onchange="check()">
                                    <option value="">Selecione umm Barbeiro</option>
                                    
                                    @foreach($barb as $barbeiro)
                                        <option value="{{$barbeiro->id}}">{{$barbeiro->slot}}</option>
                                    @endforeach
                                    
                                </select>
                            </div>
                        </form>
                        <div class="col-md-12">
                            <div class="table-responsive table-full-width" id="tabela">
                                
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
@endsection

@section('javascript')
<script>
    function get(){
        $.ajaxSetup({
            headers: {
                'X-CSRF-TOKEN': "{{ csrf_token() }}"
            }
        });
        $.ajax({
            url: "{{url('/getrelatorios')}}",
            type: "POST",
            data: vari,
            success: function(result){
                $('#tabela').html(""+result);
            }
        });
    }
    var vari = {};
    function check(){
        var input1 =$('#data_ini').val();
        var input2 =$('#data_fim').val();
        var input3 =$('#barb').val();
        if(input1 != null && input2 != null && input3 != null){
            if(input1 != "" && input2 != "" && input3 != ""){
                vari['data_ini'] = $('#data_ini').val();
                vari['data_fim'] = $('#data_fim').val();
                vari['barb'] = $('#barb').val();
                get();
            }
        }
    }
    
    
</script>
@endsection