
	@php

		//Soma Total

		$soma_total = 0;

	@endphp


@foreach($resultados as $trabalho => $marcacao)

	@php

		//Soma por Trabalho

		$soma_trabalho_total = 0;

	@endphp
<!--
	<table class="table table-striped">
	    <thead>
		    <tr>
		      	<th style="width: 100%;">{{ $trabalho }}</th>
		      	<th class="text-center">Total</th>
		    </tr>
	    </thead>
	    <tbody>

-->
			@foreach($marcacao as $value)
			
				@php

					$soma_trabalho_total += $value->valor_final;

				@endphp
<!--
				<tr>
			        <td style="width: 100%;">{{$value->user_id}}</td>
			       	<td class="text-center">{{number_format($value->valor_final, 2)}}€</td>
			    </tr>
-->
			@endforeach
<!--
			<tr style="color:black;">
		        <th style="width: 100%;">Total {{ $trabalho }}</th>
		       	<th class="text-center">{{ number_format($soma_trabalho_total, 2) }}€</th>
			</tr>
			-->

			@php

				$soma_total += $soma_trabalho_total;

			@endphp
	      <!--
	    </tbody>
  	</table>
			-->
@endforeach

	<table class="table table-striped" style="color:black;">
		<thead>
	      <tr>
	        <th class="orange-header" style="width: 100%;">Total de Todos os Trabalhos</th>
	        <th class="text-center orange-header">Total</th>
	      </tr>
	    </thead>
	    <tbody>
	      <tr>
	        <th style="width: 100%;"></th>
	        <th class="text-center">{{ number_format($soma_total, 2) }}€</th>
	      </tr>
	    </tbody>
  	</table>