@extends('voyager::master')

@section('css')
    <meta name="csrf-token" content="{{ csrf_token() }}">
@stop

@section('page_title', __('voyager::generic.'.(!is_null($dataTypeContent->getKey()) ? 'edit' : 'add')).' '.$dataType->display_name_singular)

@section('page_header')
    <h1 class="page-title">
        <i class="{{ $dataType->icon }}"></i>
        {{ __('voyager::generic.'.(!is_null($dataTypeContent->getKey()) ? 'edit' : 'add')).' '.$dataType->display_name_singular }}
    </h1>
    @include('voyager::multilingual.language-selector')
@stop

@section('content')
    <div class="page-content edit-add container-fluid">
        <div class="row">
            <div class="col-md-12">

                <div class="panel panel-bordered">
                    <!-- form start -->
                    <form role="form"
                            class="form-edit-add"
                            action="@if(!is_null($dataTypeContent->getKey())){{ route('voyager.'.$dataType->slug.'.update', $dataTypeContent->getKey()) }}@else{{ route('voyager.'.$dataType->slug.'.store') }}@endif"
                            method="POST" enctype="multipart/form-data" id="formToSubmit">
                        <!-- PUT Method if we are editing -->
                        @if(!is_null($dataTypeContent->getKey()))
                            {{ method_field("PUT") }}
                        @endif

                        <!-- CSRF TOKEN -->
                        {{ csrf_field() }}

                        <div class="panel-body">

                            @if (count($errors) > 0)
                                <div class="alert alert-danger">
                                    <ul>
                                        @foreach ($errors->all() as $error)
                                            <li>{{ $error }}</li>
                                        @endforeach
                                    </ul>
                                </div>
                            @endif

                            <!-- Adding / Editing -->
                            @php
                                $dataTypeRows = $dataType->{(!is_null($dataTypeContent->getKey()) ? 'editRows' : 'addRows' )};
                            @endphp

                            @foreach($dataTypeRows as $row)
                                <!-- GET THE DISPLAY OPTIONS -->
                                @php
                                    $display_options = isset($row->details->display) ? $row->details->display : NULL;
                                @endphp
                                @if (isset($row->details->legend) && isset($row->details->legend->text))
                                    <legend class="text-{{isset($row->details->legend->align) ? $row->details->legend->align : 'center'}}" style="background-color: {{isset($row->details->legend->bgcolor) ? $row->details->legend->bgcolor : '#f0f0f0'}};padding: 5px;">{{$row->details->legend->text}}</legend>
                                @endif
                                @if (isset($row->details->formfields_custom))
                                    @include('voyager::formfields.custom.' . $row->details->formfields_custom)
                                @else
                                    <div class="form-group @if($row->type == 'hidden') hidden @endif col-md-{{ isset($display_options->width) ? $display_options->width : 12 }}" @if(isset($display_options->id)){{ "id=$display_options->id" }}@endif>
                                        {{ $row->slugify }}
                                        @if($row->display_name == "Data")
                                            <label for="name">{{ $row->display_name }}</label>
                                            @include('voyager::multilingual.input-hidden-bread-edit-add')
                                            @if($row->type == 'relationship')
                                                @include('voyager::formfields.relationship', ['options' => $row->details])      
                                            @else
                                                {!! app('voyager')->formField($row, $dataType, $dataTypeContent) !!}
                                            @endif

                                            @foreach (app('voyager')->afterFormFields($row, $dataType, $dataTypeContent) as $after)
                                                {!! $after->handle($row, $dataType, $dataTypeContent) !!}
                                            @endforeach
                                            <input type="checkbox" name="variosDias" id="variosDias" onclick="add()"><label for="variosDias" id="lable" style="padding-left:10px;-webkit-user-select: none; /* Safari */-moz-user-select: none; /* Firefox */-ms-user-select: none; /* IE10+/Edge */user-select: none; /* Standard */">Varios dias</label>
                                        @else
                                            <label for="name">{{ $row->display_name }}</label>
                                            @include('voyager::multilingual.input-hidden-bread-edit-add')
                                            @if($row->type == 'relationship')
                                                @include('voyager::formfields.relationship', ['options' => $row->details])      
                                            @else
                                                {!! app('voyager')->formField($row, $dataType, $dataTypeContent) !!}
                                            @endif

                                            @foreach (app('voyager')->afterFormFields($row, $dataType, $dataTypeContent) as $after)
                                                {!! $after->handle($row, $dataType, $dataTypeContent) !!}
                                            @endforeach
                                        @endif
                                    </div>
                                @endif
                            @endforeach

                        </div><!-- panel-body -->

                        <div class="panel-footer">
                            <button type="button" onclick="checkMarcacoes()" class="btn btn-primary save">{{ __('voyager::generic.save') }}</button>
                        </div>
                    </form>

                    <iframe id="form_target" name="form_target" style="display:none"></iframe>
                    <form id="my_form" action="{{ route('voyager.upload') }}" target="form_target" method="post"
                            enctype="multipart/form-data" style="width:0;height:0;overflow:hidden">
                        <input name="image" id="upload_file" type="file"
                                 onchange="$('#my_form').submit();this.value='';">
                        <input type="hidden" name="type_slug" id="type_slug" value="{{ $dataType->slug }}">
                        {{ csrf_field() }}
                    </form>

                </div>
            </div>
        </div>
    </div>

    <div class="modal fade modal-danger" id="confirmarAdicionar">
        <div class="modal-dialog">
            <div class="modal-content">

                <div class="modal-header">
                    <button type="button" class="close" data-dismiss="modal"
                            aria-hidden="true">&times;</button>
                    <h4 class="modal-title"><i class="voyager-warning"></i> Estas marcações serão eliminadas, tem a certeza que pretende continuar?</h4>
                </div>

                <div class="modal-body" id="confirmarBody">
                    
                </div>

                 <div class="modal-body">
                    <div><label>Enviar Notificação?</label> <input type="checkbox" name="enviarnoti"></div>
                </div>

                <div class="modal-footer">
                    <button type="button" class="btn btn-default" data-dismiss="modal">{{ __('voyager::generic.cancel') }}</button>
                    <button type="button" class="btn btn-danger" onclick="confirmDeleteMarcacoes(this)">Sim, Continuar!</button>
                </div>
            </div>
        </div>
    </div>
    <!-- End Delete File Modal -->
@stop

@section('javascript')
    <script>
        var params = {};
        var $image;
        var idsToRemove = "";

        $('[name="start_hour"]').click(function(){
            $('[name="isAllDay"]').val('');
            $(".toggleswitch").bootstrapToggle('off');
        });

        $('[name="end_hour"]').click(function(){
            $('[name="isAllDay"]').val('');
            $(".toggleswitch").bootstrapToggle('off');
        });

        if($("[name=tipo] option:selected").val() == "aberto"){
            $("#formToSubmit").submit();
        }

        function checkMarcacoes(){
            $.ajax({url: "{{ url('special-date-estetica/check-marcacoes') }}" + "?cadeira=" + $('[name="cadeira_id"]').val() + "&dia=" + $('[name="date"]').val() + "&hora_inicio=" + $('[name="start_hour"]').val() + "&hora_fim=" + $('[name="end_hour"]').val() + "&diaTodo=" + $('[name="isAllDay"]').val(), success: function(result){
                    
                    $("#confirmarBody").empty();

                    if(result.length > 0)
                        $("#confirmarAdicionar").modal("show");
                    else
                        $("#formToSubmit").submit();

                    for (var i = 0; i < result.length; i++) {
                        idsToRemove = idsToRemove + "," + result[i]["id"];
                        $("#confirmarBody").append('<p><b>' + result[i]["hora_inicio"] + " - " + result[i]["hora_fim"] + "</b> " + result[i]["user_id"] + '</p>');
                    }
            }});
        }
        var addCheck=1;
        function add(){
            if(addCheck==1){
                $("#lable").after('<div class="form-group  col-md-12 removeVariosDias"><label for="name">Data</label><input type="date" class="form-control" name="date_fim" placeholder="Data" value=""></div>');
                addCheck++;
            }
            else if(addCheck==2){
                $(".removeVariosDias").remove();
                addCheck--;
            }
        }

        function confirmDeleteMarcacoes(e){
            $(e).attr("disabled", true);
            $.ajax({url: "{{ url('special-date-estetica/delete-marcacoes') }}" + "?ids=" + idsToRemove, success: function(result){
                $("#formToSubmit").submit();  
            }});
        }

      
        $('document').ready(function () {

        var url = window.location.href;
        var marcacao;

        var id = url.substring(
            url.lastIndexOf("special-dates-estetica/") + 14, 
            url.lastIndexOf("/edit")
        );

        
        

        if(url.includes("edit")){
            $.ajax({url: "{{ url('/special-dates-estetica/get-id') }}" + "/" + id, success: function(resultspecial){

                $.ajax({url: "{{ url('cadeiras-estetica/get') }}", success: function(result){
                    $('[name="cadeira_id"]').empty();
                    

                    @if(Auth::user()->role_id == 1 || Auth::user()->role_id == 3)
                    for (var i = 0; i < result.length; i++) {
                        if(resultspecial["cadeira_id"] == result[i]["id"])
                            $('[name="cadeira_id"]').append('<option value="' + result[i]["id"] + '"selected>'+ result[i]["slot"] +'</option>');
                        else
                            $('[name="cadeira_id"]').append('<option value="' + result[i]["id"] + '">'+ result[i]["slot"] +'</option>');
                    }
                    @else
                    var idC = "{{App\User::find(Auth::user()->id)->cadeira_id}}";
                        for (var i = 0; i < result.length; i++) {
                            if(idC == result[i]["id"])
                                $('[name="cadeira_id"]').append('<option value="' + result[i]["id"] + '">'+ result[i]["slot"] +'</option>');
                        }
                    @endif
                }});

            }}); 
        }else{
            $.ajax({url: "{{ url('cadeiras-estetica/get') }}", success: function(result){
                $('[name="cadeira_id"]').empty();

                @if(Auth::user()->role_id == 1 || Auth::user()->role_id == 3)
                    for (var i = 0; i < result.length; i++) {
                        $('[name="cadeira_id"]').append('<option value="' + result[i]["id"] + '">'+ result[i]["slot"] +'</option>');
                    }
                @else
                    var idC = "{{App\User::find(Auth::user()->id)->cadeira_id}}";
                        for (var i = 0; i < result.length; i++) {
                            if(idC == result[i]["id"])
                                $('[name="cadeira_id"]').append('<option value="' + result[i]["id"] + '">'+ result[i]["slot"] +'</option>');
                        }
                @endif
            }});
        }
           

            $('.toggleswitch').bootstrapToggle();

            //Init datepicker for date fields if data-datepicker attribute defined
            //or if browser does not handle date inputs
            $('.form-group input[type=date]').each(function (idx, elt) {
                if (elt.type != 'date' || elt.hasAttribute('data-datepicker')) {
                    elt.type = 'text';
                    $(elt).datetimepicker($(elt).data('datepicker'));
                }
            });

            @if ($isModelTranslatable)
                $('.side-body').multilingual({"editing": true});
            @endif

            $('.side-body input[data-slug-origin]').each(function(i, el) {
                $(el).slugify();
            });

            $('.form-group').on('click', '.remove-multi-image', function (e) {
                e.preventDefault();
                $image = $(this).siblings('img');

                params = {
                    slug:   '{{ $dataType->slug }}',
                    image:  $image.data('image'),
                    id:     $image.data('id'),
                    field:  $image.parent().data('field-name'),
                    _token: '{{ csrf_token() }}'
                }

                $('.confirm_delete_name').text($image.data('image'));
                $('#confirm_delete_modal').modal('show');
            });

            $('#confirm_delete').on('click', function(){
                $.post('{{ route('voyager.media.remove') }}', params, function (response) {
                    if ( response
                        && response.data
                        && response.data.status
                        && response.data.status == 200 ) {

                        toastr.success(response.data.message);
                        $image.parent().fadeOut(300, function() { $(this).remove(); })
                    } else {
                        toastr.error("Error removing image.");
                    }
                });

                $('#confirm_delete_modal').modal('hide');
            });
            $('[data-toggle="tooltip"]').tooltip();
        });
    
    setTimeout(function(){ 

        $(".toggle-group").click(function(){
            $('[name="start_hour"]').val('');
            $('[name="end_hour"]').val('');
        });

    }, 500);
        

    </script>
@stop
