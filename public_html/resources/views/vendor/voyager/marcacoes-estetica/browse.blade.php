<style>
    
    .toggle{
        width: 180px!important;
    }

</style>
@extends('voyager::master')

@section('page_title', __('voyager::generic.viewing').' '.$dataType->display_name_plural)

@section('page_header')
    
    <div class="container-fluid">
        <h1 class="page-title">
            <i class="{{ $dataType->icon }}"></i> {{ $dataType->display_name_plural }}
        </h1>
        @can('add', app($dataType->model_name))
            <a href="{{ route('voyager.'.$dataType->slug.'.create') }}" class="btn btn-success btn-add-new">
                <i class="voyager-plus"></i> <span>{{ __('voyager::generic.add_new') }}</span>
            </a>
        @endcan
        @can('delete', app($dataType->model_name))
            @include('voyager::partials.bulk-delete')
        @endcan
        @can('edit', app($dataType->model_name))
            @if(isset($dataType->order_column) && isset($dataType->order_display_column))
                <a href="{{ route('voyager.'.$dataType->slug.'.order') }}" class="btn btn-primary">
                    <i class="voyager-list"></i> <span>{{ __('voyager::bread.order') }}</span>
                </a>
            @endif
        @endcan
        @can('delete', app($dataType->model_name))
            @if($usesSoftDeletes)
                <input type="checkbox" @if ($showSoftDeleted) checked @endif id="show_soft_deletes" data-toggle="toggle" data-on="{{ __('voyager::bread.soft_deletes_off') }}" data-off="{{ __('voyager::bread.soft_deletes_on') }}">
            @endif
        @endcan
        @include('voyager::multilingual.language-selector')
    </div>
@stop

@section('content')

    <div class="page-content browse container-fluid">
        @include('voyager::alerts')
        <div class="row">
            <div class="col-md-12">
                <div class="panel panel-bordered">
                    <ul class="nav nav-tabs">
                        <li id="tab1"><a data-toggle="tab" href="#list">Lista</a></li>
                        <li id="tab2" class="active"><a data-toggle="tab" href="#calendar">Calendário</a></li>
                        <li>@if(Auth::user()->role_id == 4 || Auth::user()->role_id == 3 || Auth::user()->role_id == 5 )
                            <select onchange="selectChange()" id="currentBarber" style="height: 40px;">
                                @foreach(App\SlotsEstetica::all() as $user)
                                    <option @if($search->value[0] == $user->id) selected  @endif value="{{ $user->id }}">{{ $user->slot }}</option>
                                @endforeach
                            </select>
                            @endif
                        </li>
                      </ul>

                    <div class="tab-content">
                        <div id="list" class="panel-body tab-pane fade in ">
                            @if ($isServerSide)
                                <form method="get" class="form-search">
                                    <div id="search-input">
                                        <input type="hidden" id="search_key" name="key[]" value="cadeira_id">
                                        <input type="hidden" name="s[]" id="barberSelect" value="{{ App\User::find(Auth::user()->id)->cadeira_id }}">

                                        <input type="hidden" id="filter" name="filter" value="contains">
                                        <input type="hidden" id="search_key" name="key[]" value="user_id">
                                        <div class="input-group col-md-12">
                                            <input type="text" class="form-control" placeholder="Pesquisar por Nome/Apelido" name="s[]" @if(isset($search->value[1]))value="{{$search->value[1]}}" @endif>
                                            <span class="input-group-btn" style="border-left: 1px solid gray;">
                                             
                                            </span>
                                        </div>

                                        <input type="hidden" id="search_key" name="key[]" value="dia">
                                        <div class="input-group col-md-12">
                                            <input type="text" class="form-control" placeholder="Pesquisar por dia" name="s[]" @if(isset($search->value[2]))value="{{$search->value[2]}}" @endif>
                                            <span class="input-group-btn">
                                                <button class="btn btn-info btn-lg" type="submit">
                                                    <i class="voyager-search"></i>
                                                </button>
                                            </span>
                                        </div>
                                    </div>
                                </form>
                            @endif
                            <div class="table-responsive">
                                <table id="dataTable" class="table table-hover">
                                    <thead>
                                        <tr>
                                            @can('delete',app($dataType->model_name))
                                                <th>
                                                    <input type="checkbox" class="select_all">
                                                </th>
                                            @endcan
                                            @foreach($dataType->browseRows as $row)
                                            <th>
                                                @if ($isServerSide)
                                                    <a href="{{ $row->sortByUrl() }}">
                                                @endif
                                                {{ $row->display_name }}
                                                @if ($isServerSide)
                                                    @if ($row->isCurrentSortField())
                                                        @if (!isset($_GET['sort_order']) || $_GET['sort_order'] == 'asc')
                                                            <i class="voyager-angle-up pull-right"></i>
                                                        @else
                                                            <i class="voyager-angle-down pull-right"></i>
                                                        @endif
                                                    @endif
                                                    </a>
                                                @endif
                                            </th>
                                            @endforeach
                                            <th class="actions text-right">{{ __('voyager::generic.actions') }}</th>
                                        </tr>
                                    </thead>
                                    <tbody>
                                        @foreach($dataTypeContent as $data)
                                        <tr>
                                            @can('delete',app($dataType->model_name))
                                                <td>
                                                    <input type="checkbox" name="row_id" id="checkbox_{{ $data->getKey() }}" value="{{ $data->getKey() }}">
                                                </td>
                                            @endcan
                                            @foreach($dataType->browseRows as $row)
                                                
                                                <td>
                                                    @if($row->type == 'image')
                                                        <img src="@if( !filter_var($data->{$row->field}, FILTER_VALIDATE_URL)){{ Voyager::image( $data->{$row->field} ) }}@else{{ $data->{$row->field} }}@endif" style="width:100px">
                                                    @elseif($row->type == 'relationship')
                                                        @include('voyager::formfields.relationship', ['view' => 'browse','options' => $row->details])
                                                    @elseif($row->type == 'select_multiple')
                                                        @if(property_exists($row->details, 'relationship'))

                                                            @foreach($data->{$row->field} as $item)
                                                                @if($item->{$row->field . '_page_slug'})
                                                                    <a href="{{ $item->{$row->field . '_page_slug'} }}">{{ $item->{$row->field} }}</a>@if(!$loop->last), @endif
                                                                @else
                                                                    {{ $item->{$row->field} }}
                                                                @endif
                                                            @endforeach

                                                        @elseif(property_exists($row->details, 'options'))
                                                            @if (count(json_decode($data->{$row->field})) > 0)
                                                                @foreach(json_decode($data->{$row->field}) as $item)
                                                                    @if (@$row->details->options->{$item})
                                                                        {{ $row->details->options->{$item} . (!$loop->last ? ', ' : '') }}
                                                                    @endif
                                                                @endforeach
                                                            @else
                                                                {{ __('voyager::generic.none') }}
                                                            @endif
                                                        @endif

                                                    @elseif($row->type == 'select_dropdown' && property_exists($row->details, 'options'))

                                                        @if($data->{$row->field . '_page_slug'})
                                                            <a href="{{ $data->{$row->field . '_page_slug'} }}">{!! $row->details->options->{$data->{$row->field}} !!}</a>
                                                        @else
                                                            {!! isset($row->details->options->{$data->{$row->field}}) ?  $row->details->options->{$data->{$row->field}} : '' !!}
                                                        @endif

                                                    @elseif($row->type == 'select_dropdown' && $data->{$row->field . '_page_slug'})
                                                        <a href="{{ $data->{$row->field . '_page_slug'} }}">{{ $data->{$row->field} }}</a>
                                                    @elseif($row->type == 'date' || $row->type == 'timestamp')
                                                        {{ property_exists($row->details, 'format') ? \Carbon\Carbon::parse($data->{$row->field})->formatLocalized($row->details->format) : $data->{$row->field} }}
                                                    @elseif($row->type == 'checkbox')
                                                        @if(property_exists($row->details, 'on') && property_exists($row->details, 'off'))
                                                            @if($data->{$row->field})
                                                                <span class="label label-info">{{ $row->details->on }}</span>
                                                            @else
                                                                <span class="label label-primary">{{ $row->details->off }}</span>
                                                            @endif
                                                        @else
                                                        {{ $data->{$row->field} }}
                                                        @endif
                                                    @elseif($row->type == 'color')
                                                        <span class="badge badge-lg" style="background-color: {{ $data->{$row->field} }}">{{ $data->{$row->field} }}</span>
                                                    @elseif($row->type == 'text')
                                                        @include('voyager::multilingual.input-hidden-bread-browse')
                                                        <div class="readmore">{{ mb_strlen( $data->{$row->field} ) > 200 ? mb_substr($data->{$row->field}, 0, 200) . ' ...' : $data->{$row->field} }}</div>
                                                    @elseif($row->type == 'text_area')
                                                        @include('voyager::multilingual.input-hidden-bread-browse')
                                                        <div class="readmore">{{ mb_strlen( $data->{$row->field} ) > 200 ? mb_substr($data->{$row->field}, 0, 200) . ' ...' : $data->{$row->field} }}</div>
                                                    @elseif($row->type == 'file' && !empty($data->{$row->field}) )
                                                        @include('voyager::multilingual.input-hidden-bread-browse')
                                                        @if(json_decode($data->{$row->field}))
                                                            @foreach(json_decode($data->{$row->field}) as $file)
                                                                <a href="{{ Storage::disk(config('voyager.storage.disk'))->url($file->download_link) ?: '' }}" target="_blank">
                                                                    {{ $file->original_name ?: '' }}
                                                                </a>
                                                                <br/>
                                                            @endforeach
                                                        @else
                                                            <a href="{{ Storage::disk(config('voyager.storage.disk'))->url($data->{$row->field}) }}" target="_blank">
                                                                Download
                                                            </a>
                                                        @endif
                                                    @elseif($row->type == 'rich_text_box')
                                                        @include('voyager::multilingual.input-hidden-bread-browse')
                                                        <div class="readmore">{{ mb_strlen( strip_tags($data->{$row->field}, '<b><i><u>') ) > 200 ? mb_substr(strip_tags($data->{$row->field}, '<b><i><u>'), 0, 200) . ' ...' : strip_tags($data->{$row->field}, '<b><i><u>') }}</div>
                                                    @elseif($row->type == 'coordinates')
                                                        @include('voyager::partials.coordinates-static-image')
                                                    @elseif($row->type == 'multiple_images')
                                                        @php $images = json_decode($data->{$row->field}); @endphp
                                                        @if($images)
                                                            @php $images = array_slice($images, 0, 3); @endphp
                                                            @foreach($images as $image)
                                                                <img src="@if( !filter_var($image, FILTER_VALIDATE_URL)){{ Voyager::image( $image ) }}@else{{ $image }}@endif" style="width:50px">
                                                            @endforeach
                                                        @endif
                                                    @else
                                                        @include('voyager::multilingual.input-hidden-bread-browse')
                                                        <span data-idmy="{{ $data->{$row->field} }}">{{ $data->{$row->field} }}</span>
                                                    @endif
                                                </td>
                                            @endforeach
                                            <td class="no-sort no-click" id="bread-actions">
                                                @if($data->deleted_at == NULL)
                                                    @foreach(Voyager::actions() as $action)
                                                        @include('voyager::bread.partials.actions', ['action' => $action])
                                                    @endforeach
                                                @endif
                                            </td>
                                        </tr>
                                        @endforeach
                                    </tbody>
                                </table>
                            </div>
                            @if ($isServerSide)
                                <div class="pull-left">
                                    <div role="status" class="show-res" aria-live="polite">{{ trans_choice(
                                        'voyager::generic.showing_entries', $dataTypeContent->total(), [
                                            'from' => $dataTypeContent->firstItem(),
                                            'to' => $dataTypeContent->lastItem(),
                                            'all' => $dataTypeContent->total()
                                        ]) }}</div>
                                </div>
                                <div class="pull-right">
                                    {{ $dataTypeContent->appends(request()->input())->links() }}
                                </div>
                            @endif
                        </div>

                        <div id="calendar" class="panel-body tab-pane fade in active">
                            <div id='calendar'></div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>

    {{-- Single delete modal --}}
    <div class="modal modal-danger fade" tabindex="-1" id="delete_modal" role="dialog">
        <div class="modal-dialog">
            <div class="modal-content">
                <div class="modal-header">
                    <button type="button" class="close" data-dismiss="modal" aria-label="{{ __('voyager::generic.close') }}"><span aria-hidden="true">&times;</span></button>
                    <h4 class="modal-title"><i class="voyager-trash"></i> {{ __('voyager::generic.delete_question') }} {{ strtolower($dataType->display_name_singular) }}?</h4>
                </div>
                <div class="modal-footer">
                    <form action="#" id="delete_form" method="POST">
                        {{ method_field('DELETE') }}
                        {{ csrf_field() }}
                        <input type="submit" class="btn btn-danger pull-right delete-confirm" value="{{ __('voyager::generic.delete_confirm') }}">
                    </form>
                    <button type="button" class="btn btn-default pull-right" data-dismiss="modal">{{ __('voyager::generic.cancel') }}</button>
                </div>
            </div><!-- /.modal-content -->
        </div><!-- /.modal-dialog -->
    </div><!-- /.modal -->
@stop

@section('css')
@if(!$dataType->server_side && config('dashboard.data_tables.responsive'))
    <link rel="stylesheet" href="{{ voyager_asset('lib/css/responsive.dataTables.min.css') }}">
@endif
@stop

@section('javascript')
    <!-- DataTables -->
    @if(!$dataType->server_side && config('dashboard.data_tables.responsive'))
        <script src="{{ voyager_asset('lib/js/dataTables.responsive.min.js') }}"></script>
    @endif
    <script src="{{ url('js/moment.min.js') }}"></script>
    <link rel="stylesheet" type="text/css" href="{{ url('css/fullcalendar.min.css') }}">
    <script src="{{ url('js/fullcalendar.min.js') }}"></script>
    <script src="{{ url('js/pt.js') }}"></script>
    <link rel="stylesheet" type="text/css" href="https://stackpath.bootstrapcdn.com/font-awesome/4.7.0/css/font-awesome.min.css">
    <script>

        $(document).ready(function () {

            @if($usesSoftDeletes)
                @php
                    $params = [
                        's' => $search->value,
                        'filter' => $search->filter,
                        'key' => $search->key,
                        'order_by' => $orderBy,
                        'sort_order' => $sortOrder,
                    ];
                @endphp
                $(function() {
                    $('#show_soft_deletes').change(function() {
                        if ($(this).prop('checked')) {
                            $('#dataTable').before('<a id="redir" href="{{ (route('voyager.'.$dataType->slug.'.index', array_merge($params, ['showSoftDeleted' => 1]), true)) }}"></a>');
                        }else{
                            $('#dataTable').before('<a id="redir" href="{{ (route('voyager.'.$dataType->slug.'.index', array_merge($params, ['showSoftDeleted' => 0]), true)) }}"></a>');
                        }

                        $('#redir')[0].click();
                    })
                })
            @endif

            $(".view").remove();
            $.ajax({url: "{{ url('cadeiras-estetica/get') }}", success: function(result){

                $('#calendar').fullCalendar({
                    themeSystem: 'bootstrap4',
                    header: {
                      left: 'prev,next today',
                      center: 'title',
                      right: 'listDay,month,agendaWeek,agendaDay'
                    },
                    defaultView: "agendaDay",
                    allDaySlot: false,
                    slotDuration: '00:10:00',
                    minTime: "00:00:00",
                    maxTime: "24:00:00",
                    height: 500,
                    weekNumbers: false,
                    eventLimit: true,
                    viewRender: function (view, element) {
                       updateCalendar();
                    },
                    events: [
                    ],
                    dayClick: function(date, jsEvent, view) {
                        $('#calendar').fullCalendar('gotoDate',date);
                        $('#calendar').fullCalendar('changeView','agendaDay');

                   },
                   eventClick: function(calEvent, jsEvent, view) {
                    window.location.replace("{{ url('http://gentlemansbarbershop.pt/admin/marcacoes-estetica/') }}" + calEvent.id + "/edit");
                  }
                });

                @if(Session::has('date'))
                    date = moment("{{Session::get('date')}}", "YYYY-MM-DD");
                    $("#calendar").fullCalendar('gotoDate', date);
                @endif
            }});

            $('.select_all').on('click', function(e) {
                $('input[name="row_id"]').prop('checked', $(this).prop('checked'));
            });

        });

        var deleteFormAction;
        $('td').on('click', '.delete', function (e) {
            $('#delete_form')[0].action = '{{ route('voyager.'.$dataType->slug.'.destroy', ['id' => '__id']) }}'.replace('__id', $(this).data('id'));
            $('#delete_modal').modal('show');
        });

        var table;

        function selectChange(){
            $("#barberSelect").val($("#currentBarber").val());
            
            if ($( ".nav-tabs .active" ).attr('id') == "tab1") {
                $('.form-search').submit();
            }

            updateCalendar();
        }



        function updateCalendar(){

            if(table != null)
                table.search($("#currentBarber").find(':selected').data('id')).draw();

            var view = $('#calendar').fullCalendar('getView');

            @if(Auth::user()->role_id == 4 || Auth::user()->role_id == 3 || Auth::user()->id == 68)
                $.ajax({url: "{{ url('marcacoes-calendar-estetica/get?startDate=') }}" + moment(view.start).format('YYYY-MM-DD') + "&endDate=" + moment(view.end).format('YYYY-MM-DD') + "&cadeiraID=" + $("#currentBarber").val(), success: function(result){
            @else
                $.ajax({url: "{{ url('marcacoes-calendar-estetica/get?startDate=') }}" + moment(view.start).format('YYYY-MM-DD') + "&endDate=" + moment(view.end).format('YYYY-MM-DD') + "&cadeiraID=" + {{App\User::find(Auth::user()->id)->cadeira_id}}, success: function(result){
            @endif

                var final = [];

                for (var i = 0; i < result.length; i++) {
                    var color = "#3a87ad";
                    if(result[i]["dia_fechado"] != null){
                        color = "#76ad3a";
                    }

                    if(result[i]["user_id"] != 'Cliente Final'){
                        var c = {
                            id: result[i]["id"],
                            title: result[i]["user_id"]+result[i]["trabalho"],
                            start: result[i]["dia"]+"T"+result[i]["hora_inicio"],
                            end: result[i]["dia"]+"T"+result[i]["hora_fim"],
                            color: color
                        };
                    }else{
                        var c = {
                            id: result[i]["id"],
                            title: "("+result[i]["user_id"]+") " + result[i]["nome_temp"]+result[i]["trabalho"],
                            start: result[i]["dia"]+"T"+result[i]["hora_inicio"],
                            end: result[i]["dia"]+"T"+result[i]["hora_fim"],
                            color: color
                        };
                    }    
                    final[i] = c;
                }

                $('#calendar').fullCalendar( 'removeEvents' );
                $("#calendar").fullCalendar( 'addEventSource', final );

            }});
        }

    </script>

    <style>
        .fc-today {
            background: #c6ecff !important;
            border: none !important;
            border-top: 1px solid #ddd !important;
            font-weight: bold;
        } 

        #calendar table{
            cursor: pointer;
        }
    </style>
@stop
