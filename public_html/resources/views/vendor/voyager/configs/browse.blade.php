@extends('voyager::master')

@section('page_title', __('voyager::generic.viewing').' '.$dataType->display_name_plural)

@section('page_header')
    <div class="container-fluid">
        <h1 class="page-title">
            <i class="icon voyager-alarm-clock"></i> Horário de Trabalho
        </h1>
    </div>
@stop

@section('content')
    <div class="page-content browse container-fluid">
        @include('voyager::alerts')
        <div class="row">
            <div class="col-md-12">
                <div class="panel panel-bordered">
                    <div class="panel-body">
                        <form method="post" action="{{ url('/configs/save') }}">
                            <div class="col-md-6">
                                
                                <h3 class="text-center">Horário de Semana</h3>
                                <div class="col-md-6 text-center">
                                    <h4>Hora de Abertura</h4>  
                                    <input type="time" name="Hora_de_Abertura" id="Hora_de_Abertura">
                                    <h4>Hora de Almoço</h4>  
                                    <input type="time" name="Hora_de_Almoço" id="Hora_de_Almoço">
                                </div>
                                <div class="col-md-6 text-center">
                                    <h4>Hora de Reabertura</h4>  
                                    <input type="time" name="Hora_de_Reabertura" id="Hora_de_Reabertura">
                                    <h4>Hora de Fecho</h4>  
                                    <input type="time" name="Hora_de_Fecho" id="Hora_de_Fecho">
                                </div>
                            </div>
                            <div class="col-md-6">
                                <h3 class="text-center">Horário Fim de Semana</h3>
                                <div class="col-md-6 text-center">
                                    <h4>Hora de Abertura</h4>  
                                    <input type="time" name="Hora_de_Abertura_FS" id="Hora_de_Abertura_FS">
                                    <h4>Hora de Almoço</h4>  
                                    <input type="time" name="Hora_de_Almoço_FS" id="Hora_de_Almoço_FS">
                                </div>
                                <div class="col-md-6 text-center">
                                    <h4>Hora de Reabertura</h4>  
                                    <input type="time" name="Hora_de_Reabertura_FS" id="Hora_de_Reabertura_FS">
                                    <h4>Hora de Fecho</h4>  
                                    <input type="time" name="Hora_de_Fecho_FS" id="Hora_de_Fecho_FS">
                                </div>
                                <button type="submit" class="btn btn-primary pull-right">Guardar</button>
                            </div>
                            {{ csrf_field() }}
                        </form>
                    </div>
                </div>
            </div>
        </div>
    </div>
@stop

@section('javascript')
    <!-- DataTables -->
    @if(!$dataType->server_side && config('dashboard.data_tables.responsive'))
        <script src="{{ voyager_asset('lib/js/dataTables.responsive.min.js') }}"></script>
    @endif
    <script>
        $('document').ready(function () {

            $.ajax({url: "{{ url('configs/get') }}", success: function(result){
                for (var i = 0; i < result.length; i++) {
                    $("#"+result[i]["key"]).val(result[i]["value"]);
                }
            }});

        });
    </script>
@stop
