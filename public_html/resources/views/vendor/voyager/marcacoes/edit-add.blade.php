@extends('voyager::master')

@section('css')
    <meta name="csrf-token" content="{{ csrf_token() }}">
@stop

@section('page_title', __('voyager::generic.'.(!is_null($dataTypeContent->getKey()) ? 'edit' : 'add')).' '.$dataType->display_name_singular)

@section('page_header')
    <h1 class="page-title">
        <i class="{{ $dataType->icon }}"></i>
        {{ __('voyager::generic.'.(!is_null($dataTypeContent->getKey()) ? 'edit' : 'add')).' '.$dataType->display_name_singular }}
    </h1>
    <button class="btn btn-primary" data-toggle="modal" data-target="#myModal">Adicionar Cliente</button>
    <button class="btn btn-danger" data-toggle="modal" data-target="#confirm_delete_modal">Remover</button>
    @include('voyager::multilingual.language-selector')
@stop

@section('content')
    <div class="page-content edit-add container-fluid">
        <div class="row">
            <div class="col-md-12">

                <div class="panel panel-bordered">
                    <!-- form start -->
                    <form role="form"
                            class="form-edit-add"
                            action="@if(!is_null($dataTypeContent->getKey())){{ url('admin/marcacoes' . '/update-clean', $dataTypeContent->getKey()) }}@else{{ url('admin/marcacoes' . '/save-clean') }}@endif"
                            method="POST" enctype="multipart/form-data" id="form">
                        <!-- PUT Method if we are editing -->
                        @if(!is_null($dataTypeContent->getKey()))
                            {{ method_field("PUT") }}
                        @endif

                        <!-- CSRF TOKEN -->
                        {{ csrf_field() }}

                        <div class="panel-body">

                            @if (count($errors) > 0)
                                <div class="alert alert-danger">
                                    <ul>
                                        @foreach ($errors->all() as $error)
                                            <li>{{ $error }}</li>
                                        @endforeach
                                    </ul>
                                </div>
                            @endif

                            <!-- Adding / Editing -->
                            @php
                                $dataTypeRows = $dataType->{(!is_null($dataTypeContent->getKey()) ? 'editRows' : 'addRows' )};
                            @endphp

                            @foreach($dataTypeRows as $row)
                                <!-- GET THE DISPLAY OPTIONS -->
                                @php
                                    $display_options = isset($row->details->display) ? $row->details->display : NULL;
                                @endphp
                                @if (isset($row->details->legend) && isset($row->details->legend->text))
                                    <legend class="text-{{isset($row->details->legend->align) ? $row->details->legend->align : 'center'}}" style="background-color: {{isset($row->details->legend->bgcolor) ? $row->details->legend->bgcolor : '#f0f0f0'}};padding: 5px;">{{$row->details->legend->text}}</legend>
                                @endif
                                @if (isset($row->details->formfields_custom))
                                    @include('voyager::formfields.custom.' . $row->details->formfields_custom)
                                @else
                                    <div class="form-group @if($row->type == 'hidden') hidden @endif col-md-{{ isset($display_options->width) ? $display_options->width : 12 }}" @if(isset($display_options->id)){{ "id=$display_options->id" }}@endif>
                                        {{ $row->slugify }}
                                        <label for="name">{{ $row->display_name }}</label>
                                        @include('voyager::multilingual.input-hidden-bread-edit-add')
                                        @if($row->type == 'relationship')
                                            @include('voyager::formfields.relationship', ['options' => $row->details])      
                                        @else
                                            {!! app('voyager')->formField($row, $dataType, $dataTypeContent) !!}
                                        @endif

                                        @foreach (app('voyager')->afterFormFields($row, $dataType, $dataTypeContent) as $after)
                                            {!! $after->handle($row, $dataType, $dataTypeContent) !!}
                                        @endforeach
                                    </div>
                                @endif
                            @endforeach

                        </div><!-- panel-body -->

                        <div class="panel-footer">
                            <button type="button" onclick="saveCheck()" class="btn btn-primary save">{{ __('voyager::generic.save') }}</button>
                        </div>
                    </form>

                    <iframe id="form_target" name="form_target" style="display:none"></iframe>
                    <form id="my_form" action="{{ route('voyager.upload') }}" target="form_target" method="post"
                            enctype="multipart/form-data" style="width:0;height:0;overflow:hidden">
                        <input name="image" id="upload_file" type="file"
                                 onchange="$('#my_form').submit();this.value='';">
                        <input type="hidden" name="type_slug" id="type_slug" value="{{ $dataType->slug }}">
                        {{ csrf_field() }}
                    </form>

                </div>
            </div>
        </div>
    </div>

    <div class="modal fade modal-danger" id="confirm_delete_modal">
        <div class="modal-dialog">
            <div class="modal-content">

                <div class="modal-header">
                    <button type="button" class="close" data-dismiss="modal"
                            aria-hidden="true">&times;</button>
                    <h4 class="modal-title"><i class="voyager-warning"></i> Tem a certeza que pretende remover a marcação?</h4>
                </div>

                <div class="modal-footer">
                    <form action="http://gentlemansbarbershop.pt/admin/marcacoes/{{$dataTypeContent->getKey()}}" id="delete_form" method="POST">
                        <input type="hidden" name="_method" value="DELETE">
                        {{csrf_field()}}
                        <input type="submit" class="btn btn-danger pull-right delete-confirm" value="Sim, Remover!">
                    </form>
                </div>
            </div>
        </div>
    </div>
    
    <!-- Modal -->
    <div id="myModal" class="modal fade" role="dialog">
      <div class="modal-dialog">

        <!-- Modal content-->
        <div class="modal-content">
          <div class="modal-header">
            <button type="button" class="close" data-dismiss="modal">&times;</button>
            <h4 class="modal-title">Adicionar Cliente</h4>
          </div>
          <div class="modal-body">
            <form role="form" class="form-edit-add" id="createCliente" method="POST" enctype="multipart/form-data">
            <!-- PUT Method if we are editing -->

                <!-- CSRF TOKEN -->
                {{ csrf_field() }}

                <div class="panel-body">


                <!-- Adding / Editing -->

                <!-- GET THE DISPLAY OPTIONS -->
                <div class="form-group col-md-12">

                <label for="name">Imagem</label>
                <input type="file" name="image" accept="image/*">


                </div>
                <!-- GET THE DISPLAY OPTIONS -->
                <div class="form-group col-md-12">

                <label for="name">Primeiro Nome</label>
                <input type="text" class="form-control" name="first_name" placeholder="Primeiro Nome" value="">


                </div>
                <!-- GET THE DISPLAY OPTIONS -->
                <div class="form-group col-md-12">

                <label for="name">Apelido</label>
                <input type="text" class="form-control" name="last_name" placeholder="Apelido" value="">


                </div>
                <!-- GET THE DISPLAY OPTIONS -->
                <div class="form-group col-md-12">

                <label for="name">Email</label>
                <input type="text" class="form-control" name="email" placeholder="Email" value="">


                </div>
                <!-- GET THE DISPLAY OPTIONS -->
                <div class="form-group col-md-12">

                <label for="name">Numero</label>
                <input type="text" class="form-control" name="numero" placeholder="Numero" value="">


                </div>
                <!-- GET THE DISPLAY OPTIONS -->
                <div class="form-group col-md-12">

                <label for="name">Data de Aniversário</label>
                <input type="date" class="form-control" name="birthday" placeholder="Data de Aniversário" value="">


                </div>
                <!-- GET THE DISPLAY OPTIONS -->
                <div class="form-group col-md-12">

                <label for="name">Password</label>
                <input type="password" class="form-control" name="password" value="">


                </div>

                </div><!-- panel-body -->

                <div class="panel-footer">
                    <button type="button" onclick="saveCliente()" class="btn btn-primary save">Guardar</button>
                </div>
            </form>
          </div>
          <div class="modal-footer">
            <button type="button" class="btn btn-default" data-dismiss="modal">Cancelar</button>
          </div>
        </div>

      </div>
    </div>

<div id="dataCheck" class="modal fade" role="dialog">
  <div class="modal-dialog">

    <!-- Modal content-->
    <div class="modal-content">
      <div class="modal-header bg-danger">
        <button type="button" class="close" data-dismiss="modal">&times;</button>
        <h4 class="modal-title">Data Especial</h4>
      </div>
      <div class="modal-body">
        Atenção existe uma data especial marcada para este dia!
        <p><b>Nota:</b></p>
        <p id="nota"></p>
      </div>
      <div class="modal-footer">
        <button type="button" class="btn btn-default" data-dismiss="modal">Cancelar</button>
        <button type="button" class="btn btn-danger" data-dismiss="modal" onclick="$('#form').submit();">Forçar Marcação</button>
      </div>
    </div>

  </div>
</div>
@stop

@section('javascript')
    <script>
        var params = {};
        var $image;

        function saveCheck(){
            $.ajax({
                url : 'http://gentlemansbarbershop.pt/check/data-especial'+'/' + $('[name="cadeira_id"]').val() +'/'+$('[name="dia"]').val(),
                type: "POST",
                success: function (data) {

                    if(data == ""){
                        $('#form').submit();
                    }else{
                        $("#dataCheck").modal('toggle');
                        $('#nota').html(data["note"]);
                    }

                },
                error: function (jXHR, textStatus, errorThrown) {
                    //alert("erro1");
                    $('#form').submit();
                }
            });
        }

        function saveCliente(){
            $.ajax({
                url : 'http://gentlemansbarbershop.pt/admin/app-users',
                type: "POST",
                data: $('#createCliente').serialize(),
                success: function (data) {
                    $("#myModal").modal('toggle');

                    $('[name="user_id"]').prepend('<option value="' + data["data"]["id"] + '">'+ data["data"]["first_name"] + " " + data["data"]["last_name"] + " (" + data["data"]["email"] + ")" +'</option>');
                    
                    $('#createCliente').find("input[type=text], textarea").val("");
                },
                error: function (jXHR, textStatus, errorThrown) {
                    alert("erro2");
                }
            });
        };

        var totalSugerido = 0;

        $('[name="user_id"]').on('change', function(){
            console.log('ok');
            if($('[name="user_id"]').val() == 0){
                $('[name="nome_temp"]').parent().show();
                $('[name="numero_temp"]').parent().show();
            }else{
                $('[name="nome_temp"]').parent().hide();
                $('[name="numero_temp"]').parent().hide();
            }
        });


        $('document').ready(function () {
        
        $('[name="nome_temp"]').parent().hide();
        $('[name="numero_temp"]').parent().hide();


        var url = window.location.href;
        var marcacao;

        var id = url.substring(
            url.lastIndexOf("marcacoes/") + 10, 
            url.lastIndexOf("/edit")
        );



        if(url.includes('edit')){
            $.ajax({url: "{{ url('marcacoes/get-id') }}" + "/" + id, success: function(result){
                marcacao = result;

                var marcacoes = [];
                var m = JSON.parse(marcacao["trabalho"]);
                for (var i = 0; i < m.length; i++) {
                    marcacoes.push(m[i]["id"]);
                }

                $.ajax({url: "{{ url('/barber-jobs/get') }}", success: function(result){
                    $('[name="trabalho[]"]').empty();

                    for (var i = 0; i < result.length; i++) {
                        if(jQuery.inArray( result[i]["id"].toString(), marcacoes) != -1){
                            totalSugerido += parseInt(result[i]["valor"]);


                           $('[name="trabalho[]"]').append('<option value="' + result[i]["id"] + '" selected>'+ result[i]["nome"] +'</option>');
                        }
                        else{
                            $('[name="trabalho[]"]').append('<option value="' + result[i]["id"] + '">'+ result[i]["nome"] +'</option>');
                        }
                    }

                    $('[name="valor_final"]').val(totalSugerido);
                }});
               
                $.ajax({url: "{{ url('cadeiras/get') }}", success: function(result){
                    $('[name="cadeira"]').empty();

                    @if(Auth::user()->role_id == 1 || Auth::user()->role_id == 3 || Auth::user()->id == 68)
                    for (var i = 0; i < result.length; i++) {
                        if(result[i]["id"] == marcacao["cadeira_id"])
                            $('[name="cadeira_id"]').append('<option value="' + result[i]["id"] + '" selected>'+ result[i]["slot"] +'</option>');
                        else
                            $('[name="cadeira_id"]').append('<option value="' + result[i]["id"] + '">'+ result[i]["slot"] +'</option>');
                    }
                    @else
                    var idC = "{{App\User::find(Auth::user()->id)->cadeira_id}}";
                        for (var i = 0; i < result.length; i++) {
                            if(idC == result[i]["id"])
                                $('[name="cadeira_id"]').append('<option value="' + result[i]["id"] + '">'+ result[i]["slot"] +'</option>');
                        }
                    @endif
                }});

                $.ajax({url: "{{ url('app-users/get') }}", success: function(result){
                    $('[name="user_id"]').empty();

                    for (var i = 0; i < result.length; i++) {
                        if(result[i]["id"] == marcacao["user_id"])
                            $('[name="user_id"]').append('<option value="' + result[i]["id"] + '" selected>'+ result[i]["first_name"] + " " + result[i]["last_name"] + " (" + result[i]["email"] + ")" +'</option>');
                        else
                            $('[name="user_id"]').append('<option value="' + result[i]["id"] + '">'+ result[i]["first_name"] + " " + result[i]["last_name"] + " (" + result[i]["email"] + ")" +'</option>');

                    }

                    if($('[name="user_id"]').val() == 0){
                        $('[name="nome_temp"]').parent().show();
                        $('[name="numero_temp"]').parent().show();
                        $('[name="nome_temp"]').val(marcacao['nome_temp']);
                        $('[name="numero_temp"]').val(marcacao['numero_temp']);

                        
                    }
                }});


            }});
        }else{
            $.ajax({url: "{{ url('/barber-jobs/get') }}", success: function(result){
                    $('[name="trabalho[]"]').empty();

                    for (var i = 0; i < result.length; i++) {
                        $('[name="trabalho[]"]').append('<option value="' + result[i]["id"] + '">'+ result[i]["nome"] +'</option>');
                    }
                }});
                
                $.ajax({url: "{{ url('cadeiras/get') }}", success: function(result){
                    $('[name="cadeira"]').empty();

                    $('[name="cadeira_id"]').append('<option selected disabled>Selecione um barbeiro</option>');

                    
                    @if(Auth::user()->role_id == 1 || Auth::user()->role_id == 3 || Auth::user()->id == 68)
                    for (var i = 0; i < result.length; i++) {
                        $('[name="cadeira_id"]').append('<option value="' + result[i]["id"] + '">'+ result[i]["slot"] +'</option>');
                    }
                    @else
                    var idC = "{{App\User::find(Auth::user()->id)->cadeira_id}}";
                        for (var i = 0; i < result.length; i++) {
                            if(idC == result[i]["id"])
                                $('[name="cadeira_id"]').append('<option value="' + result[i]["id"] + '">'+ result[i]["slot"] +'</option>');
                        }
                    @endif
                }});

                $.ajax({url: "{{ url('app-users/get') }}", success: function(result){
                    $('[name="user_id"]').empty();
                    $('[name="user_id"]').append('<option selected>Selecione um Cliente</option>');
                    for (var i = 0; i < result.length; i++) {
                        $('[name="user_id"]').append('<option value="' + result[i]["id"] + '">'+ result[i]["first_name"] + " " + result[i]["last_name"] + " (" + result[i]["email"] + ")" +'</option>');
                    }
                }});
        }
        

            $('.toggleswitch').bootstrapToggle();

            //Init datepicker for date fields if data-datepicker attribute defined
            //or if browser does not handle date inputs
            $('.form-group input[type=date]').each(function (idx, elt) {
                if (elt.type != 'date' || elt.hasAttribute('data-datepicker')) {
                    elt.type = 'text';
                    $(elt).datetimepicker($(elt).data('datepicker'));
                }
            });

            @if ($isModelTranslatable)
                $('.side-body').multilingual({"editing": true});
            @endif

            $('.side-body input[data-slug-origin]').each(function(i, el) {
                $(el).slugify();
            });

            $('.form-group').on('click', '.remove-multi-image', function (e) {
                e.preventDefault();
                $image = $(this).siblings('img');

                params = {
                    slug:   '{{ $dataType->slug }}',
                    image:  $image.data('image'),
                    id:     $image.data('id'),
                    field:  $image.parent().data('field-name'),
                    _token: '{{ csrf_token() }}'
                }

                $('.confirm_delete_name').text($image.data('image'));
                $('#confirm_delete_modal').modal('show');
            });

            $('#confirm_delete').on('click', function(){
                $.post('{{ route('voyager.media.remove') }}', params, function (response) {
                    if ( response
                        && response.data
                        && response.data.status
                        && response.data.status == 200 ) {

                        toastr.success(response.data.message);
                        $image.parent().fadeOut(300, function() { $(this).remove(); })
                    } else {
                        toastr.error("Error removing image.");
                    }
                });

                $('#confirm_delete_modal').modal('hide');
            });
            $('[data-toggle="tooltip"]').tooltip();
        });
    </script>
@stop
