<ul class="nav navbar-nav">

@php
    if (Voyager::translatable($items)) {
        $items = $items->load('translations');
    }
@endphp

@foreach ($items as $item)
    @php
        $listItemClass = [];
        $styles = null;
        $linkAttributes = null;
        $transItem = $item;

        if (Voyager::translatable($item)) {
            $transItem = $item->translate($options->locale);
        }

        $href = $item->link();

        // Current page
        if(url($href) == url()->current()) {
            array_push($listItemClass, 'active');
        }

        $permission = '';
        $hasChildren = false;

        // With Children Attributes
        if(!$item->children->isEmpty())
        {
            foreach($item->children as $child)
            {
                $hasChildren = $hasChildren || Auth::user()->can('browse', $child);

                if(url($child->link()) == url()->current())
                {
                    array_push($listItemClass, 'active');
                }
            }
            if (!$hasChildren) {
                continue;
            }

            $linkAttributes = 'href="#' . $transItem->id .'-dropdown-element" data-toggle="collapse" aria-expanded="'. (in_array('active', $listItemClass) ? 'true' : 'false').'"';
            array_push($listItemClass, 'dropdown');
        }
        else
        {
            $linkAttributes =  'href="' . url($href) .'"';

            if(!Auth::user()->can('browse', $item)) {
                continue;
            }
        }
    @endphp

    <li class="{{ implode(" ", $listItemClass) }}">
        <a {!! $linkAttributes !!} target="{{ $item->target }}">
            <span class="icon {{ $item->icon_class }}"></span>
            <span class="title">{{ $transItem->title }}</span>
        </a>
        @if($hasChildren)
            <div id="{{ $transItem->id }}-dropdown-element" class="panel-collapse collapse {{ (in_array('active', $listItemClass) ? 'in' : '') }}">
                <div class="panel-body">
                    @include('voyager::menu.admin_menu_child', ['items' => $item->children, 'options' => $options, 'innerLoop' => true])
                </div>
            </div>
        @endif
    </li>
@endforeach
	
    @if(Auth::user()->role_id == 1 || Auth::user()->role_id == 3)
    <li class="">
        <a href="{{ url('centro-mensagens') }}" target="_self">
            <span class="icon voyager-paper-plane"></span>
            <span class="title">Centro de Mensagens</span>
        </a>
    </li>
    @endif
    @if(Auth::user()->role_id == 1 || Auth::user()->role_id == 3)
    <li class="">
        <a href="{{ url('relatorios') }}" target="_self">
            <span class="icon voyager-receipt"></span>
            <span class="title">Relatórios</span>
        </a>
    </li>
    @endif
    <li class="">
        <a href="{{ url('fecho-caixa') }}" target="_self">
            <span class="icon voyager-dollar"></span>
            <span class="title">Fecho Caixa</span>
        </a>
    </li>
</ul>
