<!DOCTYPE html>

<html>

  <head>

    <meta charset="utf-8">

    <meta name="viewport" content="width=device-width, initial-scale=1">

    <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/4.1.3/css/bootstrap.min.css">

    <script src="https://ajax.googleapis.com/ajax/libs/jquery/3.3.1/jquery.min.js"></script>

    <script src="https://cdnjs.cloudflare.com/ajax/libs/popper.js/1.14.3/umd/popper.min.js"></script>

    <script src="https://maxcdn.bootstrapcdn.com/bootstrap/4.1.3/js/bootstrap.min.js"></script>

    <link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/font-awesome/4.7.0/css/font-awesome.min.css">

    <link rel="stylesheet" href="../../../../css/style.css">

    <link rel="stylesheet" href="../../../../css/bootstrap.min.css">

    <script src="//netdna.bootstrapcdn.com/bootstrap/3.0.0/js/bootstrap.min.js"></script>

    <script src="//code.jquery.com/jquery-1.11.1.min.js"></script>

    <title>Barbearia</title>

  </head>

  <body id="body">

    <a href="https://www.facebook.com/GentlemansBarbershopLousada/" target="_blank">

      <div class="facebook-icon">

        <span style="color: white;" class="fa fa-facebook-square fa-1x"></span>

      </div>

    </a>

    <a href="https://www.instagram.com/gentlemans_barbershop_lousada/" target="_blank">

      <div class="instagram-icon">

        <span style="color: white;" class="fa fa-instagram fa-1x"></span>

      </div>

    </a>

    <header>

      <!-- Inicio Navbar -->

        <nav class="navbar navbar-expand-md navbar-fixed-top navbar-dark" style="background-color: #0000002e;">

          <a class="navbar-brand" href="./">

            <img class="logo-nav" src="../../../../img/barbershop_transparent_white_border.png">

          </a>

        </nav>

        <!-- Fim Navbar -->

      </header>

  <!-- Inicio Sobre -->

  <section id="sobre" class="sobre" data-nav="Sobre" style="padding-top:50px;">

    <div class="col-md-12" style="padding: 30px 30px;">

      <h1 style="color: #ffcf40;">Mudar Palavra Passe</h1>

    </div>

    <center class="col-md-4 offset-md-4 text-about" style="color: white; display: block;">

    <form action="{{url('/user/final-reset-password')}}" method="post">
      {{ csrf_field() }}
      <input type="hidden" name="id" value="{{$s->id}}">

      <div class="form-group">
        <label for="password">Palavra Passe</label>
        <input type="password" name="pass1" class="form-control" id="password" aria-describedby="emailHelp" placeholder="Enter password">
      </div>
      <div class="form-group">
        <label for="password2">Confirme a Palavra Passe</label>
        <input type="password" name="pass2" class="form-control" id="password2" placeholder="Confirm Password">
      </div>
      <div class="form-group">
        <p id="message"></p>
      </div>
      <button type="submit" class="btn btn-primary btn-lg">Mudar Palavra Passe</button>
    </form>

    <center>

  </section>

  <!-- Fim Sobre -->


  <!-- Inicio Contactos -->

  <section id="contactos" class="contactos" data-nav="Contactos">

    <div class="col-md-12" style="padding: 30px 30px;">

      <h1 style="color: #343a40;">Contactos</h1>

    </div>

    <div class="col-md-12">
          
      <div class="map-responsive mapas"><iframe width="600" height="500" id="gmap_canvas" src="https://maps.google.com/maps?q=Gentleman's%20Barber%20Shop%20lousada&t=&z=17&ie=UTF8&iwloc=&output=embed" frameborder="0" scrolling="no" marginheight="0" marginwidth="0"></iframe><a href="https://www.crocothemes.net"></a></div>

    </div>

    <div class="container animated fadeIn">

      <div class="container second-portion">
        <div class="row">
              <!-- Boxes de Acoes -->
            <div class="col-xs-12 col-sm-6 col-lg-4">
            <div class="box">             
              <div class="icon">
                <div class="image"><i class="fa fa-envelope" aria-hidden="true"></i></div>
                <div class="info">
                  <h3 class="title" style="margin-top: 20px;">Email</h3>
                  <p>
                    {{setting('site.email_box')}}
                  </p>
                
                </div>
              </div>
              <div class="space"></div>
            </div> 
          </div>
            
              <div class="col-xs-12 col-sm-6 col-lg-4">
            <div class="box">             
              <div class="icon">
                <div class="image"><i class="fa fa-mobile" aria-hidden="true"></i></div>
                <div class="info">
                  <h3 class="title" style="margin-top: 20px;">Contacto</h3>
                    <p>
                    {{setting('site.contact_box')}}
                  </p>
                </div>
              </div>
              <div class="space"></div>
            </div> 
          </div>
            
              <div class="col-xs-12 col-sm-6 col-lg-4">
            <div class="box">             
              <div class="icon">
                <div class="image"><i class="fa fa-map-marker" aria-hidden="true"></i></div>
                <div class="info">
                  <h3 class="title" style="margin-top: 20px;">Morada</h3>
                    <p>
                     {{setting('site.address_box')}}
                  </p>
                </div>
              </div>
            </div> 
          </div>        
            
        </div>
      </div>

    </div>

  </section>

  <!-- Fim Contactos -->

  <!-- Inicio Footer -->

  <footer>

    <div class="container-fluid">

      <div class="row">

        <div class="col-md-4 col-sm-6 col-xs-6" style="display: grid; vertical-align: middle; align-items: center; text-align: center;">

          <h6 class="text">Copyright @ <script>document.write(new Date().getFullYear());</script> by Gentleman's Barber Shop</h6>

        </div>

        <div class="col-md-4 d-sm-none d-md-block" style="text-align: center;">

          <img src="../../../img/barbershop_all_white.png" style="max-width:16%;">

        </div>

        <div class="col-md-4 col-sm-6 col-xs-6" style="display: grid; vertical-align: middle; align-items: center; text-align: center;">

          <h6 class="text">Desenvolvido por N'Soluções</h6>

        </div>

      </div>

    </div>

  </footer>

  <!--Fim Footer -->

</body>

<script>
$('#password, #password2').on('keyup', function () {
  if ($('#password').val() == "" || $('#password2').val() == "") {
    $('#message').html('').css('color', 'green');
  }
  else{
    if ($('#password').val() == $('#password2').val()) {
      $('#message').html('Correspondem').css('color', 'green');
    } else 
      $('#message').html('Não Correspondem').css('color', 'red');
  }
});
</script>

<script>
    $(document).ready(function(){
      $(window).scroll(function() { // check if scroll event happened
        if ($(document).scrollTop() > 50) { // check if user scrolled more than 50 from top of the browser window
          $(".navbar-fixed-top").css("background-color", "#343a40");
          $(".navbar-fixed-top").css("border-bottom", "1px solid white");
          $(".nav-link").css("color", "#ffffff80"); // if yes, then change the color of class "navbar-fixed-top" to white (#f8f8f8)
        } else {
          $(".navbar-fixed-top").css("background-color", "#0000002e");
          $(".navbar-fixed-top").css("border-bottom", "0px");
          $(".nav-link").css("color", "#ffcf40"); // if not, change it back to transparent
        }
      });
    });
</script>

<script>
function openModal() {
  document.getElementById('myModal').style.display = "block";
  document.getElementById('body').style.overflow = "hidden";
}

function closeModal() {
  document.getElementById('myModal').style.display = "none";
  document.getElementById('body').style.overflow = "auto";
}

var slideIndex = 1;
showSlides(slideIndex);

function plusSlides(n) {
  showSlides(slideIndex += n);
}

function currentSlide(n) {
  showSlides(slideIndex = n);
}

function showSlides(n) {
  var i;
  var slides = document.getElementsByClassName("mySlides");
  var dots = document.getElementsByClassName("demo");
  if (n > slides.length) {slideIndex = 1}
  if (n < 1) {slideIndex = slides.length}
  for (i = 0; i < slides.length; i++) {
      slides[i].style.display = "none";
  }
  for (i = 0; i < dots.length; i++) {
      dots[i].className = dots[i].className.replace(" active", "");
  }
  slides[slideIndex-1].style.display = "block";
  dots[slideIndex-1].className += " active";
}
</script>

<script>
function openModal1() {
  document.getElementById('myModal1').style.display = "block";
  document.getElementById('body').style.overflow = "hidden";
}

function closeModal1() {
  document.getElementById('myModal1').style.display = "none";
  document.getElementById('body').style.overflow = "auto";
}

var slideIndex1 = 1;
showSlides1(slideIndex1);

function plusSlides1(n) {
  showSlides1(slideIndex1 += n);
}

function currentSlide1(n) {
  showSlides1(slideIndex1 = n);
}

function showSlides1(n) {
  var i;
  var slides1 = document.getElementsByClassName("mySlides1");
  var dots1 = document.getElementsByClassName("demo1");
  if (n > slides1.length) {slideIndex1 = 1}
  if (n < 1) {slideIndex1 = slides1.length}
  for (i = 0; i < slides1.length; i++) {
      slides1[i].style.display = "none";
  }
  for (i = 0; i < dots1.length; i++) {
      dots1[i].className = dots1[i].className.replace(" active", "");
  }
  slides1[slideIndex1-1].style.display = "block";
  dots1[slideIndex1-1].className += " active";
}
</script>

<script>
  $(".nav-link").click(function() {
  var l = $(this).html();
    $('html, body').animate({
        scrollTop: $("[data-nav=\"" + l + "\"]").offset().top - 80
    }, 850);
});

$(window).scroll(function() {
    var windscroll = $(window).scrollTop();
    if (windscroll >= 100) {
        $('[data-nav]').each(function(i) {
            if ($(this).position().top <= windscroll + 120) {
                $('nav li.active').removeClass('active');
                $('nav li').eq(i).addClass('active');
            }
        });

    } else {

        $('nav').removeClass('fixed');
        $('nav a.active').removeClass('active');
        $('nav a:first').addClass('active');
    }

}).scroll();
</script>

</html>