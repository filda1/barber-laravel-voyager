@extends('voyager::master')

@section('page_title', 'Dashboard')

@section('page_header')
    <div class="container-fluid">
        <h1 class="page-title">
            <i class="voyager-home"></i> Dashboard
        </h1>
    </div>
@stop

@section('content')
    <div class="page-content browse container-fluid">
        @include('voyager::alerts')
        <div class="row">
            <div class="col-md-12">
                <div class="panel panel-bordered">
                    @if(Auth::user()->role_id == 1 || Auth::user()->role_id == 3)
                    <div class="panel-body">
                        <div class="col-md-6">
                            <div id="chart_div" style="height: 350px;"></div>
                        </div>
                        <div class="col-md-6">
                            <div id="mv_chart" style="height: 350px;"></div>
                        </div>
                    </div>
                    @elseif(Auth::user()->role_id == 4)
                    <div class="text-center">
                        <img src="{{url('storage')}}/{{Auth::user()->avatar}}" class="avatar" style="border-radius:50%; width:150px; height:150px; border:5px solid #fff;" alt="Pedro Martins avatar">
                        <h5 class="text-center">{{Auth::user()->name}}</h5>
                        <h3 class="text-center">Bem vindo ao painel de controlo da Joana Moreira!</h3>
                    </div>
                    
                    @else
                    <div class="text-center">
                        <img src="{{url('storage')}}/{{Auth::user()->avatar}}" class="avatar" style="border-radius:50%; width:150px; height:150px; border:5px solid #fff;" alt="Pedro Martins avatar">
                        <h5 class="text-center">{{Auth::user()->name}}</h5>
                        <h3 class="text-center">Bem vindo ao painel de controlo da Gentleman's Barber Shop!</h3>
                    </div>
                    
                    @endif
                </div>
            </div>
        </div>
    </div>

   
@stop


@section('javascript')
<script type="text/javascript" src="https://www.gstatic.com/charts/loader.js"></script>
    <script type="text/javascript">
        var mc = jQuery.parseJSON('{!! $mc !!}');
        var result = Object.keys(mc).map(function(key) {
          return [String(key), mc[key]];
        });

        var mv = jQuery.parseJSON('{!! $mv !!}');
        var resultmv = Object.keys(mv).map(function(key) {
          return [String(key), mv[key]];
        });

      google.charts.load('current', {'packages':['corechart']});
      google.charts.setOnLoadCallback(drawChart);
      google.charts.setOnLoadCallback(drawChartmv);

      function drawChart() {
        var data = new google.visualization.DataTable();
        data.addColumn('string', 'Cadeira');
        data.addColumn('number', 'numero');
        data.addRows(result);

        var options = {'title':'Número de Marcações por Cadeira (Nº)'};
        var chart = new google.visualization.PieChart(document.getElementById('chart_div'));
        chart.draw(data, options);
      }

      function drawChartmv() {
        var data = new google.visualization.DataTable();
        data.addColumn('string', 'Cadeira');
        data.addColumn('number', 'numero');
        data.addRows(resultmv);

        var options = {'title':'Valor de Maracações por Cadeira (€)'};
        var chart = new google.visualization.PieChart(document.getElementById('mv_chart'));
        chart.draw(data, options);
      }
    </script>
@stop
