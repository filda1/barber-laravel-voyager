<!DOCTYPE html>

<html>

  <head>

    <meta charset="utf-8">

    <meta name="viewport" content="width=device-width, initial-scale=1">

    <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/4.1.3/css/bootstrap.min.css">

    <script src="https://ajax.googleapis.com/ajax/libs/jquery/3.3.1/jquery.min.js"></script>

    <script src="https://cdnjs.cloudflare.com/ajax/libs/popper.js/1.14.3/umd/popper.min.js"></script>

    <script src="https://maxcdn.bootstrapcdn.com/bootstrap/4.1.3/js/bootstrap.min.js"></script>

    <link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/font-awesome/4.7.0/css/font-awesome.min.css">

    <link rel="stylesheet" href="./css/style.css">

    <link rel="stylesheet" href="./css/bootstrap.min.css">

    <script src="//netdna.bootstrapcdn.com/bootstrap/3.0.0/js/bootstrap.min.js"></script>

    <script src="//code.jquery.com/jquery-1.11.1.min.js"></script>

    <title>Barbearia</title>

  </head>

  <body id="body">

    <a href="https://www.facebook.com/GentlemansBarbershopLousada/" target="_blank">

      <div class="facebook-icon">

        <span style="color: white;" class="fa fa-facebook-square fa-1x"></span>

      </div>

    </a>

    <a href="https://www.instagram.com/gentlemans_barbershop_lousada/" target="_blank">

      <div class="instagram-icon">

        <span style="color: white;" class="fa fa-instagram fa-1x"></span>

      </div>

    </a>

    <header>

      <!-- Inicio Navbar -->

        <nav class="navbar navbar-expand-md navbar-fixed-top navbar-dark" style="background-color: #0000002e;">

          <a class="navbar-brand" href="./">

            <img class="logo-nav" src="./img/barbershop_transparent_white_border.png">

          </a>

          <button class="navbar-toggler" type="button" data-toggle="collapse" data-target="#collapsibleNavbar">

            <span class="navbar-toggler-icon"></span>

          </button>

          <div class="collapse navbar-collapse justify-content-end" id="collapsibleNavbar">

            <ul class="navbar-nav ml-auto">

              <li class="nav-item">

                <a class="nav-link" style="color: #ffcf40;" href="#!">Sobre</a>

              </li>

              <li class="nav-item">

                <a class="nav-link" style="color: #ffcf40;" href="#!">Gentleman's</a>

              </li>

              <li class="nav-item">

                <a class="nav-link" style="color: #ffcf40;" href="#!">Hair Styling</a>

              </li>

              <li class="nav-item">

                <a class="nav-link" style="color: #ffcf40;" href="#!">Contactos</a>

              </li>

            </ul>

          </div>  

        </nav>

        <!-- Fim Navbar -->

      </header>

  <!-- Inicio Carousel -->

  <div id="carouselExampleIndicators" class="carousel slide" data-ride="carousel">

    <!--<ol class="carousel-indicators">

      <li data-target="#carouselExampleIndicators" data-slide-to="0" class="active"></li>

      <li data-target="#carouselExampleIndicators" data-slide-to="1"></li>

      <li data-target="#carouselExampleIndicators" data-slide-to="2"></li>

      <li data-target="#carouselExampleIndicators" data-slide-to="3"></li>

    </ol>-->

    <div class="carousel-inner">

      <div class="carousel-item active">

        <div class="img-carousel" style="background-image: url('./img/barbearia2.jpeg'); background-size: cover; background-position: center;"></div>

      </div>

      <div class="carousel-item">

        <div class="img-carousel" style="background-image: url('./img/barbearia1.jpeg'); background-size: cover; background-position: center;"></div>

      </div>

      <div class="carousel-item">

        <div class="img-carousel" style="background-image: url('./img/barbearia3.jpeg'); background-size: cover; background-position: center;"></div>

      </div>

      <div class="carousel-item">

        <div class="img-carousel" style="background-image: url('./img/barbearia4.jpg'); background-size: cover; background-position: center;"></div>

      </div>

    </div>

    <a class="carousel-control-prev" href="#carouselExampleIndicators" role="button" data-slide="prev">

      <span class="carousel-control-prev-icon" aria-hidden="true"></span>

      <span class="sr-only">Previous</span>

    </a>

    <a class="carousel-control-next" href="#carouselExampleIndicators" role="button" data-slide="next">

      <span class="carousel-control-next-icon" aria-hidden="true"></span>

      <span class="sr-only">Next</span>

    </a>

  </div>

  <!-- Fim Carousel -->

  <!-- Inicio Sobre -->

  <section id="sobre" class="sobre" data-nav="Sobre">

    <div class="col-md-12" style="padding: 30px 30px;">

      <h1 style="color: #ffcf40;">Sobre</h1>

    </div>

    <div class="col-md-12 text-about" style="color: white; display: block;">

      {!! setting('site.about_us') !!}

    <div>

  </section>

  <!-- Fim Sobre -->

  <!-- Inicio Galeria -->

  <section id="Gentleman's" class="galeria" data-nav="Gentleman's">

    <div class="col-md-12" style="padding: 30px 30px;">

      <h1 style="color: #343a40;">Gentleman's</h1>

    </div>

    <div class="col-md-12 img">

      <div class="responsive">

        <div class="gallery">

            <div onclick="openModal();currentSlide(1)" class="img-galeria hover-shadow cursor" style="background-image: url('./img/barbearia4.jpg'); background-size: cover; background-position: center;"></div>

        </div>

      </div>

      <div class="responsive">

        <div class="gallery">

            <div onclick="openModal();currentSlide(2)" class="img-galeria hover-shadow cursor" style="background-image: url('./img/barbearia3.jpeg'); background-size: cover; background-position: center;"></div>

        </div>

      </div>

      <div class="responsive">

        <div class="gallery">

            <div onclick="openModal();currentSlide(3)" class="img-galeria hover-shadow cursor" style="background-image: url('./img/barbearia5.jpg'); background-size: cover; background-position: center;"></div>

        </div>

      </div>

      <div class="responsive">

        <div class="gallery">

            <div onclick="openModal();currentSlide(4)" class="img-galeria hover-shadow cursor" style="background-image: url('./img/barbearia7.jpg'); background-size: cover; background-position: center;"></div>

        </div>

      </div>

    </div>

    <div id="myModal" class="modal">
      <span class="close cursor" style="color: #fff!important;width: 100%;height: 100%;" onclick="closeModal()">&times;</span>
      <div class="modal-content" style="display: block!important;">

        <div class="mySlides">
          <div class="numbertext">1 / 4</div>
          <img src="./img/barbearia4.jpg" style="width:100%">
        </div>

        <div class="mySlides">
          <div class="numbertext">2 / 4</div>
          <img src="./img/barbearia3.jpeg" style="width:100%">
        </div>

        <div class="mySlides">
          <div class="numbertext">3 / 4</div>
          <img src="./img/barbearia5.jpg" style="width:100%">
        </div>
          
        <div class="mySlides">
          <div class="numbertext">4 / 4</div>
          <img src="./img/barbearia7.jpg" style="width:100%">
        </div>
          
        <a class="prev" style="color: white!important;" onclick="plusSlides(-1)">&#10094;</a>
        <a class="next" style="color: white!important;" onclick="plusSlides(1)">&#10095;</a>

        <div class="caption-container">
          <p id="caption"></p>
        </div>


        <div class="column">
          <img class="demo cursor" src="./img/barbearia4.jpg" style="width:100%" onclick="currentSlide(1)">
        </div>
        <div class="column">
          <img class="demo cursor" src="./img/barbearia3.jpeg" style="width:100%" onclick="currentSlide(2)">
        </div>
        <div class="column">
          <img class="demo cursor" src="./img/barbearia5.jpg" style="width:100%" onclick="currentSlide(3)">
        </div>
        <div class="column">
          <img class="demo cursor" src="./img/barbearia7.jpg" style="width:100%" onclick="currentSlide(4)">
        </div>

      </div>
    </div>


  </section>

  <!-- Fim Galeria -->

  <!-- Inicio Trabalho -->

  <section id="portfolio" class="portfolio" data-nav="Hair Styling">

    <div class="col-md-12" style="padding: 30px 30px;">

      <h1 style="color: #ffcf40;">Hair Styling</h1>

    </div>

    <div class="col-md-12 img">

      <div class="responsive">

        <div class="gallery">

            <div onclick="openModal1();currentSlide1(1)" class="img-galeria hover-shadow cursor" style="background-image: url('./img/Galeria/img1.jpeg'); background-size: cover; background-position: center;"></div>

        </div>

      </div>

      <div class="responsive">

        <div class="gallery">

            <div onclick="openModal1();currentSlide1(2)" class="img-galeria hover-shadow cursor" style="background-image: url('./img/Galeria/img2.jpeg'); background-size: cover; background-position: center;"></div>

        </div>

      </div>

      <div class="responsive">

        <div class="gallery">

            <div onclick="openModal1();currentSlide1(3)" class="img-galeria hover-shadow cursor" style="background-image: url('./img/Galeria/img3.jpeg'); background-size: cover; background-position: center;"></div>

        </div>

      </div>

      <div class="responsive">

        <div class="gallery">

            <div onclick="openModal1();currentSlide1(4)" class="img-galeria hover-shadow cursor" style="background-image: url('./img/Galeria/img4.jpeg'); background-size: cover; background-position: center;"></div>

        </div>

      </div>

      <div class="responsive">

        <div class="gallery">

            <div onclick="openModal1();currentSlide1(5)" class="img-galeria hover-shadow cursor" style="background-image: url('./img/Galeria/img5.jpeg'); background-size: cover; background-position: center;"></div>

        </div>

      </div>

      <div class="responsive">

        <div class="gallery">

            <div onclick="openModal1();currentSlide1(6)" class="img-galeria hover-shadow cursor" style="background-image: url('./img/Galeria/img6.jpeg'); background-size: cover; background-position: center;"></div>

        </div>

      </div>

      <div class="responsive">

        <div class="gallery">

            <div onclick="openModal1();currentSlide1(7)" class="img-galeria hover-shadow cursor" style="background-image: url('./img/Galeria/img10.jpeg'); background-size: cover; background-position: center;"></div>

        </div>

      </div>

      <div class="responsive">

        <div class="gallery">

            <div onclick="openModal1();currentSlide1(8)" class="img-galeria hover-shadow cursor" style="background-image: url('./img/Galeria/img11.jpeg'); background-size: cover; background-position: center;"></div>

        </div>

      </div>

      <div id="demo" class="responsive collapse">

        <div class="gallery">

            <div onclick="openModal1();currentSlide1(1)" class="img-galeria hover-shadow cursor" style="background-image: url('./img/Galeria/img1.jpeg'); background-size: cover; background-position: center;"></div>

        </div>

      </div>

      <div id="demo" class="responsive collapse">

        <div class="gallery">

            <div onclick="openModal1();currentSlide1(2)" class="img-galeria hover-shadow cursor" style="background-image: url('./img/Galeria/img2.jpeg'); background-size: cover; background-position: center;"></div>

        </div>

      </div>

      <div id="demo" class="responsive collapse">

        <div class="gallery">

            <div onclick="openModal1();currentSlide1(3)" class="img-galeria hover-shadow cursor" style="background-image: url('./img/Galeria/img3.jpeg'); background-size: cover; background-position: center;"></div>

        </div>

      </div>

      <div id="demo" class="responsive collapse">

        <div class="gallery">

            <div onclick="openModal1();currentSlide1(4)" class="img-galeria hover-shadow cursor" style="background-image: url('./img/Galeria/img4.jpeg'); background-size: cover; background-position: center;"></div>

        </div>

      </div>

      <div id="myModal1" class="modal">
      <span class="close cursor" style="color: #fff!important;width: 100%;height: 100%;" onclick="closeModal1()">&times;</span>
      <div class="modal-content1" style="display: block!important;">

        <div class="mySlides1" style="text-align: center;">
          <div class="numbertext">1 / 12</div>
          <img src="./img/Galeria/img1.jpeg" class="img-styling">
        </div>

        <div class="mySlides1" style="text-align: center;">
          <div class="numbertext">2 / 12</div>
          <img src="./img/Galeria/img2.jpeg" class="img-styling">
        </div>

        <div class="mySlides1" style="text-align: center;">
          <div class="numbertext">3 / 12</div>
          <img src="./img/Galeria/img3.jpeg" class="img-styling">
        </div>
          
        <div class="mySlides1" style="text-align: center;">
          <div class="numbertext">4 / 12</div>
          <img src="./img/Galeria/img4.jpeg" class="img-styling">
        </div>

        <div class="mySlides1" style="text-align: center;">
          <div class="numbertext">5 / 12</div>
          <img src="./img/Galeria/img5.jpeg" class="img-styling">
        </div>

        <div class="mySlides1" style="text-align: center;">
          <div class="numbertext">6 / 12</div>
          <img src="./img/Galeria/img6.jpeg" class="img-styling">
        </div>

        <div class="mySlides1" style="text-align: center;">
          <div class="numbertext">7 / 12</div>
          <img src="./img/Galeria/img7.jpeg" class="img-styling">
        </div>

        <div class="mySlides1" style="text-align: center;">
          <div class="numbertext">8 / 12</div>
          <img src="./img/Galeria/img8.jpeg" class="img-styling">
        </div>

        <div class="mySlides1" style="text-align: center;">
          <div class="numbertext">9 / 12</div>
          <img src="./img/Galeria/img5.jpeg" class="img-styling">
        </div>

        <div class="mySlides1" style="text-align: center;">
          <div class="numbertext">10 / 12</div>
          <img src="./img/Galeria/img6.jpeg" class="img-styling">
        </div>

        <div class="mySlides1" style="text-align: center;">
          <div class="numbertext">11 / 12</div>
          <img src="./img/Galeria/img7.jpeg" class="img-styling">
        </div>

        <div class="mySlides1" style="text-align: center;">
          <div class="numbertext">12 / 12</div>
          <img src="./img/Galeria/img8.jpeg" class="img-styling">
        </div>
          
        <a class="prev" style="color: white!important;" onclick="plusSlides1(-1)">&#10094;</a>
        <a class="next" style="color: white!important;" onclick="plusSlides1(1)">&#10095;</a>

        <div class="caption-container">
          <p id="caption"></p>
        </div>


        <div class="column">
          <img class="demo1 cursor" src="./img/Galeria/img1.jpeg" style="width:100%" onclick="currentSlide1(1)">
        </div>
        <div class="column">
          <img class="demo1 cursor" src="./img/Galeria/img2.jpeg" style="width:100%" onclick="currentSlide1(2)">
        </div>
        <div class="column">
          <img class="demo1 cursor" src="./img/Galeria/img3.jpeg" style="width:100%" onclick="currentSlide1(3)">
        </div>
        <div class="column">
          <img class="demo1 cursor" src="./img/Galeria/img4.jpeg" style="width:100%" onclick="currentSlide1(4)">
        </div>
        <div class="column">
          <img class="demo1 cursor" src="./img/Galeria/img5.jpeg" style="width:100%" onclick="currentSlide1(5)">
        </div>
        <div class="column">
          <img class="demo1 cursor" src="./img/Galeria/img6.jpeg" style="width:100%" onclick="currentSlide1(6)">
        </div>
        <div class="column">
          <img class="demo1 cursor" src="./img/Galeria/img7.jpeg" style="width:100%" onclick="currentSlide1(7)">
        </div>
        <div class="column">
          <img class="demo1 cursor" src="./img/Galeria/img8.jpeg" style="width:100%" onclick="currentSlide1(8)">
        </div>
        <div class="column">
          <img class="demo1 cursor" src="./img/Galeria/img5.jpeg" style="width:100%" onclick="currentSlide1(9)">
        </div>
        <div class="column">
          <img class="demo1 cursor" src="./img/Galeria/img6.jpeg" style="width:100%" onclick="currentSlide1(10)">
        </div>
        <div class="column">
          <img class="demo1 cursor" src="./img/Galeria/img7.jpeg" style="width:100%" onclick="currentSlide1(11)">
        </div>
        <div class="column">
          <img class="demo1 cursor" src="./img/Galeria/img8.jpeg" style="width:100%" onclick="currentSlide1(12)">
        </div>

      </div>
    </div>

    </div>

    <div class="col-12 text-center">

      <button onclick="$(this).hide();" type="button" class="btn btn-info" data-toggle="collapse" data-target="#demo" style="background-color: #ffcf40; border-color: #ffcf40">Ver Mais</button>

    </div>

  </section>

  <!-- Fim Trabalho -->

  <!-- Inicio Contactos -->

  <!--<section id="contactos" class="contactos">

    <div class="col-md-12" style="padding: 30px 30px;">

      <h1 style="color: #343a40;">Contactos</h1>

    </div>

    <div class="row" style="margin-right: auto!important; margin-left: auto!important; display:flex; align-items:center;">

      <div class="col-md-6 mx-auto ml-auto" style="background-image: url('./img/barbearia222.jpeg'); background-size: cover; background-position: center;height: 360px;">

          <p style="text-align: center; color: white;">Avenida Hans Isler, 185</p>

          <p style="text-align: center; color: white;">4620-649 Lousada</p>

          <p style="text-align: center; color: white;">+351 913 762 183</p>

          <div style="text-align: center;">
            
            <a href="https://www.facebook.com/GentlemansBarbershopLousada/" target="_blank"><span style="color: white;" class="fa fa-facebook-square fa-2x"></span></a>

            <a href="https://www.instagram.com/gentlemans_barbershop_lousada/" target="_blank"><span style="color: white;" class="fa fa-instagram fa-2x"></span></a>

          </div>

      </div>

      <div class="col-md-6">
      
        <div class="map-responsive"><iframe width="600" height="500" id="gmap_canvas" src="https://maps.google.com/maps?q=Gentleman's%20Barber%20Shop%20lousada&t=&z=17&ie=UTF8&iwloc=&output=embed" frameborder="0" scrolling="no" marginheight="0" marginwidth="0"></iframe><a href="https://www.crocothemes.net"></a></div>

      </div>

    </div>


  </section>-->

  <!-- Fim Contactos -->

  <!-- Inicio Contactos -->

  <section id="contactos" class="contactos" data-nav="Contactos">

    <div class="col-md-12" style="padding: 30px 30px;">

      <h1 style="color: #343a40;">Contactos</h1>

    </div>

    <div class="col-md-12">
          
      <div class="map-responsive mapas"><iframe width="600" height="500" id="gmap_canvas" src="https://maps.google.com/maps?q=Gentleman's%20Barber%20Shop%20lousada&t=&z=17&ie=UTF8&iwloc=&output=embed" frameborder="0" scrolling="no" marginheight="0" marginwidth="0"></iframe><a href="https://www.crocothemes.net"></a></div>

    </div>

    <div class="container animated fadeIn">

      <div class="container second-portion">
        <div class="row">
              <!-- Boxes de Acoes -->
            <div class="col-xs-12 col-sm-6 col-lg-4">
            <div class="box">             
              <div class="icon">
                <div class="image"><i class="fa fa-envelope" aria-hidden="true"></i></div>
                <div class="info">
                  <h3 class="title" style="margin-top: 20px;">Email</h3>
                  <p>
                    {{setting('site.email_box')}}
                  </p>
                
                </div>
              </div>
              <div class="space"></div>
            </div> 
          </div>
            
              <div class="col-xs-12 col-sm-6 col-lg-4">
            <div class="box">             
              <div class="icon">
                <div class="image"><i class="fa fa-mobile" aria-hidden="true"></i></div>
                <div class="info">
                  <h3 class="title" style="margin-top: 20px;">Contacto</h3>
                    <p>
                    {{setting('site.contact_box')}}
                  </p>
                </div>
              </div>
              <div class="space"></div>
            </div> 
          </div>
            
              <div class="col-xs-12 col-sm-6 col-lg-4">
            <div class="box">             
              <div class="icon">
                <div class="image"><i class="fa fa-map-marker" aria-hidden="true"></i></div>
                <div class="info">
                  <h3 class="title" style="margin-top: 20px;">Morada</h3>
                    <p>
                     {{setting('site.address_box')}}
                  </p>
                </div>
              </div>
            </div> 
          </div>        
            
        </div>
      </div>

    </div>

  </section>

  <!-- Fim Contactos -->

  <!-- Inicio Footer -->

  <footer>

    <div class="container-fluid">

      <div class="row">

        <div class="col-md-4 col-sm-6 col-xs-6" style="display: grid; vertical-align: middle; align-items: center; text-align: center;">

          <h6 class="text">Copyright @ <script>document.write(new Date().getFullYear());</script> by Gentleman's Barber Shop</h6>

        </div>

        <div class="col-md-4 d-sm-none d-md-block" style="text-align: center;">

          <img src="img/barbershop_all_white.png" style="max-width:16%;">

        </div>

        <div class="col-md-4 col-sm-6 col-xs-6" style="display: grid; vertical-align: middle; align-items: center; text-align: center;">

          <h6 class="text">Desenvolvido por N'Soluções</h6>

        </div>

      </div>

    </div>

  </footer>

  <!--Fim Footer -->

</body>

<script>
    $(document).ready(function(){
      $(window).scroll(function() { // check if scroll event happened
        if ($(document).scrollTop() > 50) { // check if user scrolled more than 50 from top of the browser window
          $(".navbar-fixed-top").css("background-color", "#343a40");
          $(".navbar-fixed-top").css("border-bottom", "1px solid white");
          $(".nav-link").css("color", "#ffffff80"); // if yes, then change the color of class "navbar-fixed-top" to white (#f8f8f8)
        } else {
          $(".navbar-fixed-top").css("background-color", "#0000002e");
          $(".navbar-fixed-top").css("border-bottom", "0px");
          $(".nav-link").css("color", "#ffcf40"); // if not, change it back to transparent
        }
      });
    });
</script>

<script>
function openModal() {
  document.getElementById('myModal').style.display = "block";
  document.getElementById('body').style.overflow = "hidden";
}

function closeModal() {
  document.getElementById('myModal').style.display = "none";
  document.getElementById('body').style.overflow = "auto";
}

var slideIndex = 1;
showSlides(slideIndex);

function plusSlides(n) {
  showSlides(slideIndex += n);
}

function currentSlide(n) {
  showSlides(slideIndex = n);
}

function showSlides(n) {
  var i;
  var slides = document.getElementsByClassName("mySlides");
  var dots = document.getElementsByClassName("demo");
  if (n > slides.length) {slideIndex = 1}
  if (n < 1) {slideIndex = slides.length}
  for (i = 0; i < slides.length; i++) {
      slides[i].style.display = "none";
  }
  for (i = 0; i < dots.length; i++) {
      dots[i].className = dots[i].className.replace(" active", "");
  }
  slides[slideIndex-1].style.display = "block";
  dots[slideIndex-1].className += " active";
}
</script>

<script>
function openModal1() {
  document.getElementById('myModal1').style.display = "block";
  document.getElementById('body').style.overflow = "hidden";
}

function closeModal1() {
  document.getElementById('myModal1').style.display = "none";
  document.getElementById('body').style.overflow = "auto";
}

var slideIndex1 = 1;
showSlides1(slideIndex1);

function plusSlides1(n) {
  showSlides1(slideIndex1 += n);
}

function currentSlide1(n) {
  showSlides1(slideIndex1 = n);
}

function showSlides1(n) {
  var i;
  var slides1 = document.getElementsByClassName("mySlides1");
  var dots1 = document.getElementsByClassName("demo1");
  if (n > slides1.length) {slideIndex1 = 1}
  if (n < 1) {slideIndex1 = slides1.length}
  for (i = 0; i < slides1.length; i++) {
      slides1[i].style.display = "none";
  }
  for (i = 0; i < dots1.length; i++) {
      dots1[i].className = dots1[i].className.replace(" active", "");
  }
  slides1[slideIndex1-1].style.display = "block";
  dots1[slideIndex1-1].className += " active";
}
</script>

<script>
  $(".nav-link").click(function() {
  var l = $(this).html();
    $('html, body').animate({
        scrollTop: $("[data-nav=\"" + l + "\"]").offset().top - 80
    }, 850);
});

$(window).scroll(function() {
    var windscroll = $(window).scrollTop();
    if (windscroll >= 100) {
        $('[data-nav]').each(function(i) {
            if ($(this).position().top <= windscroll + 120) {
                $('nav li.active').removeClass('active');
                $('nav li').eq(i).addClass('active');
            }
        });

    } else {

        $('nav').removeClass('fixed');
        $('nav a.active').removeClass('active');
        $('nav a:first').addClass('active');
    }

}).scroll();
</script>

</html>