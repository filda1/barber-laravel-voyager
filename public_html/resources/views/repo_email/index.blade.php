@extends('voyager::master')

@section('page_title', 'Repor Palavra Passe')

@section('page_header')
    <div class="container-fluid">
        <h1 class="page-title">
            Repor Palavra Passe
        </h1>
    </div>
@stop

@section('content')
    <div class="page-content browse container-fluid">
        @include('voyager::alerts')
        <div class="row">
            <div class="col-md-12">
                <div class="panel panel-bordered">
                    <ul class="nav nav-tabs">
                        <li class="active"><a data-toggle="tab" href="#mensagem">Mensagem</a></li>
                        <li><a data-toggle="tab" href="#notificacao">Notificação</a></li>
                    </ul>

                    <div class="panel-body">
                        <div class="col-md-6">
                            <table class="table table-striped">
                                <label style="float: right;">Pesquisar: <input id="searchValue" type="text" name=""></label>
                                <thead>
                                    <tr>
                                        <th><input type="checkbox" id="checkAll"></th>
                                        <th>Nome</th>
                                        <th>Email</th>
                                        <th>Contacto</th>
                                    <tr>
                                </thead>
                                <tbody id="tbodyUsers">
                                    @foreach($users as $user)
                                        <tr>
                                            <td><input type="checkbox" data-id="{{$user->id}}"></td>
                                            <td>{{ $user->first_name . ' ' . $user->last_name }}</td>
                                            <td>{{ $user->email }}</td>
                                            <td>{{ $user->numero }}</td>
                                        </tr>
                                    @endforeach
                                </tbody>
                            </table>
                        </div>
                        <div class="tab-content col-md-6">
                            <div id="mensagem" class="active" style="padding: 0;">
                                <div id="sms-counter">
                                    <textarea class="form-control" onkeyup="CheckChars(this)" style="width: 100%; height: 10em;" id="messageToSend"></textarea>
                                    <p style="display: inline-block;">Total de Caracteres: <span class="length">0</span> / Caracteres Restantes: <span class="remaining">160</span></p>
                                    <p style="display: inline-block; float: right;">Nº de Mensagens a enviar: <span class="messagesTotal">0</span></p>
                                    <span style="display: none;" class="messages">1</span>
                                    <button style="display: block;" class="btn btn-primary" onclick="displaySMS()">Confirmar</button>
                                </div>
                            </div>
                            <div id="notificacao" style="padding: 0;">
                                <label>Titulo</label>
                                <input type="text" class="form-control" id="titleToSend">
                                <label>Descrição</label>
                                <textarea class="form-control" id="descToSend" style="width: 100%; height: 4em;"></textarea>
                                <p><span class="text-warning">Nota: </span>É recomendado as notificações serem o mais curtas possiveís.</p>
                                <button style="display: block;" class="btn btn-primary" onclick="displayNoti()">Confirmar</button>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>

<style>
.tab-content>div {
    padding: 20px;
    display: none;
}
</style>
@stop


@section('javascript')
<script src="{{ url('js/sms_counter.min.js') }}"></script>
<script>

    $("#searchValue").keyup(function() {
      search();
    });


    $("#checkAll").click(function(){
        $('input:checkbox').not(this).prop('checked', this.checked);
    });

    function search(id, position) {

        filterAction($("#searchValue").val());
    }

    function filterAction(filter){
        var value = filter.toLowerCase();
        $("#tbodyUsers tr").filter(function() {
          $(this).toggle($(this).text().toLowerCase().indexOf(value) > -1)
        });
    }

    function CheckChars(e){
    
        $('.conteudoForm').val($(e).val());
        SmsCounter.count($(e).val());
        $(e).countSms('#sms-counter');

        var checked = $(":checked").length;

        $(".messagesTotal").html( parseInt($(".messages").html()) * checked );

        $("#confirmTotalDestinatarios").html(checked);
        $("#confirmTotalSMS").html(parseInt($(".messages").html()) * checked);
        $("#confirmConteudo").html($(e).val());
    } 

    function displayNoti(){

        var checked = $(":checked");

        var toSend = "";
        for (var i = 0; i < checked.length; i++) {
            toSend = toSend + $(checked[i]).data("id") + ",";
        }

        $("#toSMSNoti").val(toSend);

        $("#confirmTotalDestinatariosNoti").html(checked.length);
        $("#titleNotiConfirm").val($("#titleToSend").val());
        $("#confirmNotiDesc").html($("#descToSend").val());

        $("#confirmarModalNoti").modal();
    }

    function displaySMS(){

        var checked = $(":checked");

        var toSend = "";
        for (var i = 0; i < checked.length; i++) {
            toSend = toSend + $(checked[i]).data("id") + ",";
        }

        $("#toSMS").val(toSend);

        $("#confirmTotalDestinatarios").html(checked.length);
        $("#confirmTotalSMS").html(parseInt($(".messages").html()) * checked.length);
        $("#confirmConteudo").html($("#messageToSend").val());

        $("#confirmarModal").modal();
    }
</script>
@stop
