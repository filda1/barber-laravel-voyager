<?php

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

Route::get('/', function () {
    return view('welcome');
});

//Route::get('/teste', 'HelperController@teste');

/*Route::get('/send-pass', function () {
    $now = \Carbon\Carbon::now()->subDays(2);
    
    $u = App\AppUser::where('created_at', '<=', $now)->get();

    foreach($u as $us){
        
        if($us->email == null || $us->numero == null)
            continue;
            
        $p = rand(1000,10000);
        
        $us->password = Hash::make($p.'_gentlemans');
        $us->save();
        
        $sms = new App\Sms();
        $sms->numero = $us->numero;
        $sms->conteudo = 'Ola, as tuas credenciais para a nossa APP GENTLEMANS BARBERSHOP sao'. PHP_EOL.'Email: '. $us->email . PHP_EOL .'Password: '.$p.'_gentlemans';
        $sms->save();
    }
});*/

Route::group(['prefix' => 'admin'], function () {
    Voyager::routes();
});

Route::get('/cadeiras/get', function () {
	$b = App\Slot::where('is_active', 1)->get();
    return $b;
});

Route::get('/cadeiras-estetica/get', function () {
	$b = App\SlotsEstetica::where('is_active', 1)->get();
    return $b;
});

Route::get('/barber-jobs/get', function () {
    $b = App\BarberJob::all();
    return $b;
});

Route::get('/estetica-jobs/get', function () {
    $b = App\EsteticaJob::all();
    return $b;
});

Route::get('/marcacoes/get-id/{id}', function ($id) {
    $m = App\Marcacoes::find($id);

    return $m;
});

Route::get('/marcacoes-estetica/get-id/{id}', function ($id) {
    $m = App\MarcacoesEstetica::find($id);

    return $m;
});

Route::get('/marcacoes/get-cadeira-id/{id}', function ($id) {
    $m = App\Marcacoes::where('cadeira_id', $id);

    return $m;
});

Route::get('/marcacoes-estetica/get-cadeira-id/{id}', function ($id) {
    $m = App\MarcacoesEstetica::where('cadeira_id', $id);

    return $m;
});

Route::get('/diastrabalho/get-id/{id}', function ($id) {
    $m = App\Diastrabalho::find($id);

    return $m;
});

Route::get('/diastrabalho-estetica/get-id/{id}', function ($id) {
    $m = App\DiastrabalhoEstetica::find($id);

    return $m;
});

Route::get('/special-dates/get-id/{id}', function ($id) {
    $m = App\SpecialDate::find($id);

    return $m;
});

Route::get('/special-dates-estetica/get-id/{id}', function ($id) {
    $m = App\SpecialDatesEstetica::find($id);

    return $m;
});

Route::get('/configs/get', function () {
	$c = App\Config::all();
    return $c;
});

Route::get('/configs-estetica/get', function () {
	$c = App\ConfigsEstetica::all();
    return $c;
});

Route::get('/app-users/get', function () {
	$u = App\AppUser::all();
    return $u;
});

Route::post('/marcacoes-recorrentes-estetica/save', 'HelperController@saveRecorrenteEstetica');
Route::post('/marcacoes-recorrentes-estetica/update/{id}', 'HelperController@updateRecorrenteEstetica');
Route::post('/marcacoes-recorrentes/save', 'HelperController@saveRecorrente');
Route::post('/marcacoes-recorrentes/update/{id}', 'HelperController@updateRecorrente');

Route::put('/admin/marcacoes/update-clean/{id}', 'HelperController@updateMarcacao');
Route::post('/admin/marcacoes/save-clean', 'HelperController@newMarcacao');
Route::put('/admin/marcacoes-estetica/update-clean/{id}', 'HelperController@updateMarcacaoEstetica');
Route::post('/admin/marcacoes-estetica/save-clean', 'HelperController@newMarcacaoEstetica');

Route::get('/marcacoes-calendar/get', 'HelperController@getMaracaoes');
Route::get('/marcacoes-calendar-estetica/get', 'HelperController@getMaracaoesEstetica');

Route::get('/update-marcacoes-recorrentes', 'HelperController@updateMarcacoesRecorrentes');
Route::get('/update-marcacoes-recorrentes-estetica', 'HelperController@updateMarcacoesRecorrentesEstetica');

Route::get('/send-marcacoes-notifications', 'OneSignalController@sendMarcacoesNotifications');
Route::get('/send-marcacoes-estetica-notifications', 'OneSignalController@sendMarcacoesNotificationsEstetica');
Route::get('/send-fim-marcacoes-notifications', 'OneSignalController@sendEndMarcacoesNotifications');
Route::get('/send-fim-marcacoes-estetica-notifications', 'OneSignalController@sendEndMarcacoesNotificationsEstetica');

Route::get('/send-marcacoes-sms', 'HelperController@sendMarcacoesSms');
Route::get('/send-marcacoes-estetica-sms', 'HelperController@sendMarcacoesEsteticaSms');

Route::get('/update-data-especiais', 'HelperController@updateDatasEspeciais');
Route::get('/update-data-especiais-estetica', 'HelperController@updateDatasEspeciaisEstetica');

Route::delete('/delete/marcacao-recorrente/{id}', 'HelperController@deleteRecorrente');
Route::delete('/delete/marcacao-recorrente-estetica/{id}', 'HelperController@deleteRecorrenteEstetica');

Route::get('/special-date/check-marcacoes', 'HelperController@specialDateCheck');
Route::get('/special-date/delete-marcacoes', 'HelperController@specialDateDelete');

Route::get('/special-date-estetica/check-marcacoes', 'HelperController@specialDateCheckEstectica');
Route::get('/special-date-estetica/delete-marcacoes', 'HelperController@specialDateDeleteEstectica');

Route::get('/check/data-especial/{cadeira}/{dia}', 'HelperController@checkDate');
Route::get('/check/data-especial-estetica/{cadeira}/{dia}', 'HelperController@checkDateEstetica');

Route::get('/user/reset-password/{id}', 'AppUserController@resetPassword');
Route::post('/user/final-reset-password', 'AppUserController@resetPassword2');

Route::get('/fecho-caixa', 'HelperController@fecharCaixa');
Route::get('/fecho-caixa-estetica', 'HelperController@fecharCaixaEstetica');

Route::get('/fecho-caixa/save', 'HelperController@saveCaixa');
Route::get('/fecho-caixa-estetica/save', 'HelperController@saveCaixaEstetica');

Route::get('/fecho-imprimir/{date}/{cadeira}', 'HelperController@fechoCaixaPrint');
Route::get('/fecho-imprimir-estetica/{date}/{cadeira}', 'HelperController@fechoCaixaPrintEstetica');

Route::get('/get-fecho/{date}/{cadeira}', 'HelperController@getFecho');
Route::get('/get-fecho-estetica/{date}/{cadeira}', 'HelperController@getFechoEstetica');

Route::get('/centro-mensagens', 'HelperController@centroMensagens');
Route::get('/relatorios', 'HelperController@relatorio');
Route::post('/getrelatorios', 'HelperController@getrelatorio');
Route::post('/send-sms', 'HelperController@sendSMS');
Route::post('/send-notification', 'HelperController@sendNotification');

Route::post('/saveOneSignal', 'HelperController@saveOneSignal');

Route::get('users/history/{id}', function($id){
    return App\Marcacoes::where('user_id', $id)->where('dia_fechado', '!=', null)->get();
});

Route::post('/configs/save', function (Illuminate\Http\Request $request) {
	foreach ($request->request as $key => $value) {
		$c = App\Config::where('key', $key)->first();
		if($c){
			$c->value = $value;
			$c->save();
		}
	}

	return redirect('admin/configs');
});

Route::post('/configs-estetica/save', function (Illuminate\Http\Request $request) {
	foreach ($request->request as $key => $value) {
		$c = App\ConfigsEstetica::where('key', $key)->first();
		if($c){
			$c->value = $value;
			$c->save();
		}
	}

	return redirect('admin/configs-estetica');
});

Route::get('/sms/{tag}', 'HelperController@getSMS');
Route::get('/sms/status/{id}', 'HelperController@setSMS');

/*********************APP**********************/
//User
Route::post('/api/user/new', 'AppUserController@registerUser');
Route::get('/api/user/update/onesignal-id', 'AppUserController@updateOnesignalId');
Route::post('/api/user/login', 'AppUserController@loginUser');
Route::post('/api/user/login/with-token', 'AppUserController@loginUserWithToken');
Route::post('/api/user/change-password', 'AppUserController@changePassword');
Route::post('/api/user/new-password', 'AppUserController@newPassword');
Route::get('/api/user/last-apointment/{id}', 'MarcacoesController@lastApointment');
Route::get('/api/user/last-apointment/estetica/{id}', 'MarcacoesController@lastApointmentEstetica');
Route::get('/api/user/info-apointment/{id}', 'MarcacoesController@infoApointment');
Route::get('/api/user/info-apointment/estetica/{id}', 'MarcacoesController@infoApointmentEstetica');
Route::get('/api/user/remove-apointment/{id}/{userID}', 'MarcacoesController@deleteApointment');
Route::get('/api/user/remove-apointment/estetica/{id}/{userID}', 'MarcacoesController@deleteApointmentEstetica');

//Galeria
Route::get('/api/galeria', 'HelperController@getImages');
//Marcações
Route::post('/api/marcacoes/cadeiras', 'MarcacoesController@getCadeiras');
Route::post('/api/marcacoes/cadeiras/estetica', 'MarcacoesController@getCadeirasEstetica');
Route::post('/api/marcacoes/verficar-disponiblidade', 'MarcacoesController@verificarDisponiblidade');
Route::post('/api/marcacoes/verficar-disponiblidade/estetica', 'MarcacoesController@verificarDisponiblidadeEstetica');
Route::post('/api/marcacoes/new', 'MarcacoesController@new');
Route::post('/api/marcacoes/new/estetica', 'MarcacoesController@newEstetica');
Route::get('/api/marcacoes/old/{id}', 'MarcacoesController@old');
Route::get('/api/marcacoes/old/estetica/{id}', 'MarcacoesController@oldEstetica');
Route::get('/api/marcacoes/get/{id}', 'MarcacoesController@get');
Route::get('/api/marcacoes/get/estetica/{id}', 'MarcacoesController@getEstetica');
Route::post('/api/marcacoes/get/day', 'MarcacoesController@getDate');
Route::post('/api/marcacoes/get/estetica/day', 'MarcacoesController@getDateEstetica');
Route::get('/api/marcacao/get/{id}', 'MarcacoesController@getMarcacao');
Route::get('/api/marcacao/finaliar/{id}/{valor}', 'MarcacoesController@finalizarMarcacao');
Route::post('/api/marcacao/upload-image', 'MarcacoesController@uploadImage');
Route::post('/api/marcacao/estetica/upload-image', 'MarcacoesController@uploadImageEstetica');
//Trabalhos

//Barbeiros
Route::post('/api/barber/get/day', 'MarcacoesController@barberGetDay');

Route::get('/api/jobs', 'BarberJobsController@getJobs');
Route::get('/api/jobs/estetica', 'BarberJobsController@getJobsEstetica');


/*********************Politica RGPD**********************/
Route::get('/rgpd', function(){ echo '<h1>Termos e Condições</h1>

Estes termos e condições ("Termos", "Acordo") são um acordo entre a N\'Soluções (Desenvolvedora de Aplicações Móveis) e você (“Utilizador”, "Usuário", "você" ou "seu"). Este acordo estabelece os termos e condições gerais da sua utilização da aplicação móvel da Gentleman\'s Barbershop ou de qualquer um dos seus produtos ou serviços (coletivamente, "Aplicação Móvel" ou "Serviços").

 

<h1>Contas e filiação</h1>

Se o Utilizador criar uma conta na aplicação para dispositivos móveis, será responsável por manter a segurança da sua conta e será totalmente responsável por todas as atividades que ocorram na conta e por quaisquer outras ações tomadas em relação a ela. Fornecer informações de contacto falsas de qualquer tipo pode resultar no encerramento da sua conta. O Utilizador deve-nos notificar imediatamente sobre qualquer uso não autorizado da sua conta ou qualquer outra violação de segurança. A Mobile Application não será responsável por quaisquer atos ou omissões causados pelo utilizador, incluindo quaisquer danos de qualquer natureza incorridos como resultado de tais atos ou omissões. Poderemos suspender, desativar ou excluir a sua conta (ou qualquer parte dela) se determinarmos que o Utilizador violou qualquer cláusula deste contrato ou que a sua conduta ou conteúdo tenderá a prejudicar nossa reputação e boa vontade. Se excluirmos a sua conta pelos motivos acima, o Utilizador não se poderá registar novamente nos nossos Serviços. Podemos bloquear o seu endereço de e-mail e endereço de protocolo da internet para evitar mais inscrições.

 

<h1>Direito de propriedade intelectual</h1>
 
Este contrato não transfere para o Utilizador nenhuma propriedade intelectual de propriedade da N\'Soluções ou de terceiros, e todos os direitos, títulos e interesses relativos a essa propriedade permanecerão (como entre as partes) exclusivamente com o N\'Soluções. Todas as marcas registadas, marcas de serviço, gráficos e logotipos usados em conexão com as nossas aplicações ou Serviços Móveis são marcas comerciais ou marcas registadas de licenciadores do N\'Soluções ou do N\'Soluções. Outras marcas comerciais, marcas de serviço, gráficos e logótipos usados em conexão com as nossas aplicações ou Serviços Móveis podem ser marcas registadas de terceiros. O uso que o Utilizador faz das nossas aplicações e serviços móveis não lhe concede nenhum direito ou licença para reproduzir ou usar qualquer desenvolvedor de aplicativos móveis ou marcas registradas de terceiros.
 

<h1>Alterações e emendas</h1>
 
Reservamo-nos no direito de modificar este contrato ou as suas políticas relacionadas à aplicação ou serviços móveis a qualquer momento, em vigor após a publicação de uma versão atualizada deste contrato na aplicação para Dispositivos Móveis. Quando o fizermos, publicaremos uma notificação na nossa aplicação para dispositivos móveis. O uso contínuo da aplicação para dispositivos móveis após tais alterações constituirá seu consentimento para tais alterações.
 
 
<h1>Aceitação destes termos</h1>
 
Você reconhece que leu este Acordo e concorda com todos os seus termos e condições. Ao usar a aplicação móvel ou os seus serviços, você concorda em ficar vinculado a este contrato. Se você não concordar em cumprir os termos deste Contrato, você não está autorizado a usar ou aceder à aplicação para Dispositivos Móveis e seus Serviços.
 
Se você tiver alguma dúvida sobre este Contrato, entre em contato com a N\'Soluções.
 
Este documento foi atualizado pela última vez a 10 de Janeiro de 2019'; });

